﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ОбработчикиСобытий

&ИзменениеИКонтроль("ПриКомпоновкеРезультата")
Процедура МЛ_ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)

	СтандартнаяОбработка = Ложь;
	ПользовательскиеНастройкиМодифицированы = Ложь;

	СегментыСервер.ВключитьОтборПоСегментуНоменклатурыВСКД(КомпоновщикНастроек);

	ИспользуетсяОтборПересчетуТоваров  = КомпоновкаДанныхКлиентСервер.ПолучитьПараметр(
	КомпоновщикНастроек.Настройки, 
	"ИспользуетсяОтборПересчетуТоваров");

	Если ИспользуетсяОтборПересчетуТоваров = Неопределено Тогда
		Возврат;
	КонецЕсли;

	ИспользуетсяОтборПересчетуТоваров.Значение      = Ложь;
	ИспользуетсяОтборПересчетуТоваров.Использование = Ложь;

	НастройкиОсновнойСхемы = КомпоновщикНастроек.ПолучитьНастройки();

	ЭлементыОтбора = ОбщегоНазначенияКлиентСервер.НайтиЭлементыИГруппыОтбора(НастройкиОсновнойСхемы.Отбор,"ПересчетТоваров");

	ИспользуетсяОтборПересчетуТоваров = Ложь;

	Для каждого ЭлементОтбора Из ЭлементыОтбора Цикл
		Если ЭлементОтбора.Использование Тогда  
			ИспользуетсяОтборПересчетуТоваров = Истина;
			Прервать;
		КонецЕсли;
	КонецЦикла;

	ПараметрИспользуетсяОтборПересчетуТоваров  = КомпоновкаДанныхКлиентСервер.ПолучитьПараметр(
	КомпоновщикНастроек.Настройки, 
	"ИспользуетсяОтборПересчетуТоваров");

	ПараметрИспользуетсяОтборПересчетуТоваров.Значение = Истина;
	ПараметрИспользуетсяОтборПересчетуТоваров.Использование = ИспользуетсяОтборПересчетуТоваров;

	ТекстЗапроса = СхемаКомпоновкиДанных.НаборыДанных.НаборДанных.Запрос;

	ТекстЗапроса = СтрЗаменить(
	ТекстЗапроса, 
	"&ТекстЗапросаВесНоменклатуры", 
	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаВесУпаковки("ТоварыНаСкладахОстаткиИОбороты.Номенклатура.ЕдиницаИзмерения", "ТоварыНаСкладахОстаткиИОбороты.Номенклатура"));

	ТекстЗапроса = СтрЗаменить(
	ТекстЗапроса, 
	"&ТекстЗапросаОбъемНоменклатуры", 
	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаОбъемУпаковки("ТоварыНаСкладахОстаткиИОбороты.Номенклатура.ЕдиницаИзмерения", "ТоварыНаСкладахОстаткиИОбороты.Номенклатура"));
#Вставка
	// 4D:Милавица, АндрейБ, 01.04.2019 
	// Задача № 21558 ДС №19.1 Доработки по складским процессам сырьевых складов Милавицы
	// { 
	//ТекстЗапроса = СтрЗаменить(
	//	ТекстЗапроса, 
	//	"&Чд_ТекстЗапросаКоэффициентУпаковки", 
	//	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки("ТоварыНаСкладахОстаткиИОбороты.Номенклатура.ЕдиницаДляОтчетов",
	//	"ТоварыНаСкладахОстаткиИОбороты.Номенклатура",
	//	"ТоварыНаСкладахОстаткиИОбороты.Серия"));
#КонецВставки

	СхемаКомпоновкиДанных.НаборыДанных.НаборДанных.Запрос = ТекстЗапроса;

	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновки = КомпоновщикМакета.Выполнить(СхемаКомпоновкиДанных, НастройкиОсновнойСхемы, ДанныеРасшифровки);

	КомпоновкаДанныхСервер.УстановитьЗаголовкиМакетаКомпоновки(ПолучитьЗаголовкиПолей(), МакетКомпоновки);
	КомпоновкаДанныхСервер.УстановитьЗаголовкиМакетаКомпоновки(СтруктураДинамическихЗаголовков(), МакетКомпоновки);

	ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
	ПроцессорКомпоновки.Инициализировать(МакетКомпоновки, , ДанныеРасшифровки, Истина);

	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВТабличныйДокумент;
	ПроцессорВывода.УстановитьДокумент(ДокументРезультат);
	ПроцессорВывода.Вывести(ПроцессорКомпоновки);

	КомпоновкаДанныхСервер.СкрытьВспомогательныеПараметрыОтчета(СхемаКомпоновкиДанных, КомпоновщикНастроек, ДокументРезультат, ВспомогательныеПараметрыОтчета());

	// Сообщим форме отчета, что настройки модифицированы
	Если ПользовательскиеНастройкиМодифицированы Тогда
		КомпоновщикНастроек.ПользовательскиеНастройки.ДополнительныеСвойства.Вставить("ПользовательскиеНастройкиМодифицированы", Истина);
	КонецЕсли;
#Вставка
// 4D:Милавица, АндрейБ, 19.09.2018 
// "Вывод даты и времени печати для отчетов по складу и отчетов по фин.результату" , № 20205
// выводит колонтитулы в печатную форму (задаются в пользовательском режиме "НСИ и администрирование"=>
//=> "Администрирование" => "Настройка колонтитулов")
	УправлениеКолонтитулами.УстановитьКолонтитулы(ДокументРезультат, "Ведомость по товарам на складах");
#КонецВставки

КонецПроцедуры

#КонецОбласти 

#КонецЕсли
﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура МЛ_ПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	
	Объект.Чд_СпецификацииБригады.Сортировать("Модель");
	Если Параметры.Свойство("ОткрытьСпециализацию") Тогда
		Если Параметры.ОткрытьСпециализацию Тогда
			Элементы.ГруппаСтраницаОсновная.Видимость = Ложь;
			Элементы.ГруппаСтраницаСпециализаций.Видимость = Истина;		
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
&После("ПриОткрытии")
Процедура МЛ_ПриОткрытии(Отказ)
	
	Чд_НастроитьВидимостьГиперссылок();
	
КонецПроцедуры

&НаКлиенте
&После("ОбработкаВыбора")
Процедура МЛ_ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ТипЗнч(ИсточникВыбора) = Тип("ФормаКлиентскогоПриложения")
		И ИсточникВыбора.ИмяФормы = "Справочник.СтруктураПредприятия.Форма.Чд_НастройкиБригады" Тогда
		
		Если ВыбранноеЗначение <> Неопределено
			И ЗначениеЗаполнено(ВыбранноеЗначение) Тогда
			
			ЗаполнитьЗначенияСвойств(Объект, ВыбранноеЗначение);
			Модифицированность = Истина;
		КонецЕсли; 
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// 4D:Милавица, Анастасия, 03.10.2019 9:49:51 
// ДС №11 п.1 Разработка решений по проектному решению "Назначение бригад пошива и расчет плановых дат производства", № 20355 
// {
&НаКлиенте
Процедура Подключаемый_ОткрытьСпециализацию(Команда)
	
	Элементы.ГруппаСтраницаОсновная.Видимость = Ложь;
	Элементы.ГруппаСтраницаСпециализаций.Видимость = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВернутьсяНаОсновнуюСтраницу(Команда)
	
	Элементы.ГруппаСтраницаОсновная.Видимость = Истина;
	Элементы.ГруппаСтраницаСпециализаций.Видимость = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОткрытьЧисленностьИОтпуск(Команда)
	
	ЧисленностьБригады = ПолучитьСсылкуНаЧисленностьБригады(Объект.Ссылка, Ложь);
	Если ЗначениеЗаполнено(ЧисленностьБригады) Тогда
		ПараметрыФормы = Новый Структура("Ключ", ЧисленностьБригады);
		ОткрытьФорму("Справочник.Чд_ЧисленностьБригады.Форма.ФормаЭлемента", ПараметрыФормы);
	Иначе
		Оповещение = Новый ОписаниеОповещения("ПослеЗакрытияВопросаПоЧисленностиБригады",
		ЭтотОбъект);	
		
		ПоказатьВопрос(Оповещение,
		"По данному подразделению Численность не найдена. Создать?",
		РежимДиалогаВопрос.ДаНет,0, КодВозвратаДиалога.Да, 
		"Оповещение" ); 	
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОткрытьМощность(Команда)
	
	МощностьБригады = ПолучитьСсылкуНаМощностьБригады(Объект.Ссылка, Ложь);
	Если ЗначениеЗаполнено(МощностьБригады) Тогда
		ПараметрыФормы = Новый Структура("Ключ", МощностьБригады);
		ОткрытьФорму("Справочник.Чд_КалендариМощностиПодразделения.Форма.ФормаЭлемента", ПараметрыФормы);
	Иначе
		Оповещение = Новый ОписаниеОповещения("ПослеЗакрытияВопросаМощность",
		ЭтотОбъект);	
		
		ПоказатьВопрос(Оповещение,
		"По данному подразделению Мощность не найдена. Создать?",
		РежимДиалогаВопрос.ДаНет,0, КодВозвратаДиалога.Да, 
		"Оповещение" ); 	
	КонецЕсли; 
	
КонецПроцедуры

 // 4D:Милавица, Анастасия, 30.10.2020
// Доработка планирование и исправление ошибок, № 27139  
// {п.3 Доработать для Бригады заполнение НЗП через периодический регистр сведений
&НаКлиенте
Процедура Подключаемый_ДанныеНЗП(Команда)
	
	Если ЗначениеЗаполнено(Объект.Ссылка) Тогда 
		ПараметрыФормы = Новый Структура("Ключ", Объект.Ссылка);
		ОткрытьФорму("РегистрСведений.Чд_НЗПБригадПошива.ФормаСписка", ПараметрыФормы);
	Иначе
		ТекстСообщения = НСтр("ru = 'Необходимо сначала записать элемент справочника!");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);	
	КонецЕсли;
	
КонецПроцедуры
// }
// 4D  Анастасия

// 4D:Милавица, АндрейБ,  02.11.2020 
// Задача №27118 'Бригады-исполнители производственных процессов.Кладовые бригад ШЦ'
// { 
&НаКлиенте
Процедура Подключаемый_НастройкиБригады(Команда)
	
	ПараметрыФормы = Чд_ПараметрыФормыНастройкиБригады();
	ЗаполнитьЗначенияСвойств(ПараметрыФормы, Объект);
	ОткрытьФорму("Справочник.СтруктураПредприятия.Форма.Чд_НастройкиБригады", ПараметрыФормы, ЭтотОбъект);
	
КонецПроцедуры
// }4D

&НаКлиенте
Функция Чд_ПараметрыФормыНастройкиБригады()
	
	СтруктураНастроек = Новый Структура;
	СтруктураНастроек.Вставить("Чд_НЗП", 0);
	СтруктураНастроек.Вставить("Чд_ГрафикПриезда",                 ПредопределенноеЗначение("Справочник.Календари.ПустаяСсылка"));
	СтруктураНастроек.Вставить("Чд_БригадаПошива",                 Ложь);
	СтруктураНастроек.Вставить("Чд_БригадаПодготовительныхПроцессов", Ложь);
	СтруктураНастроек.Вставить("Чд_БригадаРаскрояКружева",         ПредопределенноеЗначение("Справочник.СтруктураПредприятия.ПустаяСсылка"));
	СтруктураНастроек.Вставить("Чд_БригадаРаскрояПолотен",         ПредопределенноеЗначение("Справочник.СтруктураПредприятия.ПустаяСсылка"));
	СтруктураНастроек.Вставить("Чд_БригадаЗаготовкиБретелей",      ПредопределенноеЗначение("Справочник.СтруктураПредприятия.ПустаяСсылка"));
	СтруктураНастроек.Вставить("Чд_СкладКладоваяМатериалов",       ПредопределенноеЗначение("Справочник.Склады.ПустаяСсылка"));
	СтруктураНастроек.Вставить("Чд_Кладовая",                      ПредопределенноеЗначение("Справочник.Склады.ПустаяСсылка"));
	СтруктураНастроек.Вставить("Чд_СкладКладоваяИсправлений",      ПредопределенноеЗначение("Справочник.Склады.ПустаяСсылка"));
	СтруктураНастроек.Вставить("Чд_ПроизводственноеПодразделение", ПредопределенноеЗначение("Справочник.СтруктураПредприятия.ПустаяСсылка"));	
	
	Возврат СтруктураНастроек;
	
КонецФункции

// ++EE:TEN 29.11.2022 MLVC-922
// Добавить Регистр сведений «Процент контроля качества ГП %» , 
// ссылку на него на форме элемента справочника "Структура предприятия" 
&НаКлиенте
Процедура Подключаемый_ПроцентКонтроляКачестваГП(Команда)
	
	Если ЗначениеЗаполнено(Объект.Ссылка) Тогда 
		ПараметрыФормы = Новый Структура("Ключ", Объект.Ссылка);
		ОткрытьФорму("РегистрСведений.ЭЭ_ПроцентКонтроляКачестваГП.ФормаСписка", ПараметрыФормы);
	Иначе
		ТекстСообщения = НСтр("ru = 'Необходимо сначала записать элемент справочника!");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);	
	КонецЕсли;
	
КонецПроцедуры
// --EE:TEN 29.11.2022 MLVC-922

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура Подключаемый_БригадаПошиваПриИзменении(Элемент)
	
	Чд_НастроитьВидимостьГиперссылок();
	Если НЕ Объект.Чд_БригадаПошива Тогда
		Элементы.ЭЭ_ОтражатьРасходМатериалов.Видимость 			= Ложь;
		Элементы.ЭЭ_УдалятьНеобеспеченнуюНоменклатуру.Видимость = Ложь;
	Иначе
		Элементы.ЭЭ_ОтражатьРасходМатериалов.Видимость 			= Истина;
		Элементы.ЭЭ_УдалятьНеобеспеченнуюНоменклатуру.Видимость = Истина;
	КонецЕсли;

КонецПроцедуры
 
&НаКлиенте
Процедура Подключаемый_БригадаПодготовительныхПроцессовПриИзменении(Элемент)
	
	Чд_НастроитьВидимостьГиперссылок();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_СпецификацииБригадыМодельПриИзменении(Элемент)
	
	КодСтроки = Элементы.Чд_СпецификацииБригады.ТекущиеДанные.ПолучитьИдентификатор();
	Чд_СпецификацииБригадыНаСервереМодельПриИзменении(КодСтроки);
	
КонецПроцедуры

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции

&НаСервереБезКонтекста
Функция ПолучитьСсылкуНаЧисленностьБригады(Подразделение, ПризнакНового)
	
	Если ПризнакНового Тогда
		ЧисленностьБригадыОбъект = Справочники.Чд_ЧисленностьБригады.СоздатьЭлемент();
		ЧисленностьБригадыОбъект.Подразделение = Подразделение;
		ЧисленностьБригадыОбъект.Наименование = "Численность бригады " + Строка(Подразделение);
		ЧисленностьБригадыОбъект.Записать();
		Возврат ЧисленностьБригадыОбъект.Ссылка;
	Иначе
		Возврат Справочники.Чд_ЧисленностьБригады.НайтиПоРеквизиту("Подразделение", Подразделение);
	КонецЕсли;
	
КонецФункции

&НаКлиенте
Процедура ПослеЗакрытияВопросаПоЧисленностиБригады(Результат, Параметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		ЧисленностьБригады = ПолучитьСсылкуНаЧисленностьБригады(Объект.Ссылка, Истина);
		ПараметрыФормы = Новый Структура("Ключ", ЧисленностьБригады);
		ОткрытьФорму("Справочник.Чд_ЧисленностьБригады.Форма.ФормаЭлемента", ПараметрыФормы);
	Иначе
		Возврат;
	КонецЕсли;	
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗакрытияВопросаМощность(Результат, Параметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		МощностьБригады = ПолучитьСсылкуНаМощностьБригады(Объект.Ссылка, Истина);
		ПараметрыФормы = Новый Структура("Ключ", МощностьБригады);
		ОткрытьФорму("Справочник.Чд_КалендариМощностиПодразделения.Форма.ФормаЭлемента", ПараметрыФормы);
	Иначе
		Возврат;
	КонецЕсли;	
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьСсылкуНаМощностьБригады(Подразделение, ПризнакНового)
	
	Если ПризнакНового Тогда
		НоваяМощностьБригады = Справочники.Чд_КалендариМощностиПодразделения.СоздатьЭлемент();
		НоваяМощностьБригады.Подразделение = Подразделение;
		НоваяМощностьБригады.Наименование = "Мощность бригады " + Строка(Подразделение);
		НоваяМощностьБригады.Записать();
		Возврат НоваяМощностьБригады.Ссылка;
	Иначе
		Возврат Справочники.Чд_КалендариМощностиПодразделения.НайтиПоРеквизиту("Подразделение", Подразделение);
	КонецЕсли;
	
КонецФункции

&НаСервере
Процедура Чд_СпецификацииБригадыНаСервереМодельПриИзменении(КодСтроки)
	
	ТекущаяСтрока = Объект.Чд_СпецификацииБригады.НайтиПоИдентификатору(КодСтроки);

	Для каждого СтрокаСпец Из Объект.Чд_СпецификацииБригады Цикл
		Если СтрокаСпец.НомерСтроки <> ТекущаяСтрока.НомерСтроки 
			и СтрокаСпец.Модель = ТекущаяСтрока.Модель Тогда
			ТекущаяСтрока.Модель = Справочники.Чд_МоделиНоменклатуры.ПустаяСсылка();
			ТекстСообщения = НСтр("ru = 'Данная Модель уже есть в специализации Бригады!'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
			Возврат;
		КонецЕсли; 

	КонецЦикла; 

КонецПроцедуры

// Описание:
// управляет видимостью гиперссылок в зависимости от значений реквизитов
&НаКлиенте
Процедура Чд_НастроитьВидимостьГиперссылок()

	Если НЕ Объект.Чд_БригадаПошива
		И НЕ Объект.Чд_БригадаПодготовительныхПроцессов Тогда
		
		Элементы.ГруппаГиперссылок.Видимость = Ложь;
		Возврат;
	Иначе
		Элементы.ГруппаГиперссылок.Видимость = Истина;
	КонецЕсли;
	
	Если НЕ Объект.Чд_БригадаПошива Тогда
		Элементы.Чд_ОткрытьЧисленностьИОтпуск.Видимость = Ложь;
		Элементы.Чд_ОткрытьСпециализацию.Видимость      = Ложь;
		Элементы.Чд_ОткрытьМощность.Видимость           = Ложь;
		// ++EE:TEN 29.11.2022 MLVC-922
		Элементы.ЭЭ_ПроцентКонтроляКачестваГП.Видимость = Ложь;
		// --EE:TEN 29.11.2022 MLVC-922
	Иначе
		Элементы.Чд_ОткрытьЧисленностьИОтпуск.Видимость = Истина;
		Элементы.Чд_ОткрытьСпециализацию.Видимость      = Истина;
		Элементы.Чд_ОткрытьМощность.Видимость           = Истина;
		// ++EE:TEN 29.11.2022 MLVC-922
		Элементы.ЭЭ_ПроцентКонтроляКачестваГП.Видимость = Истина;
		// --EE:TEN 29.11.2022 MLVC-922
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти
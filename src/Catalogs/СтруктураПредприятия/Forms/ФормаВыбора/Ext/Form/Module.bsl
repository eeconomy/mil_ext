﻿
#Область ОбработчикиСобытийФормы

&НаСервере
&После("ПриСозданииНаСервере")
Процедура МЛ_ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	// 4D:Милавица, Анастасия, 09.12.2020 
	// Доработка Состояния бригад ДС№7, 27540 
	// {если есть уловия отбора необходимо выводить список иначе ничего не выводит для отображения
	Если Параметры.Отбор.Количество() <> 0 Тогда 
		Элементы.Список.Отображение = ОтображениеТаблицы.Список;
	КонецЕсли;
КонецПроцедуры

&НаСервере
&После("НастроитьОтображениеСписка")
Процедура МЛ_НастроитьОтображениеСписка()

	// 4D:Милавица, Михаил, 17.09.2019 
	// Процессы раскроя (учет производственных операций и трудозатрат), №22743
	// {
	Если Элементы.Список.Отображение = ОтображениеТаблицы.Список Тогда
		Возврат;
	КонецЕсли;
	
	Если Параметры.Свойство("Отбор")
			И (Параметры.Отбор.Свойство("Чд_ИспользоватьКонтрольныеЛисты")
				ИЛИ Параметры.Отбор.Свойство("Родитель")) Тогда
		
		// Если установлен отбор то список должен быть не иерахическим
		Элементы.Список.Отображение = ОтображениеТаблицы.Список;
	КонецЕсли;
	// }4D
	
КонецПроцедуры

#КонецОбласти
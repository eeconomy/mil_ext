﻿
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ПараметрыФормы = Новый Структура("Отбор", Новый Структура("Владелец", ПараметрКоманды));
	
	ОткрытьФорму("Справочник.ВидыЦенПоставщиков.ФормаСписка",
	             ПараметрыФормы,
	             ПараметрыВыполненияКоманды.Источник,
	             ПараметрыВыполненияКоманды.Уникальность,
	             ПараметрыВыполненияКоманды.Окно);
КонецПроцедуры

#КонецОбласти
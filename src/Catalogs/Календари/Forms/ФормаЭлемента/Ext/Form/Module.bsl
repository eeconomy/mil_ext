﻿ 
 #Область ОбработчикиСобытийФормы
 
 &НаСервере
 &Перед("ПриСозданииНаСервере")
 Процедура МЛ_ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)   
	 // ++EE:KAS 28.06.2023 SDEE-2394
	 Чд_УправлениеФормамиСправочников.ПриСозданииНаСервереПеред(ЭтотОбъект, Отказ, СтандартнаяОбработка);  
	 // --EE:KAS 28.06.2023 SDEE-2394 
 КонецПроцедуры
 
 &НаСервере
 &Перед("ПриЧтенииНаСервере")
 Процедура МЛ_ПриЧтенииНаСервере(ТекущийОбъект) 
	 // ++EE:KAS 26.05.2023 MLVC-1193
	 Чд_УправлениеФормамиСправочников.ПриЧтенииНаСервереПеред(ЭтаФорма, ТекущийОбъект);
	 // --EE:KAS 26.05.2023 MLVC-1193
 КонецПроцедуры
 
 #КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиентеНаСервереБезКонтекста
&ИзменениеИКонтроль("СформироватьШаблонЗаполнения")
Процедура МЛ_СформироватьШаблонЗаполнения(СпособЗаполнения, ШаблонЗаполнения, Знач ДлинаЦикла, Знач ДатаОтсчета)

	 // Формирует таблицу редактирования шаблона заполнения по дням.

	 Если СпособЗаполнения = ПредопределенноеЗначение("Перечисление.СпособыЗаполненияГрафикаРаботы.ПоНеделям") Тогда
		 ДлинаЦикла = 7;
	 КонецЕсли;

	 Пока ШаблонЗаполнения.Количество() > ДлинаЦикла Цикл
		 ШаблонЗаполнения.Удалить(ШаблонЗаполнения.Количество() - 1);
	 КонецЦикла;

	 Пока ШаблонЗаполнения.Количество() < ДлинаЦикла Цикл
		 ШаблонЗаполнения.Добавить();
	 КонецЦикла;

	 Если СпособЗаполнения = ПредопределенноеЗначение("Перечисление.СпособыЗаполненияГрафикаРаботы.ПоНеделям") Тогда
		 ШаблонЗаполнения[0].ПредставлениеДня = НСтр("ru = 'Понедельник';
		 |en = 'Monday'");
		 ШаблонЗаполнения[1].ПредставлениеДня = НСтр("ru = 'Вторник';
		 |en = 'Tuesday'");
		 ШаблонЗаполнения[2].ПредставлениеДня = НСтр("ru = 'Среда';
		 |en = 'Wednesday'");
		 ШаблонЗаполнения[3].ПредставлениеДня = НСтр("ru = 'Четверг';
		 |en = 'Thursday'");
		 ШаблонЗаполнения[4].ПредставлениеДня = НСтр("ru = 'Пятница';
		 |en = 'Friday'");
		 ШаблонЗаполнения[5].ПредставлениеДня = НСтр("ru = 'Суббота';
		 |en = 'Saturday'");
		 ШаблонЗаполнения[6].ПредставлениеДня = НСтр("ru = 'Воскресенье';
		 |en = 'Sunday'");
	 Иначе
		 ДатаДня = ДатаОтсчета;
		 Для Каждого СтрокаДня Из ШаблонЗаполнения Цикл
			 СтрокаДня.ПредставлениеДня = Формат(ДатаДня, "ДФ=д.ММ");
			 СтрокаДня.ПредставлениеРасписания = ПредставлениеПустогоРасписания();  
			 #Вставка 
			  // ++EE:KAS 26.05.2023 MLVC-1193
			 СтрокаДня.ЭЭ_ДеньНедели = ЭЭ_ПолучитьПредставлениеДняНедели(ДатаДня); 
			  // --EE:KAS 26.05.2023 MLVC-1193
			 #КонецВставки
			 ДатаДня = ДатаДня + 86400;
		 КонецЦикла;
	 КонецЕсли;

 КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
&После("ЗаполнитьПредставлениеРасписания")
 Процедура МЛ_ЗаполнитьПредставлениеРасписания(Форма)
	 
	 // ++EE:KAS 26.05.2023 MLVC-1193 
	 Элементы = Форма.Элементы;    
	 Попытка
		 Если Форма.Объект.СпособЗаполнения = ПредопределенноеЗначение("Перечисление.СпособыЗаполненияГрафикаРаботы.ПоНеделям") Тогда 
			 Элементы.ШаблонЗаполненияЭЭ_ДеньНедели.Видимость = Ложь;
			 Элементы.ШаблонЗаполненияПредставлениеДня.Ширина = 20;
		 Иначе   
			 Элементы.ШаблонЗаполненияЭЭ_ДеньНедели.Видимость = Истина; 
			 Элементы.ШаблонЗаполненияПредставлениеДня.Ширина = 5;
		 КонецЕсли;	
	 Исключение
	 КонецПопытки;
	 // --EE:KAS 26.05.2023 MLVC-1193

 КонецПроцедуры  
 
 // ++EE:KAS 26.05.2023 MLVC-1193
 &НаСервереБезКонтекста
 Функция ЭЭ_ПолучитьПредставлениеДняНедели(ДатаДня) 
	 
	 НомерДняНедели = ДеньНедели(ДатаДня);
	 ПредставлениеДняНедели = "";
	 Если НомерДняНедели = 1 Тогда 
		 ПредставлениеДняНедели = НСтр("ru = 'Понедельник';
		 |en = 'Monday'"); 
	 ИначеЕсли НомерДняНедели = 2 Тогда
		 ПредставлениеДняНедели = НСтр("ru = 'Вторник';
		 |en = 'Tuesday'");  
	 ИначеЕсли НомерДняНедели = 3 Тогда		 
		 ПредставлениеДняНедели = НСтр("ru = 'Среда';
		 |en = 'Wednesday'"); 
	 ИначеЕсли НомерДняНедели = 4 Тогда		 
		 ПредставлениеДняНедели = НСтр("ru = 'Четверг';
		 |en = 'Thursday'");      
	 ИначеЕсли НомерДняНедели = 5 Тогда		 
		 ПредставлениеДняНедели = НСтр("ru = 'Пятница';
		 |en = 'Friday'");
	 ИначеЕсли НомерДняНедели = 6 Тогда		 
		 ПредставлениеДняНедели = НСтр("ru = 'Суббота';
		 |en = 'Saturday'");   
	 ИначеЕсли НомерДняНедели = 7 Тогда		 
		 ПредставлениеДняНедели = НСтр("ru = 'Воскресенье';
		 |en = 'Sunday'");  
	 КонецЕсли;	 
	 Возврат ПредставлениеДняНедели;
 КонецФункции
 // --EE:KAS 26.05.2023 MLVC-1193
  
 &НаКлиенте
 &После("ДатаНачалаПриИзменении")
 Процедура МЛ_ДатаНачалаПриИзменении(Элемент)
	 
	 // ++EE:KAS 09.06.2023 MLVC-1215
	 Если ЗначениеЗаполнено(Объект.ДатаНачала) Тогда
		 Объект.ДатаОтсчета = Объект.ДатаНачала;
	 КонецЕсли;	 
	 // --EE:KAS 09.06.2023 MLVC-1215  
	 
 КонецПроцедуры

 #КонецОбласти
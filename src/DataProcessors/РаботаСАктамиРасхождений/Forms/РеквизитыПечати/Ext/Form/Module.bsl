﻿
#Область СлужебныеПроцедурыИФункции

// 4D:Милавица ЕленаТ 17.03.2021
// № 28642. Разработка Регистра накопления и печатных форм для АРМ по качеству фурнитуры
// {
&После("НастроитьФормуВЗависимостиОтТипаДокумента")
Процедура МЛ_НастроитьФормуВЗависимостиОтТипаДокумента1(ДокументОбъект)
	ЕстьПравоИзменения = Ложь;
	
	Если ТипЗнч(ДокументОбъект.Ссылка) = Тип("ДокументСсылка.Чд_АктПриемкиТМЦ") Тогда
				
		Элементы.Руководитель.ОграничениеТипа     = Новый ОписаниеТипов("СправочникСсылка.ОтветственныеЛицаОрганизаций");
		Элементы.ГлавныйБухгалтер.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.ОтветственныеЛицаОрганизаций");
		Элементы.Грузоотправитель.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.Контрагенты");
		
		Элементы.СтраховаяКомпания.Видимость 				= ложь;
		Элементы.КонтрагентСтраховойКомпании.Видимость		= Ложь;
		Элементы.ГруппаВетеринарноеСвидетельство.Видимость 	= Ложь;
		Элементы.ГруппаЖелезнодорожнаяНакладная.Видимость 	= Ложь;
		Элементы.ГруппаУдостоверениеОКачестве.Видимость 	= Ложь;
		Элементы.ГруппаКоносамент.Видимость 				= Ложь;
		
		Элементы.ЧленКомиссии3.Видимость 					= Ложь;
		Элементы.ДолжностьЧленаКомиссии3.Видимость 			= Ложь;
		
		ЕстьПравоИзменения = ПравоДоступа("Изменение", Метаданные.Документы.Чд_АктПриемкиТМЦ);		
		
	КонецЕсли;
	
	ТолькоПросмотр = Не ЕстьПравоИзменения;

КонецПроцедуры

// 4D:Милавица, ЕленаТ, 21.10.2021 
// Задача №31209 'ДС7 Доработка механизма формирования печатной формы Акта приемки ТМЦ
// {
&НаСервере
Процедура ЧД_НастроитьФорму()
	// добавить группу ГруппаДокументВызова1
	
	ПараметрыЭлементаГруппа = Новый Структура;			
	ПараметрыЭлементаГруппа.Вставить("ГоризонтальноеПоложениеВГруппе", ГоризонтальноеПоложениеЭлемента.Право);

	Чд_УправлениеФормамиСервер.ДобавитьГруппуНаФорму(ЭтаФорма, 
		 "ГруппаДокументВызова1", "СтраницаСопроводительныеДокументы", "ГруппаПартнеры", , ПараметрыЭлементаГруппа, Истина);
	Чд_УправлениеФормамиСервер.ДобавитьГруппуНаФорму(ЭтаФорма, 
		 "ГруппаДокументВызова2", "СтраницаСопроводительныеДокументы", "ГруппаПартнеры", , ПараметрыЭлементаГруппа, Истина);
	Чд_УправлениеФормамиСервер.ДобавитьГруппуНаФорму(ЭтаФорма, 
		 "ГруппаДокументВызова3", "СтраницаСопроводительныеДокументы", "ГруппаПартнеры", , ПараметрыЭлементаГруппа, Истина);
		
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипСтрока(ЭтаФорма, "ВидДокументаВызоваПредставителяПартнера1", 30);
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипСтрока(ЭтаФорма, "ВидДокументаВызоваПредставителяПартнера2", 30);
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипСтрока(ЭтаФорма, "ВидДокументаВызоваПредставителяПартнера3", 30);
	
	// НомерДокументаОВызовеПредставителяПартнера
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипСтрока(ЭтаФорма, "НомерДокументаОВызовеПредставителяПартнера1");      
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипСтрока(ЭтаФорма, "НомерДокументаОВызовеПредставителяПартнера2");
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипСтрока(ЭтаФорма, "НомерДокументаОВызовеПредставителяПартнера3");

	// ДатаДокументаОВызовеПредставителяПартнера
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипДата(ЭтаФорма, "ДатаДокументаОВызовеПредставителяПартнера1", ЧастиДаты.Дата);
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипДата(ЭтаФорма, "ДатаДокументаОВызовеПредставителяПартнера2", ЧастиДаты.Дата);
	Чд_УправлениеФормамиСервер.ДобавитьРеквизитФормыТипДата(ЭтаФорма, "ДатаДокументаОВызовеПредставителяПартнера3", ЧастиДаты.Дата);
	
	//Чд_УправлениеФормамиСервер.ДобавитьПолеВводаНаФорму(ЭтаФорма,
	//			"ВидДокументаВызоваПредставителяПартнера1", "Объект.ВидДокументаВызоваПредставителяПартнера1" ,"ГруппаДокументВызова1");

	НовыйЭлемент = Элементы.Добавить("ВидДокументаВызоваПредставителяПартнера1", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова1);   
	НовыйЭлемент.ПутьКДанным    = "ВидДокументаВызоваПредставителяПартнера1";
	НовыйЭлемент.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Нет;	
	НовыйЭлемент.АвтоМаксимальнаяШирина = Ложь;
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.КнопкаВыпадающегоСписка = Истина;
	
	НовыйЭлемент = Элементы.Добавить("ВидДокументаВызоваПредставителяПартнера2", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова2);   
	НовыйЭлемент.ПутьКДанным    = "ВидДокументаВызоваПредставителяПартнера2";
	НовыйЭлемент.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Нет;	
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.КнопкаВыпадающегоСписка = Истина;

	НовыйЭлемент = Элементы.Добавить("ВидДокументаВызоваПредставителяПартнера3", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова3);   
	НовыйЭлемент.ПутьКДанным    = "ВидДокументаВызоваПредставителяПартнера3";
	НовыйЭлемент.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Нет;	
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.КнопкаВыпадающегоСписка = Истина;

	НовыйЭлемент = Элементы.Добавить("НомерДокументаОВызовеПредставителяПартнера1", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова1);   
	НовыйЭлемент.ПутьКДанным    = "НомерДокументаОВызовеПредставителяПартнера1";
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = "№";
	
	НовыйЭлемент = Элементы.Добавить("НомерДокументаОВызовеПредставителяПартнера2", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова2);   
	НовыйЭлемент.ПутьКДанным    = "НомерДокументаОВызовеПредставителяПартнера2";
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = "№";
	
	НовыйЭлемент = Элементы.Добавить("НомерДокументаОВызовеПредставителяПартнера3", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова3);   
	НовыйЭлемент.ПутьКДанным    = "НомерДокументаОВызовеПредставителяПартнера3";
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = "№";

	
	НовыйЭлемент = Элементы.Добавить("ДатаДокументаОВызовеПредставителяПартнера1", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова1);   
	НовыйЭлемент.ПутьКДанным    = "ДатаДокументаОВызовеПредставителяПартнера1";
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = "от ";
	
	НовыйЭлемент = Элементы.Добавить("ДатаДокументаОВызовеПредставителяПартнера2", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова2);   
	НовыйЭлемент.ПутьКДанным    = "ДатаДокументаОВызовеПредставителяПартнера2";
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = "от ";

	НовыйЭлемент = Элементы.Добавить("ДатаДокументаОВызовеПредставителяПартнера3", Тип("ПолеФормы"), Элементы.ГруппаДокументВызова3);   
	НовыйЭлемент.ПутьКДанным    = "ДатаДокументаОВызовеПредставителяПартнера3";
	НовыйЭлемент.Вид            = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = "от ";
	
	МассивВидДокумента = Новый Массив;
	МассивВидДокументаПредставление = Новый Массив;
	МассивВидДокумента.Добавить("Телеграмма");
	МассивВидДокументаПредставление.Добавить("Телеграмма");
	МассивВидДокумента.Добавить("Факс");
	МассивВидДокументаПредставление.Добавить("Факс");
	МассивВидДокумента.Добавить("Телефонограмма");
	МассивВидДокументаПредставление.Добавить("Телефонограмма");
	МассивВидДокумента.Добавить("Радиограмма");
	МассивВидДокументаПредставление.Добавить("Радиограмма");
	МассивВидДокумента.Добавить("ЭлектронноеСообщение");
	МассивВидДокументаПредставление.Добавить("Электронное сообщение");		

	СписокВыбора = Новый СписокЗначений;
	
	Элементы.ВидДокументаВызоваПредставителяПартнера1.СписокВыбора.Очистить();
	Элементы.ВидДокументаВызоваПредставителяПартнера2.СписокВыбора.Очистить();
	Элементы.ВидДокументаВызоваПредставителяПартнера3.СписокВыбора.Очистить();
	
	Для  Счетчик = 0 По 4 Цикл	
		Элементы.ВидДокументаВызоваПредставителяПартнера1.СписокВыбора.Добавить(МассивВидДокумента[Счетчик], МассивВидДокументаПредставление[Счетчик]);		
		Элементы.ВидДокументаВызоваПредставителяПартнера2.СписокВыбора.Добавить(МассивВидДокумента[Счетчик], МассивВидДокументаПредставление[Счетчик]);		
		Элементы.ВидДокументаВызоваПредставителяПартнера3.СписокВыбора.Добавить(МассивВидДокумента[Счетчик], МассивВидДокументаПредставление[Счетчик]);		
	КонецЦикла;

КонецПроцедуры
// } 4D

&НаСервере
&ИзменениеИКонтроль("ПриСозданииНаСервере")
Процедура МЛ_ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	Если Не Параметры.Свойство("ДокументОбъект") 
		Или Не ТипЗнч(Параметры.ДокументОбъект) = Тип("ДанныеФормыСтруктура")
		Или НЕ Параметры.ДокументОбъект.Свойство("Ссылка") Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
#Вставка
	// 4D:Милавица, ЕленаТ, 21.10.2021 
	// Задача №31209 'ДС7 Доработка механизма формирования печатной формы Акта приемки ТМЦ
	Если ТипЗнч(Параметры.ДокументОбъект.Ссылка) = Тип("ДокументСсылка.Чд_АктПриемкиТМЦ") Тогда
		ЧД_НастроитьФорму();
	КонецЕсли;
#КонецВставки

	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Параметры.ДокументОбъект,,"ПриемкаТоваров");
	ПриемкаТоваров.Загрузить(Параметры.ДокументОбъект.ПриемкаТоваров.Выгрузить());

	НастроитьФормуВЗависимостиОтТипаДокумента(Параметры.ДокументОбъект);

	Если ОбщегоНазначенияУТКлиентСервер.АвторизованВнешнийПользователь() Тогда

		Если Параметры.ДокументОбъект.Статус <> Перечисления.СтатусыАктаОРасхождениях.НеСогласовано Тогда
			ТолькоПросмотр = Истина;
		КонецЕсли;

		Элементы.СтраховаяКомпания.Видимость           = Ложь;
		Элементы.КонтрагентСтраховойКомпании.Видимость = Ложь;

	КонецЕсли;

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

// }
// 4D

#КонецОбласти

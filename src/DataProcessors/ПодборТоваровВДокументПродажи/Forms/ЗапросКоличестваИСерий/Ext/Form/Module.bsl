﻿// 4D:Милавица, АндрейБ, 04.12.2018 
// "Реализация учета материалов в разрезе паспортов кусков в 1С ERP" , № 18881
#Область ОбработчикиСобытийФормы

&НаСервере
&ИзменениеИКонтроль("ПриСозданииНаСервере")
Процедура МЛ_ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Действие = "ВсеДействия";
	РежимОтображенияСерий = "ТолькоОстатки";
	
	Дата                      = Параметры.Дата;
	Склад                     = Параметры.Склад;
	Номенклатура              = Параметры.Номенклатура;
	Характеристика            = Параметры.Характеристика;
	Упаковка 				  = Параметры.Упаковка;
	ТипНоменклатуры           = Параметры.ТипНоменклатуры;
	
	ПодборТоваров             = Параметры.ПодборТоваров;
	Если Не ПодборТоваров Тогда
		ПодборВариантовОбеспечения = Истина;
	Иначе
		ПодборВариантовОбеспечения = Параметры.ПодборВариантовОбеспечения;
	КонецЕсли;
	Параметры.Свойство("ПараметрыУказанияСерий", ПараметрыУказанияСерий);
	ПодборСерий               = ЗначениеЗаполнено(ПараметрыУказанияСерий);
	АдресТаблицыПодобраноРанее= Параметры.АдресТаблицыПодобраноРанее;
	Подразделение             = Параметры.Подразделение;
	Назначение                = Параметры.Назначение;
	КоличествоОформлено       = Параметры.КоличествоОформлено;
	Регистратор               = Параметры.Регистратор;
	Параметры.Свойство("РазборкаНаКомплектующие", РазборкаНаКомплектующие);
	
	РедактироватьВидЦены      = Параметры.РедактироватьВидЦены;
	
	БезОтбораПоВключениюНДСВЦену = Неопределено;
	Параметры.Свойство("БезОтбораПоВключениюНДСВЦену", БезОтбораПоВключениюНДСВЦену);
	
	Параметры.Свойство("СписокВыбора", СписокВыбора);
	Параметры.Свойство("ВариантыОбеспечения", ВариантыОбеспечения);
	Параметры.Свойство("ОграничиватьВариантыОбеспечения", ОграничиватьВариантыОбеспечения);
	
	Параметры.Свойство("ПроизводитсяВПроцессе", ПроизводитсяВПроцессе);
	ВыборСерии = Ложь;
	Параметры.Свойство("ВыборСерии", ВыборСерии);
	Если ВыборСерии = Неопределено Тогда
		ВыборСерии = Ложь;
	КонецЕсли;
	
	ВыборТолькоСерии = Ложь;
	Если Параметры.Свойство("ВыборТолькоСерии", ВыборТолькоСерии) Тогда
		Элементы.ОстаткиСерийКВыборуДействий.Видимость = Не ВыборТолькоСерии;
	КонецЕсли;
	
	Параметры.Свойство("ТолькоОбособленно", ТолькоОбособленно);
	
	// Установка заголовка формы
	Если ПодборТоваров Тогда
		Если Параметры.СкрытьЦену Тогда
			ЭтаФорма.Заголовок         = НСтр("ru = 'Ввод количества';
												|en = 'Enter quantity'");
		КонецЕсли;
	ИначеЕсли ВыборСерии Тогда
		ЭтаФорма.Заголовок         = НСтр("ru = 'Указание серий';
											|en = 'Specify series'");
	Иначе
		ЭтаФорма.Заголовок         = НСтр("ru = 'Выбор действия';
											|en = 'Select action'");
	КонецЕсли;
 
	// Параметры для расчета серий
	ПараметрыРасчетаРаспоряжения = Новый Структура;
	ПараметрыРасчетаРаспоряжения.Вставить("ПараметрыУказанияСерий", ПараметрыУказанияСерий);
	ПараметрыРасчетаРаспоряжения.Вставить("ЗначенияПолейДляОпределенияРаспоряжения", Новый Массив);
	
	Распоряжение = Обработки.ПодборСерийВДокументы.РаспоряжениеПоПараметрамФормы(ПараметрыРасчетаРаспоряжения);

	// Параметры номенклатуры
	НаименованиеТовара = "" + Параметры.Номенклатура +
		?(ЗначениеЗаполнено(Параметры.Характеристика),
		" (" + Параметры.Характеристика + ")",
		"");
	
	#Удаление
	СтруктураНоменклатуры = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Номенклатура,
		"ЕдиницаИзмерения, ИспользоватьУпаковки, ВидНоменклатуры");
	#КонецУдаления
	#Вставка
	// 4D:Милавица, АндрейБ,  21.08.2019 
	// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
	// {
	СтруктураНоменклатуры = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Номенклатура,
		"ЕдиницаИзмерения, ИспользоватьУпаковки, ВидНоменклатуры, Чд_УчетПаспортов, ДлинаЕдиницаИзмерения");
	#КонецВставки 
	ВидНоменклатуры = СтруктураНоменклатуры.ВидНоменклатуры;
	
	// Настройка полей колечиства и упаковок
	Если Упаковка.Пустая()
		И СтруктураНоменклатуры.ИспользоватьУпаковки Тогда 
		Упаковка = ПодборТоваровВызовСервера.ПолучитьУпаковкуХранения(Номенклатура);
	КонецЕсли;
	Если СтруктураНоменклатуры.ИспользоватьУпаковки Тогда
		Элементы.Упаковка.ПодсказкаВвода = СтруктураНоменклатуры.ЕдиницаИзмерения;
	КонецЕсли;
	#Вставка
	// 4D:Милавица, АндрейБ,  21.08.2019 
	// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
	// {
	Чд_УправлениеФормамиОбработок.ПриСозданииНаСервереПеред(ЭтаФорма, Отказ, СтандартнаяОбработка);
	Если СтруктураНоменклатуры.Чд_УчетПаспортов Тогда
		Чд_УправлениеФормамиОбработок.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	КонецЕсли;
	#КонецВставки
	ЕдиницаИзмеренияНоменклатуры = СтруктураНоменклатуры.ЕдиницаИзмерения;
	СтараяУпаковка               = Упаковка;
	КоэффициентУпаковки          = Справочники.УпаковкиЕдиницыИзмерения.КоэффициентУпаковки(Упаковка, Номенклатура);
	КоличествоУпаковок           = 1;
	
	СтруктураИзмеренийУказанияВДокументах = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Номенклатура, "ПлощадьМожноУказыватьВДокументах, ОбъемМожноУказыватьВДокументах, ВесМожноУказыватьВДокументах, ДлинаМожноУказыватьВДокументах");
	Если СтруктураИзмеренийУказанияВДокументах.ПлощадьМожноУказыватьВДокументах
		Или СтруктураИзмеренийУказанияВДокументах.ВесМожноУказыватьВДокументах
		Или СтруктураИзмеренийУказанияВДокументах.ОбъемМожноУказыватьВДокументах
		Или СтруктураИзмеренийУказанияВДокументах.ДлинаМожноУказыватьВДокументах
		Или ЗначениеЗаполнено(Номенклатура.НаборУпаковок) Тогда
		
		Элементы.Упаковка.Видимость = Истина;
		Элементы.ЕдиницаИзмерения.Видимость = Ложь;
		Элементы.Упаковка.ПодсказкаВвода = СтруктураНоменклатуры.ЕдиницаИзмерения;
	Иначе
		Элементы.Упаковка.Видимость = Ложь;
		Элементы.ЕдиницаИзмерения.Видимость = Истина;
	КонецЕсли;
	
	// Настройки склада
	Если Параметры.ТипНоменклатуры = Перечисления.ТипыНоменклатуры.Услуга Тогда
		Склад = Справочники.Склады.ПустаяСсылка();
	КонецЕсли;
	
	Если Не Параметры.Свойство("Склады")
		Или Параметры.Склады = Неопределено
		Или Параметры.Склады.Количество() = 0 Тогда
		ЗапросПоСкладам = Новый Запрос(
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
		|	Склады.Ссылка КАК Склад
		|ИЗ
		|	Справочник.Склады КАК Склады
		|ГДЕ
		|	Склады.Ссылка В ИЕРАРХИИ(&Склад)
		|	И НЕ Склады.ЭтоГруппа");
		ЗапросПоСкладам.УстановитьПараметр("Склад", Склад);
		Склады.ЗагрузитьЗначения(ЗапросПоСкладам.Выполнить().Выгрузить().ВыгрузитьКолонку("Склад"));
	Иначе
		Склады.ЗагрузитьЗначения(Параметры.Склады);
	КонецЕсли;
	
	ЭтоНабор = Параметры.ТипНоменклатуры = Перечисления.ТипыНоменклатуры.Набор;
	
	// Настройки ценовых параметров
	Если ПодборТоваров Тогда
		ВидЦены                   = Параметры.ВидЦены;
		Если ЗначениеЗаполнено(ВидЦены) Тогда
			ВидЦеныПредставление  = ВидЦены;
		Иначе
			ВидЦеныПредставление  = НСтр("ru = '<произвольная>';
										|en = '<custom>'");
		КонецЕсли;
		Цена                      = Параметры.Цена;
		Валюта                    = Параметры.Валюта;
		ЦенаВключаетНДС           = Параметры.ЦенаВключаетНДС;
		Соглашение                = Параметры.Соглашение;
		
		Если Параметры.СкрытьЦену Тогда
			Элементы.Цена.Видимость    = Ложь;
			Элементы.ВидЦеныПредставление.Видимость    = Ложь;
			Элементы.Валюта.Видимость  = Ложь;
			
			Элементы.ВидЦены.Видимость    = Ложь;
			Элементы.ЦенаНабор.Видимость    = Ложь;
			Элементы.ВалютаНабор.Видимость  = Ложь;
			
			ЭтаФорма.АвтоЗаголовок     = Ложь;
			ЭтаФорма.Заголовок         = НСтр("ru = 'Ввод количества';
												|en = 'Enter quantity'");
		КонецЕсли;
		
		Если ЭтоНабор Тогда
			
			Элементы.Цена.Видимость    = Ложь;
			Элементы.ВидЦеныПредставление.Видимость    = Ложь;
			Элементы.Валюта.Видимость  = Ложь;
			
			Элементы.ВидЦены.ТолькоПросмотр     = Не Параметры.РедактироватьВидЦены;
			Элементы.ВидЦены.ПропускатьПриВводе = Не Параметры.РедактироватьВидЦены;
			Элементы.ЦенаНабор.ТолькоПросмотр        = Не Параметры.РедактироватьЦену;
			Элементы.ЦенаНабор.ПропускатьПриВводе    = Не Параметры.РедактироватьЦену;
			
			// Заполнить список выбора видов цен.
			Запрос = Новый Запрос(
			"ВЫБРАТЬ РАЗРЕШЕННЫЕ
			|	ВидыЦен.Ссылка КАК ВидЦен
			|ИЗ
			|	Справочник.ВидыЦен КАК ВидыЦен
			|ГДЕ
			|	НЕ ВидыЦен.ПометкаУдаления
			|	И ВидыЦен.ИспользоватьПриПродаже
			|	И ВидыЦен.ЦенаВключаетНДС = &ЦенаВключаетНДС");
			
			Запрос.УстановитьПараметр("ЦенаВключаетНДС", ЦенаВключаетНДС);
			
			Элементы.ВидЦены.СписокВыбора.Очистить();
			Элементы.ВидЦены.СписокВыбора.ЗагрузитьЗначения(Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("ВидЦен"));
			Элементы.ВидЦены.СписокВыбора.Добавить(Справочники.ВидыЦен.ПустаяСсылка(), НСтр("ru = '<произвольная>';
																							|en = '<custom>'"));
			
			Элементы.ЦенаНабор.ТолькоПросмотр = ЗначениеЗаполнено(ВидЦены);
			Элементы.ЦенаНабор.ПропускатьПриВводе = ЗначениеЗаполнено(ВидЦены);
		Иначе
			
			Элементы.ВидЦены.Видимость    = Ложь;
			Элементы.ЦенаНабор.Видимость    = Ложь;
			Элементы.ВалютаНабор.Видимость  = Ложь;
			
			Элементы.Цена.Доступность = Параметры.РедактироватьВидЦены;
			
			ЗаполнитьСписокЦен();
			
		КонецЕсли;
			
		// Настроить видимость и установить значения реквизитов для редактирования ручных скидок, наценок.
		СуммаДокумента = КоличествоУпаковок * Цена;
		
		Если Не Параметры.ИспользоватьРучныеСкидкиВПродажах Или Параметры.СкрыватьРучныеСкидки Тогда
			
			Элементы.ГруппаПараметрыСкидкиНаценки.Видимость = Ложь;
			
		Иначе
			
			// Установить свойства элементов относящихся к скидкам (наценкам).
			ИспользоватьОграниченияРучныхСкидок = (ПолучитьФункциональнуюОпцию("ИспользоватьОграниченияРучныхСкидокВПродажахПоПользователям")
				Или ПолучитьФункциональнуюОпцию("ИспользоватьОграниченияРучныхСкидокВПродажахПоСоглашениям"));
			
			Если ИспользоватьОграниченияРучныхСкидок Тогда
				
				СтруктураТаблиц = ПолучитьИзВременногоХранилища(Параметры.АдресВоВременномХранилище);
				
				МаксимальныйПроцентСкидки  = СтруктураТаблиц.Ограничения[0].МаксимальныйПроцентРучнойСкидки;
				МаксимальныйПроцентНаценки = СтруктураТаблиц.Ограничения[0].МаксимальныйПроцентРучнойНаценки;
				
				Если МаксимальныйПроцентСкидки > 0 Тогда
					Элементы.ПроцентРучнойСкидкиНаценки.КнопкаСпискаВыбора = Истина;
					Элементы.ПроцентРучнойСкидкиНаценки.СписокВыбора.Добавить(МаксимальныйПроцентСкидки,
						Формат(МаксимальныйПроцентСкидки, "ЧДЦ=2"));
				КонецЕсли;
				
			КонецЕсли;
			
			Элементы.НадписьМаксимальнаяРучнаяСкидка.Видимость  = ИспользоватьОграниченияРучныхСкидок;
			Элементы.НадписьМаксимальнаяРучнаяСкидка.Заголовок = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = '%2 Макс. скидка: %1%2';
					|en = '%2 Max. discount: %1%2'"),
				МаксимальныйПроцентСкидки,
				"%");
			
			Элементы.НадписьМаксимальнаяРучнаяНаценка.Видимость = ИспользоватьОграниченияРучныхСкидок;
			Элементы.НадписьМаксимальнаяРучнаяНаценка.Заголовок = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = '%2 Макс. наценка: %1%2';
					|en = '%2 Max. markup: %1%2'"),
				МаксимальныйПроцентНаценки,
				"%");
			
			Элементы.НадписьМаксимальнаяСкидкаНеОграничена.Видимость   = Не ИспользоватьОграниченияРучныхСкидок;
			Элементы.НадписьМаксимальнаяНаценкаНеОграничена.Видимость  = Не ИспользоватьОграниченияРучныхСкидок;
			
			// Установить варианты выбора ручной скидки (наценки).
			Элементы.ВариантПредоставления.СписокВыбора.Добавить("Скидка", НСтр("ru = 'Скидка';
																				|en = 'Discount'"));
			Элементы.ВариантПредоставления.СписокВыбора.Добавить("Наценка", НСтр("ru = 'Наценка';
																				|en = 'Markup'"));
			
			// Установить значение варианта предоставления при открытии.
			ВариантПредоставления = "Скидка"; // Скидка
			Элементы.ВариантыПредоставления.ТекущаяСтраница = Элементы.ВариантыПредоставления.ПодчиненныеЭлементы.Скидка;
			
		КонецЕсли;
		
	КонецЕсли;
	
	// Настройки по типу номенклатуры
	Если ТипНоменклатуры = Перечисления.ТипыНоменклатуры.Услуга
		Или ТипНоменклатуры = Перечисления.ТипыНоменклатуры.Работа Тогда
		Если ПодборТоваров Тогда
			Элементы.ГруппаЗаказаноОбеспеченоПоСтроке.Видимость = Ложь;
			Элементы.ДекорацияОтступ4.Видимость = Ложь;
		Иначе
			Элементы.ГруппаДекорацияИнформация.Видимость = Ложь;
		КонецЕсли;
	КонецЕсли;
	
	Если ЭтоНабор Тогда
		РассчитатьПараметрыНабора();
		
		Если Не ЗначениеЗаполнено(ВариантКомплектацииНоменклатуры) Тогда
			ТекстОшибки = НСтр("ru = 'Для номенклатуры %1 не определен состав набора';
								|en = 'Set content is not determined for products %1'");
			ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				ТекстОшибки,
				Номенклатура);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки);
			Отказ = Истина;
			Возврат;
		КонецЕсли;
		
		Элементы.СоставНабора.Видимость = Истина;
		Элементы.ГруппаВНаличииДоступноПоНабору.Видимость = Истина;
		Элементы.ГруппаВНаличииДоступноПоНабору.Видимость = Истина;
		ТекущаяНоменклатура = СоставНабора[0].Номенклатура;
		ТекущаяХарактеристика = СоставНабора[0].Характеристика;
		ТекущийТипНоменклатуры = СоставНабора[0].ТипНоменклатуры;
		ТекущийВидНоменклатуры = СоставНабора[0].ВидНоменклатуры;
	Иначе
		ТекущаяНоменклатура = Номенклатура;
		ТекущаяХарактеристика = Характеристика;
		ТекущийТипНоменклатуры = ТипНоменклатуры;
		ТекущийВидНоменклатуры = ВидНоменклатуры
	КонецЕсли;
	
	// Настройки варианта открытия формы из подбора/документа
	Если Не ПодборТоваров Тогда
		Элементы.ГруппаКоличество.Видимость = Ложь;
		Элементы.ГруппаЦеновыеПараметры.Видимость = Ложь;
		Элементы.НадписьПодобратьПодобрано.Видимость = Ложь;
		Элементы.ДекорацияОтступ3.Видимость = Ложь;
		Элементы.ДекорацияОтступ4.Видимость = Ложь;
	КонецЕсли;
	
	Если Не ПодборВариантовОбеспечения
		Или ОграничиватьВариантыОбеспечения Тогда
		Элементы.Действие.Видимость = Ложь;
		Элементы.НадписьОбеспечено.Видимость = Ложь;
		Элементы.ДекорацияОтступ5.Видимость = Ложь;
	КонецЕсли;
		
	// Получение доступных остатков по действиям
	Отбор = Новый Структура("Номенклатура, Характеристика, Склад, Подразделение, Назначение, ТипНоменклатуры");
	ЗаполнитьЗначенияСвойств(Отбор, Параметры);
	Отбор.Номенклатура = ТекущаяНоменклатура;
	Отбор.Характеристика = ТекущаяХарактеристика;
	Отбор.ТипНоменклатуры = ТекущийТипНоменклатуры;
	АдресаВХранилище = Новый Структура("АдресКорректировки, АдресТаблицыПодобраноРанее"); // первый параметр передается из формы заказа, второй из формы подбора
	ЗаполнитьЗначенияСвойств(АдресаВХранилище, Параметры);
	
	Параметры.Свойство("ОтгружатьЕслиПоступилоПодНазначениеПолностью", ОтгружатьЕслиПоступилоПодНазначениеПолностью);
	Если ОтгружатьЕслиПоступилоПодНазначениеПолностью Тогда
		КоличествоТребуется = Параметры.ТекущийВариант.Количество - КоличествоОформлено;
	КонецЕсли;
	
	Если Не ПодборТоваров Тогда
		ПолучитьТаблицуДоступныхДействий(Отбор, АдресаВХранилище, Элементы.НадписьОбеспечено.Заголовок, Параметры.ВариантОбеспечения);
	Иначе
		ПолучитьТаблицуДоступныхДействий(Отбор, АдресаВХранилище, Элементы.НадписьОбеспечено.Заголовок);
	КонецЕсли;
	
	Элементы.НадписьОбеспечено.Заголовок = СтрЗаменить(Элементы.НадписьОбеспечено.Заголовок,
		"ЕдиницаИзмерения",
		ТекущаяЕдиницаИзмерения());
	
	Если ПодборВариантовОбеспечения Тогда
		ОбновитьСписокВыбораДоступныхДействий();
	КонецЕсли;
		
	// Доп. реквизиты таб. части серий
	Если ПодборСерий Тогда
		СерииИспользуются = Ложь;
		Для Каждого Строка Из ТаблицаДоступныхДействий Цикл
			Если Строка.СтатусУказанияСерий <> 0
				И НоменклатураКлиентСервер.ВЭтомСтатусеСерииУказываютсяВТЧТовары(Строка.СтатусУказанияСерий, ПараметрыУказанияСерий) Тогда
				СерииИспользуются = Истина;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		Если СерииИспользуются И Не ЭтоНабор Тогда
			УправлениеСвойствамиУТ.ДобавитьКолонкиДополнительныхРеквизитов(
				Обработки.ПодборСерийВДокументы.ВладелецСвойствСерий(ВидНоменклатуры),
				ЭтаФорма,
				"ОстаткиСерий",
				"ОстаткиСерий",
				"ОстаткиСерийСвободныйОстаток",
				Истина);
			УправлениеСвойствамиУТ.ЗаполнитьКолонкиДополнительныхРеквизитов(
				ЭтаФорма,
				"ОстаткиСерий",
				"Серия");
				
			УправлениеСвойствамиУТ.ДобавитьКолонкиДополнительныхРеквизитов(
				Обработки.ПодборСерийВДокументы.ВладелецСвойствСерий(ВидНоменклатуры),
				ЭтаФорма,
				"ОстаткиСерийТекущие",
				"ОстаткиСерийТекущие",
				"ОстаткиСерийТекущиеСвободныйОстаток",
				Истина);
			УправлениеСвойствамиУТ.ЗаполнитьКолонкиДополнительныхРеквизитов(
				ЭтаФорма,
				"ОстаткиСерийТекущие",
				"Серия");
			КонецЕсли;
	КонецЕсли;
	
	// Выделение строк из заказа
	Если Не ПодборТоваров Тогда
		
		Если Параметры.ТипНоменклатуры = Перечисления.ТипыНоменклатуры.Товар
			Или Параметры.ТипНоменклатуры = Перечисления.ТипыНоменклатуры.МногооборотнаяТара Тогда
			ТекущийСклад = Параметры.СкладВТЧ;
		КонецЕсли;
		
		ТекущийВариантОбеспечения = Параметры.ВариантОбеспечения;
		
		Отбор = Новый Структура;
		Если ЗначениеЗаполнено(ТекущийСклад) Тогда
			Отбор.Вставить("Склад", ТекущийСклад);
		КонецЕсли;
		Отбор.Вставить("ВариантОбеспечения", ТекущийВариантОбеспечения);
		
		Если ТекущийВариантОбеспечения = Перечисления.ВариантыОбеспечения.ИзЗаказов Тогда
			Отбор.Вставить("ДатаОтгрузки", Параметры.ТекущийВариант.ДатаДоступности);
		КонецЕсли;
		
		НайденныеСтроки = ТаблицаДоступныхДействий.НайтиСтроки(Отбор);
		
		Если Отбор.Свойство("ДатаОтгрузки") Тогда
			Отбор.Удалить("ДатаОтгрузки");
		КонецЕсли;
		
		Если НайденныеСтроки.Количество()=0 Тогда
			Отбор.Вставить("ВариантОбеспечения", Перечисления.ВариантыОбеспечения.Требуется);
			НайденныеСтроки = ТаблицаДоступныхДействий.НайтиСтроки(Отбор);
		КонецЕсли;
		Если НайденныеСтроки.Количество()=0 Тогда
			Отбор.Вставить("ВариантОбеспечения", Перечисления.ВариантыОбеспечения.Обособленно);
			НайденныеСтроки = ТаблицаДоступныхДействий.НайтиСтроки(Отбор);
		КонецЕсли;
		
		// Если Вариант обеспечения, используемый по умолчанию не доступен,
		// то подберем любой доступный вариант обеспечения
		Если НайденныеСтроки.Количество() = 0 Тогда
			Отбор.Удалить("ВариантОбеспечения");
			НайденныеСтроки = ТаблицаДоступныхДействий.НайтиСтроки(Отбор);
		КонецЕсли;
		
		// Если Склад не достпен по РЛС, то подберем любой доступный склад.
		Если НайденныеСтроки.Количество()=0 Тогда
			Если Параметры.ВариантОбеспечения = Перечисления.ВариантыОбеспечения.Обособленно
				Или Параметры.ВариантОбеспечения = Перечисления.ВариантыОбеспечения.ОтгрузитьОбособленно Тогда
				Отбор.Вставить("ВариантОбеспечения", Перечисления.ВариантыОбеспечения.Обособленно);
			Иначе
				Отбор.Вставить("ВариантОбеспечения", Перечисления.ВариантыОбеспечения.Требуется);
			КонецЕсли;
			Отбор.Удалить("Склад");
			НайденныеСтроки = ТаблицаДоступныхДействий.НайтиСтроки(Отбор);
			
			// Если Вариант обеспечения по умолчанию не доступен, то подберем любой доступный вариант обеспечения
			Если НайденныеСтроки.Количество() = 0 Тогда
				Отбор.Удалить("ВариантОбеспечения");
				НайденныеСтроки = ТаблицаДоступныхДействий.НайтиСтроки(Отбор);
			КонецЕсли;
			
		КонецЕсли;
		
		Если НайденныеСтроки.Количество() > 0 Тогда
			СтрокаЗаказа = НайденныеСтроки[0];
			СтрокаЗаказа.СтрокаЗаказа = 1;
			СтрокаЗаказа.Количество = Параметры.Количество;
			СтрокаЗаказа.ПроизводитсяВПроцессе = ПроизводитсяВПроцессе;
			ТекущееКоличествоПодобрано = Параметры.Количество;
		КонецЕсли;
		
		Если ПодборСерий 
			И НайденныеСтроки.Количество() > 0 Тогда
			ВариантПолучениеДанныхИзРегистров = Обработки.ПодборСерийВДокументы.ВариантПолучениеДанныхИзРегистровПоПараметрамФормы(ПараметрыУказанияСерий,
				Распоряжение,
				ТекущийСклад,
				ВидНоменклатуры);
			Если ТекущийВариантОбеспечения = Перечисления.ВариантыОбеспечения.Отгрузить 
				Или ТекущийВариантОбеспечения = Перечисления.ВариантыОбеспечения.ОтгрузитьОбособленно
				Или ТекущийВариантОбеспечения = Перечисления.ВариантыОбеспечения.СоСклада Тогда
				ВыполнитьЗапросЗаполненияТаблицыОстатков();
				
				Если РазборкаНаКомплектующие Тогда
					Для Каждого СтруктураСерия Из Параметры.Серия Цикл
						
						Отбор.Вставить("Серия", СтруктураСерия.Серия);
						Отбор.Вставить("ВариантОбеспечения", ТекущийВариантОбеспечения);
						НайденныеСтроки = ОстаткиСерий.НайтиСтроки(Отбор);
						Если НайденныеСтроки.Количество()>0 Тогда
							НайденныеСтроки[0].Количество = СтруктураСерия.Количество;
							НайденныеСтроки[0].СвободныйОстаток = НайденныеСтроки[0].СвободныйОстаток - СтруктураСерия.Количество;
							НайденныеСтроки[0].СтрокаЗаказа =1;
							СтрокаЗаказа.КоличествоСерий = СтрокаЗаказа.КоличествоСерий + СтруктураСерия.Количество;
						КонецЕсли;
						
					КонецЦикла;
				Иначе
					Отбор.Вставить("Серия", Параметры.Серия);
					Отбор.Вставить("ВариантОбеспечения", ТекущийВариантОбеспечения);
					
					НайденныеСтроки = ОстаткиСерий.НайтиСтроки(Отбор);
					Если НайденныеСтроки.Количество()>0 Тогда
						НайденныеСтроки[0].Количество = Параметры.Количество;
						НайденныеСтроки[0].СвободныйОстаток = НайденныеСтроки[0].СвободныйОстаток-Параметры.Количество;
						НайденныеСтроки[0].СтрокаЗаказа =1;
						СтрокаЗаказа.КоличествоСерий = Параметры.Количество;
					КонецЕсли;
						
				КонецЕсли;
				ПересчитатьСтатусУказанияСерий(СтрокаЗаказа,
					ПараметрыУказанияСерий,
					СтрокаЗаказа.Количество,
					СтрокаЗаказа.КоличествоСерий);
			КонецЕсли;
		КонецЕсли;	
	КонецЕсли;
	
	// Вывод даннах на форму
	РасчитатьДоступноеКоличество();
	УстановитьОтборПоДействиюИТовару();
	
	
	Если ПодборТоваров Тогда
		ОбновитьКоличество();
	Иначе
		Количество = Параметры.Количество;
		ОбновитьПодобранноеКоличесто();
		ОбновитьКоличествоУпаковок();
	КонецЕсли;
	
	Если ТаблицаДоступныхДействийТекущая.Количество() > 0 Тогда
		
		Если Не ПодборТоваров Тогда
			СтрокаЗаказа = ТаблицаДоступныхДействийТекущая.НайтиСтроки(Новый Структура("СтрокаЗаказа",1))[0];
			ИдентификаторТекущейСтрокиТаблицаДоступныхДействийТекущая = СтрокаЗаказа.ПолучитьИдентификатор();
			Элементы.ТаблицаДоступныхДействийТекущая.ТекущаяСтрока = ИдентификаторТекущейСтрокиТаблицаДоступныхДействийТекущая;
			
			Если ВыборСерии Тогда
				КУказаниюСерийНаСервере();
			КонецЕсли;
		КонецЕсли;
		
		Если ЕдинственныйСклад И Не ЭтоНабор Тогда
			Если ТаблицаДоступныхДействийТекущая[0].СтатусУказанияСерий <> 0
				И ЗначениеЗаполнено(ТаблицаДоступныхДействийТекущая[0].ВариантОбеспечения) Тогда
				КУказаниюСерийНаСервере();
			КонецЕсли;
		КонецЕсли;
		
	КонецЕсли;
	
	УстановитьВидимостьКоличествоЕдиницХранения();
	
КонецПроцедуры

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции

&НаСервере
&ИзменениеИКонтроль("СформироватьПодобранныеТовары")
Функция МЛ_СформироватьПодобранныеТовары()
	
	ПодобранныеТовары = Новый Массив;
	
	Если ЭтоНабор Тогда
		
		Для Каждого ПозицияНабора Из СоставНабора Цикл
			
			Отбор = Новый Структура;
			Отбор.Вставить("Номенклатура", ПозицияНабора.Номенклатура);
			Отбор.Вставить("Характеристика", ПозицияНабора.Характеристика);
			
			ПараметрыКомплектующей = ПараметрыТовара();
			ЗаполнитьЗначенияСвойств(ПараметрыКомплектующей, ПозицияНабора);
			КоэффициентУпаковкиКомплектующей = ПозицияНабора.КоэффициентУпаковки;
			
			ПараметрыКомплектующей.Цена = ПозицияНабора.Цена;
			ПараметрыКомплектующей.ВидЦены = ВидЦены;
			Если ВариантПредоставления = "Скидка" Тогда
				ПараметрыКомплектующей.ПроцентРучнойСкидки = ПроцентРучнойСкидкиНаценки;
			Иначе
				ПараметрыКомплектующей.ПроцентРучнойСкидки = -ПроцентРучнойСкидкиНаценки;
			КонецЕсли;
			
			Если Склады.Количество() = 1 Тогда
				ПараметрыКомплектующей.Вставить("Склад", Склады.Получить(0).Значение);
			КонецЕсли;
	
			Для Каждого СтрокаТовары Из ТаблицаДоступныхДействий.НайтиСтроки(Отбор) Цикл
				Если СтрокаТовары.Количество = 0 Тогда
					Продолжить;
				КонецЕсли;
				ПараметрыТовара = ПараметрыТовара();
				ЗаполнитьЗначенияСвойств(ПараметрыТовара, СтрокаТовары);
				ПараметрыТовара.Упаковка = ПозицияНабора.Упаковка;
				
				ПараметрыТовара.Цена = ПозицияНабора.Цена;
				ПараметрыТовара.ВидЦены = ВидЦены;
				
				Если ВариантПредоставления = "Скидка" Тогда
					ПараметрыТовара.ПроцентРучнойСкидки = ПроцентРучнойСкидкиНаценки;
				Иначе
					ПараметрыТовара.ПроцентРучнойСкидки = -ПроцентРучнойСкидкиНаценки;
				КонецЕсли;
				
				СтруктураОтбора = Новый Структура;
				СтруктураОтбора.Вставить("Номенклатура", СтрокаТовары.Номенклатура);
				СтруктураОтбора.Вставить("Характеристика", СтрокаТовары.Характеристика);
				СтруктураОтбора.Вставить("Склад", СтрокаТовары.Склад);
				СтруктураОтбора.Вставить("ВариантОбеспечения", СтрокаТовары.ВариантОбеспечения);
				СтрокиСерий = ОстаткиСерий.НайтиСтроки(СтруктураОтбора);
				
				Если СтрокиСерий.Количество()>0 Тогда
					СтрокаДобавлена = Ложь;
					Для Каждого СтрокаСерии Из СтрокиСерий Цикл
						Если СтрокаСерии.Количество = 0 Тогда
							Продолжить;
						КонецЕсли;
						
						СтрокаССерией = ПараметрыТовара();
						ЗаполнитьЗначенияСвойств(СтрокаССерией,ПараметрыТовара);
						СтрокаССерией.КоличествоУпаковок = СтрокаСерии.Количество/КоэффициентУпаковкиКомплектующей;
						СтрокаССерией.Количество = СтрокаСерии.Количество;
						СтрокаССерией.Серия = СтрокаСерии.Серия;
						ПодобранныеТовары.Добавить(СтрокаССерией);
						СтрокаДобавлена = Истина;
					КонецЦикла;
					
					Если Не СтрокаДобавлена Тогда
						ПараметрыТовара.КоличествоУпаковок = СтрокаТовары.Количество/КоэффициентУпаковкиКомплектующей;
						ПодобранныеТовары.Добавить(ПараметрыТовара);
					КонецЕсли;
				Иначе
					ПараметрыТовара.КоличествоУпаковок = СтрокаТовары.Количество/КоэффициентУпаковкиКомплектующей;
					ПодобранныеТовары.Добавить(ПараметрыТовара);
				КонецЕсли;
			КонецЦикла;
			
			Если ПозицияНабора.КоличествоПодобрать > ПозицияНабора.КоличествоПодобрано Тогда
				ПараметрыКомплектующей.КоличествоУпаковок = ПозицияНабора.КоличествоПодобрать - ПозицияНабора.КоличествоПодобрано;
				ПараметрыКомплектующей.Количество = ПараметрыКомплектующей.КоличествоУпаковок * КоэффициентУпаковкиКомплектующей;
				ПодобранныеТовары.Добавить(ПараметрыКомплектующей);
			КонецЕсли;
			
		КонецЦикла;
		
	Иначе
		
		ОсновнаяСтрока = ПараметрыТовара();
		ЗаполнитьЗначенияСвойств(ОсновнаяСтрока, ЭтаФорма);
		ОсновнаяСтрока.ВариантОбеспечения = ТекущийВариантОбеспечения;
		Если ОграничиватьВариантыОбеспечения Тогда
			ОсновнаяСтрока.ВариантОбеспечения =
				ВариантыОбеспечения[ОбеспечениеКлиентСервер.ТипНоменклатурыСтрокой(ОсновнаяСтрока.ТипНоменклатуры)];
		КонецЕсли;
		Если ВариантПредоставления = "Скидка" Тогда
			ОсновнаяСтрока.ПроцентРучнойСкидки = ПроцентРучнойСкидкиНаценки;
		Иначе
			ОсновнаяСтрока.ПроцентРучнойСкидки = -ПроцентРучнойСкидкиНаценки;
		КонецЕсли;
		Если Склады.Количество()=1 Тогда
			ОсновнаяСтрока.Склад = Склады[0].Значение;
		Иначе
		#Удаление 
			ОсновнаяСтрока.Склад = Справочники.Склады.ПустаяСсылка();
		#КонецУдаления 
		#Вставка 
		// 4D:Милавица, Михаил, 05.03.2020 
		// Доработка механизмов отражения документов в рабочем месте Передача в переработку, №24239
		// {
		ОсновнаяСтрока.Склад = ТекущийСклад;
		// }
		// 4D
		#КонецВставки 			
		КонецЕсли;
		
		Для Каждого СтрокаТовары Из ТаблицаДоступныхДействий Цикл
			Если СтрокаТовары.Количество = 0 Тогда
				Продолжить;
			КонецЕсли;
			
			ПараметрыТовара = ПараметрыТовара();
			ЗаполнитьЗначенияСвойств(ПараметрыТовара, СтрокаТовары);
			ПараметрыТовара.Упаковка = Упаковка;
			ПараметрыТовара.ТипНоменклатуры = ТипНоменклатуры;
			ПараметрыТовара.Цена = Цена;
			ПараметрыТовара.ВидЦены = ВидЦены;
			
			Если ВариантПредоставления = "Скидка" Тогда
				ПараметрыТовара.ПроцентРучнойСкидки = ПроцентРучнойСкидкиНаценки;
			Иначе
				ПараметрыТовара.ПроцентРучнойСкидки = -ПроцентРучнойСкидкиНаценки;
			КонецЕсли;
			
			СтруктураОтбора = Новый Структура;
			СтруктураОтбора.Вставить("Номенклатура", СтрокаТовары.Номенклатура);
			СтруктураОтбора.Вставить("Характеристика", СтрокаТовары.Характеристика);
			СтруктураОтбора.Вставить("Склад", СтрокаТовары.Склад);
			СтруктураОтбора.Вставить("ВариантОбеспечения", СтрокаТовары.ВариантОбеспечения);
			СтрокиСерий = ОстаткиСерий.НайтиСтроки(СтруктураОтбора);
			
			Если СтрокиСерий.Количество()>0 Тогда
				СтрокаДобавлена = Ложь;
				Для Каждого СтрокаСерии Из СтрокиСерий Цикл
					Если СтрокаСерии.Количество = 0 Тогда
						Продолжить;
					КонецЕсли;
					
					СтрокаССерией = ПараметрыТовара();
					ЗаполнитьЗначенияСвойств(СтрокаССерией,ПараметрыТовара);
					СтрокаССерией.КоличествоУпаковок = СтрокаСерии.Количество/КоэффициентУпаковки;
					СтрокаССерией.Количество = СтрокаСерии.Количество;
					Если СтрокаСерии.СтрокаЗаказа Тогда
						ПараметрыТовара.Отгружено = КоличествоОформлено;
					КонецЕсли;
					СтрокаССерией.Серия = СтрокаСерии.Серия;
					ПодобранныеТовары.Добавить(СтрокаССерией);
					СтрокаДобавлена = Истина;
					ОсновнаяСтрока.КоличествоУпаковок = ОсновнаяСтрока.КоличествоУпаковок - СтрокаССерией.КоличествоУпаковок;
				КонецЦикла;
				
				Если Не СтрокаДобавлена Тогда
					ПараметрыТовара.КоличествоУпаковок = СтрокаТовары.Количество/КоэффициентУпаковки;
					Если СтрокаТовары.СтрокаЗаказа Тогда
						ПараметрыТовара.Отгружено = КоличествоОформлено;
					КонецЕсли;
					ПодобранныеТовары.Добавить(ПараметрыТовара);
					ОсновнаяСтрока.КоличествоУпаковок = ОсновнаяСтрока.КоличествоУпаковок - ПараметрыТовара.КоличествоУпаковок;
				КонецЕсли;
			Иначе
				ПараметрыТовара.КоличествоУпаковок = СтрокаТовары.Количество/КоэффициентУпаковки;
				Если СтрокаТовары.СтрокаЗаказа Тогда
					ПараметрыТовара.Отгружено = КоличествоОформлено;
				КонецЕсли;
				ПодобранныеТовары.Добавить(ПараметрыТовара);
				ОсновнаяСтрока.КоличествоУпаковок = ОсновнаяСтрока.КоличествоУпаковок - ПараметрыТовара.КоличествоУпаковок;
			КонецЕсли;
		КонецЦикла;
		
		Если Не РазборкаНаКомплектующие
			И Окр(ОсновнаяСтрока.КоличествоУпаковок, 3) > 0 Тогда
			
			ОсновнаяСтрока.Количество = ОсновнаяСтрока.КоличествоУпаковок * КоэффициентУпаковки;
			ПодобранныеТовары.Добавить(ОсновнаяСтрока);
			
		КонецЕсли;
		
	КонецЕсли;
	
	МаксимальнаяДатаОтгрузки = '00010101';
	Для Каждого Элемент Из ПодобранныеТовары Цикл
		
		Если Элемент.Отгружено = 0 Тогда
			МаксимальнаяДатаОтгрузки = Макс(МаксимальнаяДатаОтгрузки, Элемент.ДатаОтгрузки);
		КонецЕсли;
		
	КонецЦикла;
	Результат = Новый Структура("ПодобранныеТовары, МаксимальнаяДатаОтгрузки", ПодобранныеТовары, МаксимальнаяДатаОтгрузки);
	
	Возврат Результат;
	
КонецФункции

&НаКлиенте
Процедура Чд_ОстаткиСерийТекущиеКоличествоУпаковокПриИзменении(Элемент)
	
	ОчиститьСообщения();
	ТекСтрока = Элементы.ОстаткиСерийТекущие.ТекущиеДанные;
	Если ТекСтрока.КоличествоУпаковок <> ТекСтрока.Чд_СвободныйОстатокВУпаковках Тогда
		ТекстСообщения = НСтр("ru='Доступен подбор только полного количества по паспорту %1 м!'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, ТекСтрока.Чд_СвободныйОстатокВУпаковках);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		
		ТекСтрока.КоличествоУпаковок = ТекСтрока.Чд_СвободныйОстатокВУпаковках;
	КонецЕсли;
	
	ТекСтрока.Количество = ТекСтрока.КоличествоУпаковок * ?(ТекСтрока.Чд_КоэффициентУпаковкиПоПаспорту > 0,
												ТекСтрока.Чд_КоэффициентУпаковкиПоПаспорту, КоэффициентУпаковки);
	ОстаткиСерийТекущиеКоличествоПриИзмененииНаСервере(Элементы.ОстаткиСерийТекущие.ТекущаяСтрока);
	
КонецПроцедуры

&НаСервере
&ИзменениеИКонтроль("ПерейтиКВыборуДействий")
Процедура МЛ_ПерейтиКВыборуДействий(Отказ)
	
	ТекущиеДанные = ТаблицаДоступныхДействийТекущая.НайтиПоИдентификатору(ИдентификаторТекущейСтрокиТаблицаДоступныхДействийТекущая);
	#Вставка
	// 4D:Милавица, АндрейБ,  21.08.2019 
	// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
	// { обход ошибки
	Если ТекущиеДанные = Неопределено Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	#КонецВставки

	ВведенноеКоличествоСерий = 0;
	
	Отбор = Новый Структура();
	Отбор.Вставить("Номенклатура",ТекущиеДанные.Номенклатура);
	Отбор.Вставить("Характеристика",ТекущиеДанные.Характеристика);
	Отбор.Вставить("ВариантОбеспечения",ТекущиеДанные.ВариантОбеспечения);
	Отбор.Вставить("Склад",ТекущиеДанные.Склад);
	НайденныеСтроки = ОстаткиСерий.НайтиСтроки(Отбор);
	
	Для Каждого СтрокаСерийТекущая Из НайденныеСтроки Цикл
		ВведенноеКоличествоСерий = ВведенноеКоличествоСерий + СтрокаСерийТекущая.Количество;
	КонецЦикла;
	
	#Удаление
	Если Не ПодборТоваров Тогда
		
		СтрокаЗаказа = ТаблицаДоступныхДействий.НайтиСтроки(Новый Структура("СтрокаЗаказа",1))[0];
		ДопустимоеКоличествоСерий = ?(ТекущиеДанные.СтрокаЗаказа = 1, 0, СтрокаЗаказа.Количество - КоличествоОформлено) + ТекущиеДанные.Количество;
		
		Если ВведенноеКоличествоСерий > ДопустимоеКоличествоСерий Тогда
			ТекстСообщения = НСтр("ru = 'Введено серий больше чем требуется.';
									|en = 'You have entered more series than it is required.'");
			
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Введено серий больше чем требуется. Требуется указать %1 %2';
					|en = 'You have entered more series than it is required. It is required to specify %1 %2'"),
				ДопустимоеКоличествоСерий,
				ЕдиницаИзмеренияНоменклатуры);
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,,,Отказ);
			Возврат;
		КонецЕсли;
	КонецЕсли;
	#КонецУдаления
	#Вставка
	// 4D:Милавица, АндрейБ,  21.08.2019 
	// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
	// {	
	Если Не ПодборТоваров
		И НЕ ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "Чд_УчетПаспортов") Тогда
		
		СтрокаЗаказа = ТаблицаДоступныхДействий.НайтиСтроки(Новый Структура("СтрокаЗаказа",1))[0];
		ДопустимоеКоличествоСерий = ?(ТекущиеДанные.СтрокаЗаказа = 1, 0, СтрокаЗаказа.Количество - КоличествоОформлено) + ТекущиеДанные.Количество;

		Если ВведенноеКоличествоСерий > ДопустимоеКоличествоСерий Тогда
			ТекстСообщения = НСтр("ru='Введено серий больше чем требуется.'");
	
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Введено серий больше чем требуется. Требуется указать %1 %2'"),
			ДопустимоеКоличествоСерий,
			ЕдиницаИзмеренияНоменклатуры);

			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,,,Отказ);
			Возврат;
		КонецЕсли;
	КонецЕсли;
	#КонецВставки

	Если ТекущиеДанные.СтрокаЗаказа Тогда
		ДополнитьСтрокуЗаказаКоличеством = ТекущиеДанные.Количество - ВведенноеКоличествоСерий;
		Если ДополнитьСтрокуЗаказаКоличеством<>0 Тогда
			
			Отбор = Новый Структура("СтрокаЗаказа", 1);
			НайденныеСтроки = ОстаткиСерий.НайтиСтроки(Отбор);
			Если НайденныеСтроки.Количество() > 0 Тогда
				
				СтрокаЗаказаСерий = НайденныеСтроки[0];
				
				Если Не РазборкаНаКомплектующие
					Или (РазборкаНаКомплектующие
						И СтрокаЗаказаСерий.Количество > 0) Тогда
					
					СтрокаЗаказаСерий.Количество = СтрокаЗаказаСерий.Количество + ДополнитьСтрокуЗаказаКоличеством;
					#Вставка
					// 4D:Милавица, АндрейБ, 31.05.2019 
					// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
					// {выявлена некорректная работа при переходе по команде "ОстаткиСерийКВыборуДействий"
					СтрокаЗаказаСерий.СвободныйОстаток = СтрокаЗаказаСерий.СвободныйОстаток - ДополнитьСтрокуЗаказаКоличеством;
					#КонецВставки
					ВведенноеКоличествоСерий = ВведенноеКоличествоСерий + ДополнитьСтрокуЗаказаКоличеством;
					
				КонецЕсли;
				
			КонецЕсли;
		КонецЕсли;
	Иначе
		ТекущиеДанные.Количество = ВведенноеКоличествоСерий;
	КонецЕсли;
	
	ТекущиеДанные.КоличествоСерий = ВведенноеКоличествоСерий;
	
	Элементы.СтраницыТоварыСерии.ТекущаяСтраница = Элементы.СтраницаТовары;
	
	ПриИзмененииПодобранногоКоличестваНаСервере(ИдентификаторТекущейСтрокиТаблицаДоступныхДействийТекущая);
	
	НастроитьВидимостьНаСервере();
	
КонецПроцедуры

&НаСервере
&ИзменениеИКонтроль("СфоримроватьНадписьПодобратьПодобрано")
Процедура МЛ_СфоримроватьНадписьПодобратьПодобрано()
	
	Если ЭтоНабор Тогда
		Отбор = Новый Структура();
		Отбор.Вставить("Номенклатура",ТекущаяНоменклатура);
		Отбор.Вставить("Характеристика",ТекущаяХарактеристика);
		ТекущаяСтрокаНабора = СоставНабора.НайтиСтроки(Отбор)[0];
		
		Элементы.НадписьПодобратьПодобрано.Заголовок = 
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Требуется подобрать - %1 %3. Подобрано - %2 %3';
					|en = 'Required to pick %1 %3. Picked - %2 %3'"),
				Количество*ТекущаяСтрокаНабора.Количество,
				КоличествоПодобрано,
				ТекущаяСтрокаНабора.ЕдиницаИзмерения);
	Иначе
		Если Не ПодборТоваров Тогда
			Отбор = Новый Структура();
			Отбор.Вставить("СтрокаЗаказа",1);
			СтрокиЗаказа = ТаблицаДоступныхДействий.НайтиСтроки(Отбор);
			
			Если СтрокиЗаказа.Количество() > 0 Тогда
				СтрокаЗаказа = СтрокиЗаказа[0];
				КоличествоЗаказ = СтрокаЗаказа.Количество;
			Иначе
				КоличествоЗаказ = 0;
			КонецЕсли;
			
			Элементы.НадписьПодобратьПодобрано.Заголовок = 
			СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
					НСтр("ru = 'Требуется распределить - %1 %3. Распределено на другой вариант обеспечения - %2 %3';
						|en = 'To allocate - %1 %3. Allocated to another supply option - %2 %3'"),
					Количество,
					КоличествоПодобрано - КоличествоЗаказ,
					ЕдиницаИзмеренияНоменклатуры);
		Иначе
			Элементы.НадписьПодобратьПодобрано.Заголовок = 
			СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
					НСтр("ru = 'Требуется подобрать - %1 %3. Подобрано - %2 %3';
						|en = 'Required to pick %1 %3. Picked - %2 %3'"),
					Количество,
					КоличествоПодобрано,
					ЕдиницаИзмеренияНоменклатуры);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

// 4D:Милавица, СветланаА, 24.09.2021 
// ДОРАБОТКА. Ввод единицы количества повторным нажатием стрелочки при подборе не происходит, № 30829
// { Добавить возможность при двойном клике подбора ввода единиц количества
&НаКлиенте
Процедура МЛ_ТаблицаДоступныхДействийТекущаяВыборПосле(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Элемент.ТекущийЭлемент.Имя = "ТаблицаДоступныхДействийТекущаяПодобратьКоличество" Тогда
		СтандартнаяОбработка = Ложь;
		
		Если ПрекратитьПодбор Тогда
			ПрекратитьПодбор = Ложь;
			Элемент.ТекущийЭлемент = Элементы.ТаблицаДоступныхДействийТекущаяКоличество;
			Возврат;
		КонецЕсли;
		
		ТекущиеДанные = Элементы.ТаблицаДоступныхДействийТекущая.ТекущиеДанные;
		Если ТекущиеДанные = Неопределено Тогда
			Возврат;
		КонецЕсли;
		
		ТаблицаДоступныхДействийТекущаяПодобратьКоличество(ТекущиеДанные.ПолучитьИдентификатор());
		
	КонецЕсли;
	
КонецПроцедуры
// }
// 4D

#КонецОбласти
// }
// 4D
﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ОбработчикиСобытий

&ИзменениеИКонтроль("ОбработкаЗаполнения")
Процедура МЛ_ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)

#Вставка
// 4D:Милавица, Михаил, 15.02.2019 21:51:57 
// Внутреннее потребление товаров (списание на расходы) на основании заказа на перемещение товаров, №21531
// {
	Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ЗаказНаПеремещение") 
		Или ТипЗнч(ДанныеЗаполнения) = Тип("Структура")
		И ДанныеЗаполнения.Свойство("ДокументОснование")
		И ТипЗнч(ДанныеЗаполнения.ДокументОснование) = Тип("ДокументСсылка.ЗаказНаПеремещение") Тогда
		
		Чд_ЗаполнитьНаОснованииЗаказаНаПеремещение(ДанныеЗаполнения);
	КонецЕсли;
#КонецВставки
	Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ПриобретениеТоваровУслуг") 
		Или ТипЗнч(ДанныеЗаполнения) = Тип("Структура")
		И ДанныеЗаполнения.Свойство("ДокументОснование")
		И ТипЗнч(ДанныеЗаполнения.ДокументОснование) = Тип("ДокументСсылка.ПриобретениеТоваровУслуг") Тогда

		ЗаполнитьНаОснованииПоступления(ДанныеЗаполнения);

		//++ НЕ УТКА
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.РегистрацияДефекта") Тогда

		ДокументОснование = ДанныеЗаполнения;

		//-- НЕ УТКА
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("Структура") И ДанныеЗаполнения.Свойство("Товары") Тогда

		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения.РеквизитыШапки);
		Товары.Загрузить(ДанныеЗаполнения.Товары.Выгрузить());

		ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ВнутреннееПотреблениеТоваров);
		НоменклатураСервер.ЗаполнитьСерииПоFEFO(ЭтотОбъект,ПараметрыУказанияСерий, Ложь);	
		
#Вставка
// 4D:Милавица, Анастасия, 22.01.2019 14:01:54 
// Возможность "ввода на основании", № 21274, 21273, 21369, 21932   
// {
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("Структура") И ДанныеЗаполнения.Свойство("ДокументОснование")
		и ТипЗнч(ДанныеЗаполнения.ДокументОснование) = Тип("ДокументСсылка.ПеремещениеТоваров")Тогда
		
		ДокументОснование = ДанныеЗаполнения.ДокументОснование;
		
		Подразделение = ДанныеЗаполнения.ДокументОснование.СкладПолучатель.Подразделение;
		Склад = ДанныеЗаполнения.ДокументОснование.СкладПолучатель;
		
		Товары.Загрузить(ДанныеЗаполнения.ДокументОснование.Товары.Выгрузить());
		Для каждого СтрокаТовары Из Товары Цикл
			СтрокаТовары.КодСтроки = 0;
		КонецЦикла; 
		
		Дата = ТекущаяДатаСеанса() + 300;
		
		ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ВнутреннееПотреблениеТоваров);
		НоменклатураСервер.ЗаполнитьСерииПоFEFO(ЭтотОбъект,ПараметрыУказанияСерий, Ложь);
		
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("Структура") И ДанныеЗаполнения.Свойство("ДокументОснование")
		и ТипЗнч(ДанныеЗаполнения.ДокументОснование) = Тип("ДокументСсылка.ПрочееОприходованиеТоваров")Тогда
		
		Если ДанныеЗаполнения.ДокументОснование.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ОприходованиеЗаСчетДоходов Тогда
			ДокументОснование = ДанныеЗаполнения.ДокументОснование;
			Подразделение = ?(ЗначениеЗаполнено(ДанныеЗаполнения.ДокументОснование.Подразделение), ДанныеЗаполнения.ДокументОснование.Подразделение, ПараметрыСеанса.ТекущийПользователь.Подразделение);
			Склад = ДанныеЗаполнения.ДокументОснование.Склад;
			Товары.Загрузить(ДанныеЗаполнения.ДокументОснование.Товары.Выгрузить());
			Для каждого СтрокаТовары Из Товары Цикл
				СтрокаТовары.КодСтроки = 0;
			КонецЦикла; 		
			Дата = ТекущаяДатаСеанса() + 300;
			
			ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ВнутреннееПотреблениеТоваров);
			НоменклатураСервер.ЗаполнитьСерииПоFEFO(ЭтотОбъект,ПараметрыУказанияСерий, Ложь);
		КонецЕсли; 	
#КонецВставки
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ИнвентаризацияПродукцииВЕТИС") Тогда
	
		ИнтеграцияВЕТИСУТ.ЗаполнитьВнутреннееПотреблениеТоваровНаОснованииИнвентаризацииПродукцииВЕТИС(ЭтотОбъект, ДанныеЗаполнения);
	
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ВыводИзОборотаИСМП") Тогда
		
		ИнтеграцияИСМПУТ.ЗаполнитьВнутреннееПотреблениеТоваровНаОснованииВыводаИзОборотаИСМП(ЭтотОбъект, ДанныеЗаполнения);

	Иначе // Заполнение по заказу(-ам).

		Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") И ДанныеЗаполнения.Свойство("МассивЗаказов") Тогда

			СтруктураЗаполнения = ДанныеЗаполнения;

		КонецЕсли;

		Если СтруктураЗаполнения <> Неопределено Тогда

			ЗаполнитьПоЗаказу(СтруктураЗаполнения);

		КонецЕсли;

	КонецЕсли;

	ИнициализироватьДокумент(ДанныеЗаполнения);

	ВнутреннееПотреблениеТоваровЛокализация.ОбработкаЗаполнения(ЭтотОбъект, ДанныеЗаполнения, СтандартнаяОбработка);

КонецПроцедуры

&ИзменениеИКонтроль("ОбработкаПроведения")
Процедура МЛ_ОбработкаПроведения(Отказ, РежимПроведения)

	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	Документы.ВнутреннееПотреблениеТоваров.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	ПроведениеСерверУТ.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ЗапасыСервер.ОтразитьТоварыНаСкладах(ДополнительныеСвойства, Движения, Отказ);
	ЗаказыСервер.ОтразитьТоварыКОтгрузке(ДополнительныеСвойства, Движения, Отказ);
	ЗаказыСервер.ОтразитьЗаказыНаВнутреннееПотребление(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыОрганизаций(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьРезервыТоваровОрганизаций(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыОрганизацийКПередаче(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыКОформлениюОтчетовКомитента(ДополнительныеСвойства, Движения, Отказ);
	ДоходыИРасходыСервер.ОтразитьСебестоимостьТоваров(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьСвободныеОстатки(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьОбеспечениеЗаказов(ДополнительныеСвойства, Движения, Отказ);
	СкладыСервер.ОтразитьДвиженияСерийТоваров(ДополнительныеСвойства, Движения, Отказ);

	// Движения по оборотным регистрам управленческого учета
	УправленческийУчетПроведениеСервер.ОтразитьДвиженияНоменклатураДоходыРасходы(ДополнительныеСвойства, Движения, Отказ);

	//++ НЕ УТ
	РегистрыСведений.ДокументыПоОС.ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ);
	РегистрыСведений.ДокументыПоНМА.ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ);
	//-- НЕ УТ

	РегистрыСведений.РеестрДокументов.ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ);

	//++ ЛокализацияБел
	// УчетБланковСтрогойОтчетности
	РегистрыНакопления.БланкиСтрогойОтчетности.ОтразитьДвижения(ДополнительныеСвойства, Движения, Отказ);
	// Конец УчетБланковСтрогойОтчетности
	//-- ЛокализацияБел
#Вставка
	// ++EE:BAN 13.12.2021 MLVC-27 
	РегистрыСведений.ИспользованныеБСО.ЗаписатьДанныеДокумента(ДополнительныеСвойства, Движения, Отказ);
#КонецВставки

	// 4D:ERP для Беларуси
	// Учет драгоценных материалов
	// {
	УчетДрагоценныхМатериаловРасширенный.ОтразитьДрагоценныеМатериалы(ДополнительныеСвойства, Движения, Отказ);
	// }
	// 4D

	СформироватьСписокРегистровДляКонтроля();

	ВнутреннееПотреблениеТоваровЛокализация.ОбработкаПроведения(ЭтотОбъект, Отказ, РежимПроведения);

	ПроведениеСерверУТ.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	//++ НЕ УТКА
	МеждународныйУчетПроведениеСервер.ЗарегистрироватьКОтражению(ЭтотОбъект, ДополнительныеСвойства, Движения, Отказ);
	//-- НЕ УТКА
	ПроведениеСерверУТ.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	ПроведениеСерверУТ.ЗаписатьПодчиненныеНаборамЗаписейДанные(ЭтотОбъект, Отказ);
	ПроведениеСерверУТ.СформироватьЗаписиРегистровЗаданий(ЭтотОбъект);
	РегистрыСведений.СостоянияЗаказовКлиентов.ОтразитьСостояниеЗаказа(ЭтотОбъект, Отказ);
	ПроведениеСерверУТ.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции

&После("ИнициализироватьДокумент")
Процедура МЛ_ИнициализироватьДокумент(ДанныеЗаполнения)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ЗаказНаПеремещение") 
		Или ТипЗнч(ДанныеЗаполнения) = Тип("Структура")
		И ДанныеЗаполнения.Свойство("ДокументОснование")
		И ТипЗнч(ДанныеЗаполнения.ДокументОснование) = Тип("ДокументСсылка.ЗаказНаПеремещение") Тогда
		
		ВидЦены = Справочники.ВидыЦен.ПустаяСсылка();
	КонецЕсли;

КонецПроцедуры // 4D

// 4D:Милавица, Михаил, 15.02.2019 
// Внутреннее потребление товаров (списание на расходы) на основании заказа на перемещение товаров, №21531
// {
Процедура Чд_ЗаполнитьНаОснованииЗаказаНаПеремещение(ДанныеЗаполнения)
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ЗаказНаПеремещение.Организация,
	|	ЗаказНаПеремещение.СкладОтправитель КАК Склад,
	|	ЗаказНаПеремещение.СкладОтправитель.Подразделение КАК Подразделение,	
	|	НЕ ЗаказНаПеремещение.Проведен КАК ЕстьОшибкиПроведен,
	|	ЗаказНаПеремещение.Ссылка КАК ДокументОснование,
	|	ЗаказНаПеремещение.НаправлениеДеятельности КАК НаправлениеДеятельности
	|ИЗ
	|	Документ.ЗаказНаПеремещение КАК ЗаказНаПеремещение
	|ГДЕ
	|	ЗаказНаПеремещение.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЗаказНаПеремещениеТовары.Номенклатура,
	|	ЗаказНаПеремещениеТовары.Характеристика,
	|	ЗаказНаПеремещениеТовары.Упаковка,
	|	ЗаказНаПеремещениеТовары.КоличествоУпаковок КАК КоличествоУпаковок,
	|	ЗаказНаПеремещениеТовары.Количество КАК Количество,
	|	ЗаказНаПеремещениеТовары.Серия,
	|	СпрНаправленияДеятельности.Назначение КАК Назначение
	|ИЗ
	|	Документ.ЗаказНаПеремещение.Товары КАК ЗаказНаПеремещениеТовары
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.НаправленияДеятельности КАК СпрНаправленияДеятельности
	|		ПО СпрНаправленияДеятельности.Ссылка = ЗаказНаПеремещениеТовары.Ссылка.НаправлениеДеятельности
	|			И СпрНаправленияДеятельности.УчетЗатрат
	|
	|ГДЕ
	|	ЗаказНаПеремещениеТовары.Ссылка = &Ссылка
	|	И ЗаказНаПеремещениеТовары.Номенклатура.ТипНоменклатуры В (ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Товар))
	|");
	
	Запрос.УстановитьПараметр("Ссылка", ДанныеЗаполнения.ДокументОснование);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	ВыборкаРеквизиты = РезультатЗапроса[0].Выбрать();
	Если Не ВыборкаРеквизиты.Следующий() Тогда
		Возврат;
	КонецЕсли;
	
	ОбщегоНазначенияУТ.ПроверитьВозможностьВводаНаОсновании(
	ДанныеЗаполнения.ДокументОснование,,
	ВыборкаРеквизиты.ЕстьОшибкиПроведен);
	
	Если РезультатЗапроса[1].Пустой() Тогда
		ТекстОшибки = НСтр("ru='Поступление не содержит товаров. Ввод на основании невозможен.'");
		ВызватьИсключение ТекстОшибки;
	КонецЕсли;
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, ВыборкаРеквизиты);
	
	ТаблицаТовары = РезультатЗапроса[1].Выгрузить();
	Товары.Загрузить(ТаблицаТовары);
	
	ХозяйственнаяОперация = ДанныеЗаполнения.ХозяйственнаяОперация;
	
	// Заполнение статусов указания серий
	ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ВнутреннееПотреблениеТоваров);
	НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(ЭтотОбъект, ПараметрыУказанияСерий);
	
КонецПроцедуры

#КонецОбласти

#Область Прочее 

&После("СформироватьСписокРегистровДляКонтроля")
Процедура МЛ_СформироватьСписокРегистровДляКонтроля()
	// ++EE:TEN 05.01.2023 MLVC-923
	Если Склад = Справочники.ЭЭ_ПредопределенныеЗначения.СкладУчастокЛЦ.Значение Тогда
		ДополнительныеСвойства.ДляПроведения.РегистрыДляКонтроля.Добавить(Движения.ЭЭ_ОтражениеКачестваЛЦ);
	КонецЕсли;
	// --EE:TEN 05.01.2023 MLVC-923
КонецПроцедуры

#КонецОбласти 

#КонецЕсли
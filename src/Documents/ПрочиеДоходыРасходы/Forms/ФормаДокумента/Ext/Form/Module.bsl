﻿
#Область ОбработчикиСобытийЭлементовФормы
// 4D:Милавица, Антон,  
// <описание>, №24240
// {
&НаКлиенте
&ИзменениеИКонтроль("ПрочиеРасходыСуммаПриИзменении")
Процедура МЛ_ПрочиеРасходыСуммаПриИзменении(Элемент)

	ТекущиеДанные = Элементы.ПрочиеРасходы.ТекущиеДанные;
	Если ЗначениеЗаполнено(ТекущиеДанные.Сумма) Тогда
		ТекущиеДанные.СуммаБезНДС = ТекущиеДанные.Сумма;
#Удаление
		ТекущиеДанные.СуммаРегл = ТекущиеДанные.Сумма * КоэффициентПересчетаВВалютуРегл;
#КонецУдаления
		ПрочиеРасходыСуммаРеглПриИзменении(Элемент);
		РассчитатьИтоговыеПоказатели();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура МЛ_ПрочиеРасходыСуммаРеглПриИзмененииПосле(Элемент)
	
	ТекущиеДанные = Элементы.ПрочиеРасходы.ТекущиеДанные;
	Если ЗначениеЗаполнено(ТекущиеДанные.СуммаРегл) Тогда
		ТекущиеДанные.Сумма = ТекущиеДанные.СуммаРегл / КоэффициентПересчетаВВалютуРегл;
		ТекущиеДанные.СуммаБезНДС = ТекущиеДанные.Сумма;
		ПрочиеРасходыСуммаРеглПриИзменении(Элемент);
		РассчитатьИтоговыеПоказатели();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
&ИзменениеИКонтроль("ПрочиеДоходыСуммаПриИзменении")
Процедура МЛ_ПрочиеДоходыСуммаПриИзменении(Элемент)

	ТекущиеДанные = Элементы.ПрочиеДоходы.ТекущиеДанные;
	Если ЗначениеЗаполнено(ТекущиеДанные.Сумма) Тогда
		ТекущиеДанные.СуммаУпр = ТекущиеДанные.Сумма;
		#Удаление
		ТекущиеДанные.СуммаРегл = ТекущиеДанные.Сумма * КоэффициентПересчетаВВалютуРегл;
		#КонецУдаления
		ПрочиеДоходыСуммаРеглПриИзменении(Элемент);
		РассчитатьИтоговыеПоказатели();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
&ИзменениеИКонтроль("ПрочиеДоходыСуммаРеглПриИзменении")
Процедура МЛ_ПрочиеДоходыСуммаРеглПриИзменении(Элемент)
	СтрокаТаблицы = Элементы.ПрочиеДоходы.ТекущиеДанные;
	#Вставка
	СтрокаТаблицы.Сумма = СтрокаТаблицы.СуммаРегл / КоэффициентПересчетаВВалютуРегл;
	СтрокаТаблицы.СуммаУпр = СтрокаТаблицы.Сумма;
	РассчитатьИтоговыеПоказатели();
	#КонецВставки
	СтрокаТаблицы.СуммаНУ = СтрокаТаблицы.СуммаРегл - СтрокаТаблицы.ПостояннаяРазница - СтрокаТаблицы.ВременнаяРазница;
КонецПроцедуры
 
#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаполнитьРезультатыПромеров(Команда)
	
	Объект.Основание = "Результат от промеров за месяц";
	Объект.Дата = КонецДня(КонецМесяца(Объект.Дата));
	
	ЭтоРасходы = 
		Объект.ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ПрочиеРасходы")
		ИЛИ Объект.ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.СписаниеПрочихРасходов")
		ИЛИ Объект.ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.СторнированиеПрочихРасходов")
		ИЛИ Объект.ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.РеклассификацияРасходов")
		ИЛИ Объект.ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ПередачаПрочихРасходовМеждуФилиалами");
	
	Объект.ПрочиеДоходы.Очистить();
	Объект.ПрочиеРасходы.Очистить();
	
	Если ЭтоРасходы Тогда
		ЗаполнитьРезультатыПромеровРасходыНаСервере();
		Для каждого Строка Из Объект.ПрочиеРасходы Цикл
			СоответствиеИменРеквизитов = Новый Соответствие();
			СоответствиеИменРеквизитов.Вставить("ПредставлениеОтраженияВРеглУчете", "ПредставлениеОтраженияВРеглУчете");
			НастройкаОтраженияВРеглУчете = РеглУчетКлиент.НастройкаОтраженияВРеглУчете(Строка, СоответствиеИменРеквизитов);
			ИмяПредставления = "ПредставлениеОтраженияВРеглУчете"; 
			Строка[ИмяПредставления] = РеглУчетВызовСервера.ПредставлениеОтраженияВРеглУчете(НастройкаОтраженияВРеглУчете, Неопределено);		
		КонецЦикла; 
	Иначе	
		ЗаполнитьРезультатыПромеровДоходыНаСервере();
		Для каждого Строка Из Объект.ПрочиеДоходы Цикл
			СоответствиеИменРеквизитов = Новый Соответствие();
			СоответствиеИменРеквизитов.Вставить("ПредставлениеОтраженияВРеглУчете", "ПредставлениеОтраженияВРеглУчете");
			НастройкаОтраженияВРеглУчете = РеглУчетКлиент.НастройкаОтраженияВРеглУчете(Строка, СоответствиеИменРеквизитов);
			ИмяПредставления = "ПредставлениеОтраженияВРеглУчете"; 
			Строка[ИмяПредставления] = РеглУчетВызовСервера.ПредставлениеОтраженияВРеглУчете(НастройкаОтраженияВРеглУчете, Неопределено);		
		КонецЦикла; 
	КонецЕсли;
	
	ЭтаФорма.ОбновитьОтображениеДанных();
		
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ЗаполнитьРезультатыПромеровРасходыНаСервере()

	Запрос = Новый Запрос();
	Запрос.УстановитьПараметр("Дата", КонецДня(КонецМесяца(Объект.Дата)));
	Запрос.УстановитьПараметр("Счет", ПланыСчетов.Хозрасчетный.Чд_РезультатыОтПромеров);
	Запрос.УстановитьПараметр("Организация", Объект.Организация);
	Запрос.УстановитьПараметр("СтатьяРасходов", ПланыВидовХарактеристик.СтатьиРасходов.Чд_СписаниеНедостач);
	Запрос.УстановитьПараметр("СтатьяАктивовПассивов", ПланыВидовХарактеристик.СтатьиАктивовПассивов.Чд_РезультатОтПромеров);
	Запрос.Текст = "ВЫБРАТЬ
	|	ХозрасчетныйОстатки.Подразделение КАК Подразделение,
	|	ХозрасчетныйОстатки.Счет КАК СчетУчета,
	|	ХозрасчетныйОстатки.Субконто1 КАК Субконто1,
	|	ХозрасчетныйОстатки.Субконто2 КАК Субконто2,
	|	ХозрасчетныйОстатки.Субконто3 КАК Субконто3,
	|	ХозрасчетныйОстатки.СуммаОстатокДт КАК СуммаРегл,
	|	ХозрасчетныйОстатки.СуммаНУОстатокДт КАК СуммаНУ,
	|	ХозрасчетныйОстатки.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|	&Дата КАК ДатаОтражения,
	|	&СтатьяРасходов КАК СтатьяРасходов,
	|	ХозрасчетныйОстатки.Подразделение КАК АналитикаРасходов,
	|	&СтатьяАктивовПассивов КАК СтатьяАктивовПассивов,
	|	ХозрасчетныйОстатки.Подразделение КАК АналитикаАктивовПассивов
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(&Дата, Счет = &Счет, , Организация = &Организация) КАК ХозрасчетныйОстатки";
	
	Рез = Запрос.Выполнить().Выбрать();
	
	Пока Рез.Следующий() Цикл
	
		НоваяСтрока = Объект.ПрочиеРасходы.Добавить();
		СтруктураЗаполненияСтроки = Новый Структура();
		СтруктураЗаполненияСтроки.Вставить("СуммаУпр", Рез.СуммаРегл / КоэффициентПересчетаВВалютуРегл);
		СтруктураЗаполненияСтроки.Вставить("Сумма", Рез.СуммаРегл / КоэффициентПересчетаВВалютуРегл);
		СтруктураЗаполненияСтроки.Вставить("СуммаБезНДС", Рез.СуммаРегл / КоэффициентПересчетаВВалютуРегл);
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Рез);
		ЗаполнитьЗначенияСвойств(НоваяСтрока, СтруктураЗаполненияСтроки);
	
	КонецЦикла; 

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьРезультатыПромеровДоходыНаСервере()

	Запрос = Новый Запрос();
	Запрос.УстановитьПараметр("Дата", КонецДня(КонецМесяца(Объект.Дата)));
	Запрос.УстановитьПараметр("Счет", ПланыСчетов.Хозрасчетный.Чд_РезультатыОтПромеров);
	Запрос.УстановитьПараметр("Организация", Объект.Организация);
	Запрос.УстановитьПараметр("СтатьяДоходов", ПланыВидовХарактеристик.СтатьиДоходов.Чд_ОприходованиеИзлишков);
	Запрос.УстановитьПараметр("СтатьяАктивовПассивов", ПланыВидовХарактеристик.СтатьиАктивовПассивов.Чд_РезультатОтПромеров);
	Запрос.Текст = "ВЫБРАТЬ
	|	ХозрасчетныйОстатки.Подразделение КАК Подразделение,
	|	ХозрасчетныйОстатки.Счет КАК СчетУчета,
	|	ХозрасчетныйОстатки.Субконто1 КАК Субконто1,
	|	ХозрасчетныйОстатки.Субконто2 КАК Субконто2,
	|	ХозрасчетныйОстатки.Субконто3 КАК Субконто3,
	|	ХозрасчетныйОстатки.СуммаОстатокКт КАК СуммаРегл,
	|	ХозрасчетныйОстатки.СуммаНУОстатокКт КАК СуммаНУ,
	|	ХозрасчетныйОстатки.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|	&Дата КАК ДатаОтражения,
	|	&СтатьяДоходов КАК СтатьяДоходов,
	|	ХозрасчетныйОстатки.Подразделение КАК АналитикаДоходов,
	|	&СтатьяАктивовПассивов КАК СтатьяАктивовПассивов,
	|	ХозрасчетныйОстатки.Подразделение КАК АналитикаАктивовПассивов
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(&Дата, Счет = &Счет, , Организация = &Организация) КАК ХозрасчетныйОстатки";
	
	Рез = Запрос.Выполнить().Выбрать();
	
	Пока Рез.Следующий() Цикл
	
		НоваяСтрока = Объект.ПрочиеДоходы.Добавить();
		СтруктураЗаполненияСтроки = Новый Структура();
		СтруктураЗаполненияСтроки.Вставить("СуммаУпр", Рез.СуммаРегл / КоэффициентПересчетаВВалютуРегл);
		СтруктураЗаполненияСтроки.Вставить("Сумма", Рез.СуммаРегл / КоэффициентПересчетаВВалютуРегл);
		СтруктураЗаполненияСтроки.Вставить("СуммаБезНДС", Рез.СуммаРегл / КоэффициентПересчетаВВалютуРегл);
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Рез);
		ЗаполнитьЗначенияСвойств(НоваяСтрока, СтруктураЗаполненияСтроки);
	
	КонецЦикла; 

КонецПроцедуры
// } 4D

#КонецОбласти
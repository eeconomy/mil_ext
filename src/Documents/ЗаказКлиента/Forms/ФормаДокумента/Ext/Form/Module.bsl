﻿
#Область ИзмененияНаФорме

// 4D:Милавица, АнастасияМ, 23.03.2021
// Документ «Реализация товаров и услуг» - настройка формы документа, № 28195
// В рамках задачи на форму добавлено программно:
// 1. Поля ввода "Количество" и "Всего"
// 2. Для элемента ГруппаВсегоСкидка установлена Вертикальная группировка
// 3. Сопоставлен обработчик для формы "ОбработкаВыбора"
// 4. Сопоставлен обработчик ТЧ Товары "ТоварыСерияПриИзменении"

#КонецОбласти 

#Область ОбработчикиСобытий

// 4D:Милавица, АндрейБ, 31.10.2018 
// "Реализация учета материалов в разрезе паспортов кусков в 1С ERP" , № 18881
// 1. Проверка возможности использования серий с признаком "Образец" для данного подразделения
// 2. Программное заполнение поля "КоличествоУпаковок" для номенклатуры по которой ведется учет паспортов
&НаКлиенте
Процедура МЛ_ОбработкаВыбораПосле(ВыбранноеЗначение, ИсточникВыбора)
	
	Если НоменклатураКлиент.ЭтоУказаниеСерий(ИсточникВыбора) Тогда
		ЗаполнитьКоличествоПоПаспорту();
	КонецЕсли;
		
КонецПроцедуры
// }4D

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТовары
// 4D:Милавица, АндрейБ, 31.10.2018 
// "Реализация учета материалов в разрезе паспортов кусков в 1С ERP" , № 18881
// 1. Проверка возможности использования серий с признаком "Образец" для данного подразделения
// 2. Программное заполнение поля "КоличествоУпаковок" для номенклатуры по которой ведется учет паспортов
// Программное заполнение поля "КоличествоУпаковок" для номенклатуры по которой ведется учет паспортов
&НаКлиенте
Процедура МЛ_ТоварыСерияПриИзмененииПосле(Элемент)
	
	ЗаполнитьКоличествоПоПаспорту();
	
КонецПроцедуры
// }4D

// ++EE:TEN 04.05.2022 MLVC-467
&НаКлиенте
Процедура МЛ_ТоварыНоменклатураПриИзмененииПосле(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	ТекущаяСтрока.ЭЭ_ПриоритетПродаж = ЭЭ_ЗаполнитьРейтингПродажТЧ(ТекущаяСтрока.Номенклатура);
	
КонецПроцедуры
// --EE:TEN 04.05.2022 MLVC-467
#КонецОбласти 

#Область ОбработчикиКомандФормы
// ++EE:TEN 04.05.2022 MLVC-467
&НаКлиенте
Процедура ЭЭ_ИзменитьПриоритетПродаж(Команда)
	
	Оповещение = Новый ОписаниеОповещения("ЭЭ_ПослеВводаЗначения", 
      ЭтотОбъект);	
 
    ПоказатьВводЗначения(
        Оповещение,
        , 
        "Введите приоритет продаж",
        "Число"
    );
		
КонецПроцедуры

&НаКлиенте
Процедура ЭЭ_ПослеВводаЗначения(Результат, Параметры) Экспорт	

	Если Не Результат = Неопределено Тогда
		
		Если Результат >= 0 и Результат <= 10 Тогда 	
			ВыделенныеСтроки = Элементы.Товары.ВыделенныеСтроки;
			Для каждого ТекСтрока Из ВыделенныеСтроки Цикл
				СтрокаТовары = Объект.Товары.НайтиПоИдентификатору(ТекСтрока);
				СтрокаТовары.ЭЭ_ПриоритетПродаж = Результат;
			КонецЦикла;
		Иначе
			ТекстСообщения = "Рейтинг продаж может быть только от 0 до 10.";
		    ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения);
		КонецЕсли;
	
	КонецЕсли;
	
КонецПроцедуры
// --EE:TEN 04.05.2022 MLVC-467 

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции

// 4D:Милавица, АндрейБ, 31.10.2018 
// "Реализация учета материалов в разрезе паспортов кусков в 1С ERP" , № 18881
&НаКлиенте
Процедура ЗаполнитьКоличествоПоПаспорту()
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Если ТекущаяСтрока <> Неопределено Тогда
		// 4D:Милавица, Алексей, 17.01.2019 22:11:52 
		// Ошибка обработки количества в строке заказа
		// {
		Если ЗначениеЗаполнено(ТекущаяСтрока.Номенклатура) 
			И НЕ ОбщегоНазначенияУТВызовСервера.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.Номенклатура, "Чд_УчетПаспортов") Тогда
			Возврат;	
		КонецЕсли; 
		// }
		// 4D
		ПрежнееКоличество = ТекущаяСтрока.Количество;
		Если ЗначениеЗаполнено(ТекущаяСтрока.Серия) Тогда
			Если НЕ ПроверитьВозможностьПодбораПаспорта(Объект.Ссылка,ТекущаяСтрока.Серия,ЭтаФорма.УникальныйИдентификатор) Тогда
				ТекущаяСтрока.Серия = ПредопределенноеЗначение("Справочник.СерииНоменклатуры.ПустаяСсылка");
				ТекущаяСтрока.Количество = 0;
			Иначе
				КоличествоПоПаспорту = ПолучитьКоличествоПоПаспорту(ТекущаяСтрока.Серия,ПрежнееКоличество);
				ТекущаяСтрока.Количество = КоличествоПоПаспорту;
			КонецЕсли;
			Если ТекущаяСтрока.Количество <> ПрежнееКоличество Тогда
				Чд_ТоварыКоличествоУпаковокПриИзменении(ТекущаяСтрока);
			КонецЕсли;
		ИначеЕсли ТекущаяСтрока.СтатусУказанияСерий <> 0 Тогда 
			ТекущаяСтрока.Количество = 0;
			Если ТекущаяСтрока.Количество <> ПрежнееКоличество Тогда
				Чд_ТоварыКоличествоУпаковокПриИзменении(ТекущаяСтрока);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Чд_ТоварыКоличествоУпаковокПриИзменении(ТекущаяСтрока)
	
	СтруктураДействий = Новый Структура;
	ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий,Объект);	
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
	РассчитатьИтоговыеПоказателиЗаказа(ЭтаФорма);
	
	СкидкиНаценкиКлиент.СброситьФлагСкидкиРассчитаны(ЭтаФорма);
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьКоличествоПоПаспорту(Серия,ПрежнееКоличество)
	                                     
	КоличествоПоПаспорту = ПрежнееКоличество;
	
	Если ЗначениеЗаполнено(Серия.Чд_ПаспортКуска) Тогда
		КоличествоПоПаспорту = Справочники.Чд_ПаспортКуска.ПолучитьКоличествоОстатокНаСкладе(Серия.Чд_ПаспортКуска);
	КонецЕсли;
				
	Возврат КоличествоПоПаспорту;
	
КонецФункции

&НаСервереБезКонтекста
Функция ПроверитьВозможностьПодбораПаспорта(Ссылка,СерияСсылка,ИдентификаторНазначения)
	
	Результат = Истина;
	Паспорт = СерияСсылка.Чд_ПаспортКуска;
	Если Паспорт.Состояние = Перечисления.Чд_СостоянияПаспортовКуска.Зарезервирован 
		И Паспорт.ОснованиеДляСостояния <> Ссылка Тогда	
		Если ТипЗнч(Паспорт.ОснованиеДляСостояния) = Тип("ДокументСсылка.РасходныйОрдерНаТовары")
			И ЗначениеЗаполнено(Паспорт.ОснованиеДляСостояния) Тогда
			
			РаспоряжениеВРасходномОрдере = Документы.РасходныйОрдерНаТовары.ПолучитьРаспоряжение(Паспорт.ОснованиеДляСостояния, СерияСсылка);
			
			Если РаспоряжениеВРасходномОрдере <> Неопределено И РаспоряжениеВРасходномОрдере <> Ссылка И РаспоряжениеВРасходномОрдере.Проведен
				И ТипЗнч(РаспоряжениеВРасходномОрдере) <> Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
				
				Результат = Ложь;
				ТекстСообщения = НСтр("ru='Паспорт %1 зарезервирован %2. Выбор не доступен.'");
				ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения,Паспорт,Паспорт.ОснованиеДляСостояния);
				Сообщение = Новый СообщениеПользователю;
				Сообщение.ИдентификаторНазначения = ИдентификаторНазначения;
				Сообщение.Текст = ТекстСообщения;
				Сообщение.Сообщить();
			КонецЕсли;
		ИначеЕсли ЗначениеЗаполнено(Паспорт.ОснованиеДляСостояния)
			И (ТипЗнч(Паспорт.ОснованиеДляСостояния) = Тип("ДокументСсылка.ПеремещениеТоваров")
				ИЛИ НЕ Паспорт.ОснованиеДляСостояния.Проведен) Тогда
			
			Результат = Истина;
		Иначе
			Результат = Ложь;
			ТекстСообщения = НСтр("ru='Паспорт %1 зарезервирован %2. Выбор не доступен.'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения,Паспорт,Паспорт.ОснованиеДляСостояния);
			Сообщение = Новый СообщениеПользователю;
			Сообщение.ИдентификаторНазначения = ИдентификаторНазначения;
			Сообщение.Текст = ТекстСообщения;
			Сообщение.Сообщить();
		КонецЕсли;
	КонецЕсли;
	Возврат Результат;
	
КонецФункции
// }4D

#КонецОбласти

#Область Прочее

// 4D:Милавица, АнстасияТ, 11.08.2021  
// ДОКУМЕНТ ПЕРЕМЕЩЕНИЕ - доработка, №29005 
// {
&НаКлиенте
Процедура МЛ_ЗаполнитьПоОтборуПосле(Команда)
Если Не СкладПомещениеЗаполнены() Тогда
		Возврат;
	КонецЕсли;
	
	Если Объект.Товары.Количество() > 0 Тогда
		Ответ = Неопределено;

		ПоказатьВопрос(Новый ОписаниеОповещения("ЗаполнитьЗавершение", ЭтотОбъект),
			НСтр("ru = 'Перед заполнением список товаров будет очищен. Продолжить?'"), РежимДиалогаВопрос.ДаНет);
        Возврат;
	КонецЕсли;
	
	ЗаполнитьФрагмент();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
    
    Ответ = РезультатВопроса;
    Если Ответ = КодВозвратаДиалога.Да Тогда
        Объект.Товары.Очистить();
    Иначе
        Возврат;
    КонецЕсли;
    
    ЗаполнитьФрагмент();

КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьФрагмент()
    
    ЗаполнитьСервер();
    
КонецПроцедуры

&НаКлиенте
Функция СкладПомещениеЗаполнены()

	Результат = Истина;
	
	Если НЕ ЗначениеЗаполнено(Объект.Склад) Тогда
				
		Результат = Ложь;
		ТекстСообщения = НСтр("ru = 'Не заполнено поле ""Склад"".
			|Для выполнения действия необходимо указать склад, на котором проводится пересчет'");
		ОчиститьСообщения();
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Склад", "Объект");
		
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

&НаСервере
Процедура ЗаполнитьСервер()
	
	УстановитьЗначениеПараметраНастроек(ОтборПересчета.Настройки, "ДатаОстатков", ?(ЗначениеЗаполнено(Объект.Дата), Объект.Дата, ТекущаяДатаСеанса()));
	
	Если ИспользоватьАдресноеХранение Тогда
		Если ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.Склад, "Наименование") = "Склад основных материалов" Тогда
			СхемаКомпоновкиДанных = Документы.ПересчетТоваров.ПолучитьМакет("Чд_ОтборПересчетаПоЯчейкам");
		Иначе
			СхемаКомпоновкиДанных = Документы.ПересчетТоваров.ПолучитьМакет("ОтборПересчетаПоЯчейкам");
		КонецЕсли; 
	Иначе	
		СхемаКомпоновкиДанных = Документы.ПересчетТоваров.ПолучитьМакет("ОтборПересчетаПоСкладу");
	КонецЕсли;
	
	ТекстЗапроса = СхемаКомпоновкиДанных.НаборыДанных.ЗаполнениеПоОтборам.Запрос;
	
	Если ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.Склад, "Наименование") = "Склад основных материалов" Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса,
		"&ТекстЗапросаКоэффициентУпаковки",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки(
		"ТоварыВЯчейках.Номенклатура.ДлинаЕдиницаИзмерения", "ТоварыВЯчейках.Номенклатура", "ТоварыВЯчейках.Серия"));
	Иначе
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса,
		"&ТекстЗапросаКоэффициентУпаковки",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки(
		"ТоварыВЯчейках.Упаковка", Неопределено));
	КонецЕсли; 
	
	СхемаКомпоновкиДанных.НаборыДанных.ЗаполнениеПоОтборам.Запрос = ТекстЗапроса;
	
	СегментыСервер.ВключитьОтборПоСегментуНоменклатурыВСКД(ОтборПересчета);
	
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновки   = КомпоновщикМакета.Выполнить(СхемаКомпоновкиДанных, ОтборПересчета.ПолучитьНастройки(),,,
				Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
	
	ПроцессорКомпоновкиДанных = Новый ПроцессорКомпоновкиДанных;
	ПроцессорКомпоновкиДанных.Инициализировать(МакетКомпоновки);
	
	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
	Объект.Товары.Загрузить(ПроцессорВывода.Вывести(ПроцессорКомпоновкиДанных));
	
	ЗаполнитьСлужебныеРеквизитыПоНоменклатуре();
	
	НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(Объект, ПараметрыУказанияСерий);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьЗначениеПараметраНастроек(Настройки, ИмяПараметра, Значение)
	
	Параметр = Настройки.ПараметрыДанных.Элементы.Найти(ИмяПараметра);
	
	Если Параметр <> Неопределено Тогда
		Параметр.Значение = Значение;
		Параметр.Использование = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
&ИзменениеИКонтроль("ПриЧтенииСозданииНаСервере")
Процедура МЛ_ПриЧтенииСозданииНаСервере()
#Вставка
	// 4D:Милавица, АнстасияТ, 11.08.2021  
	// ДОКУМЕНТ ПЕРЕМЕЩЕНИЕ - доработка, №29005 
	//
	ИнициализироватьКомпоновкуДанных(Ложь);
#КонецВставки
	ИспользоватьСтатусы = ПолучитьФункциональнуюОпцию("ИспользоватьРасширенныеВозможностиЗаказаКлиента");
	Элементы.Статус.Видимость = ИспользоватьСтатусы;
	Элементы.ЗакрытьЗаказ.Видимость = ИспользоватьСтатусы;

	ВалютаДокумента = Объект.Валюта;
	СкладГруппа = Справочники.Склады.ЭтоГруппаИСкладыИспользуютсяВТЧДокументовПродажи(Объект.Склад);
	Элементы.ТоварыЗаполнитьСкладВВыделенныхСтроках.Доступность = СкладГруппа;

	ВзаиморасчетыСервер.ЗаполнитьПорядокРасчетовВФорме(ЭтаФорма, Ложь , Ложь);

	УстановитьПараметрыВыбораТоварыСклад();

	ИспользоватьСоглашенияСКлиентами                  = ПолучитьФункциональнуюОпцию("ИспользоватьСоглашенияСКлиентами");
	ИспользоватьОтгрузкуБезПереходаПраваСобственности = ПолучитьФункциональнуюОпцию("ИспользоватьОтгрузкуБезПереходаПраваСобственности");
	ИспользоватьУпрощеннуюСхемуОплаты                 = ПолучитьФункциональнуюОпцию("ИспользоватьУпрощеннуюСхемуОплатыВПродажах");
	ИспользоватьГрафикиОплаты                         = ПолучитьФункциональнуюОпцию("ИспользоватьГрафикиОплаты");
	ИспользоватьНесколькоОрганизаций                  = ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоОрганизаций");
	ИспользоватьНаправленияДеятельности               = ПолучитьФункциональнуюОпцию("ИспользоватьУчетДоходовПоНаправлениямДеятельности");
	ИспользоватьПередачуТоваровНаХранение             = Ложь;
	//++ НЕ УТ
	ИспользоватьПередачуТоваровНаХранение             = ПолучитьФункциональнуюОпцию("ИспользоватьПередачуНаОтветственноеХранениеСПравомПродажи");
	//-- НЕ УТ

	ИспользоватьРасширеннуюФормуПодбораКоличестваИВариантовОбеспечения	= ПолучитьФункциональнуюОпцию("ИспользоватьРасширеннуюФормуПодбораКоличестваИВариантовОбеспечения");
	ИспользоватьПострочнуюОтгрузкуВЗаказеКлиента						= ПолучитьФункциональнуюОпцию("ИспользоватьПострочнуюОтгрузкуВЗаказеКлиента");
	НоваяАрхитектураВзаиморасчетов                                      = ПолучитьФункциональнуюОпцию("НоваяАрхитектураВзаиморасчетов");

	Если НЕ ИспользоватьСоглашенияСКлиентами Тогда
		ПараметрыВыбораВидаЦены = ЗначениеНастроекПовтИсп.ПараметрыВыбораВидаЦеныПоУмолчанию();

		ПараметрыВыбораВидаЦены.ЦенаВключаетНДС        = Объект.ЦенаВключаетНДС;
		ПараметрыВыбораВидаЦены.ИспользоватьПриПродаже = Истина;
		ПараметрыВыбораВидаЦены.Статус                 = Перечисления.СтатусыДействияВидовЦен.Действует;

		ВидЦеныПоУмолчанию = ЗначениеНастроекПовтИсп.ВидЦеныПоУмолчанию(ПараметрыВыбораВидаЦены);
	КонецЕсли;

	УстановитьВидимостьОпераций();
	УстановитьДоступностьЭлементовПоСтатусуСервер();

	ЗаполнитьСлужебныеРеквизитыПоНоменклатуре();

	ПараметрыЗаполнения = Новый Структура("ЕстьРаботы, ЕстьОтменено", Истина, Истина);
	ОбеспечениеСервер.ЗаполнитьСлужебныеРеквизиты(Объект.Товары, ПараметрыЗаполнения, ДатаОтгрузкиОбязательна, СкладОбязателен);

	ОбновитьДубликатыЗависимыхРеквизитов();
	ОбновитьСостояниеСервер();

	УстановитьСвойстваЭлементовПоПорядкуРасчетов();
	УстановитьВидимостьЭлементовПоОперацииСервер();

	РассчитатьИтоговыеПоказателиЗаказа(ЭтаФорма);
	ПараметрыУказанияСерий = Новый ФиксированнаяСтруктура(НоменклатураСервер.ПараметрыУказанияСерий(Объект, Документы.ЗаказКлиента));
	Элементы.ТоварыСерия.Видимость = ПараметрыУказанияСерий.ИспользоватьСерииНоменклатуры;
	ЗаполнитьСписокВыбораЖелаемаяДатаОтгрузки();

	Элементы.Организация.Видимость = ИспользоватьНесколькоОрганизаций;

	УстановитьВидимостьЭлементовФормыДатОтгрузки();
	ОбновитьДоступностьЭлементовВозвратнойТары(ЭтаФорма);

	ДоставкаТоваров.ПриЧтенииСозданииРаспоряженийНаСервере(Элементы, Объект);

	УстановитьВидимостьЗапретаОтгрузкиПартнеру();
	УстановитьВидимостьОбеспечения();
	Элементы.ТоварыЗаполнитьОбеспечение.Доступность = Не ТолькоПросмотр;

	РассылкиИОповещенияКлиентам.УстановитьВидимостьПодпискиНаОповещенияВОбъекте(
	Элементы.ГруппаПодпискаНаОповещения,
	Объект.Партнер,
	ПредопределенноеЗначение("Перечисление.ТипыСобытийОповещений.ИзменениеСостоянияЗаказа"));

	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГруппаСостояниеЭДО", "Видимость", ИспользоватьСтатусы, Истина);

	УстановитьПривилегированныйРежим(Истина);
	ВариантыОбеспечения = ПродажиСервер.ВариантыОбеспеченияПоУмолчанию(Объект.Соглашение, Объект.Статус);

	СкладыСервер.ПриИзмененииСкладаВТабличнойЧасти(Объект.Товары, ТаблицаСкладов, СкладГруппа);
	ВсегоСкладов = ТаблицаСкладов.Количество();
	СкладыКлиентСервер.ОбновитьКартинкуГруппыСкладов(НадписьНесколькоСкладов, Элементы.КартинкаНесколькоСкладов, ВсегоСкладов);

	ОбщегоНазначенияУТ.ИнициализироватьКешТекущейСтроки(ЭтотОбъект, "Товары");

	МетаданныеФормы = ОбщегоНазначения.МенеджерОбъектаПоСсылке(Объект.Ссылка).ОписаниеФормыДокументаДляЗаполненияРеквизитовСвязанныхСНаправлениемДеятельности();
	НаправленияДеятельностиСервер.ПриЧтенииСозданииНаСервере(ЭтаФорма);

	ПараметрыЗаполнения = Документы.ЗаказКлиента.ПараметрыЗаполненияНалогообложенияНДСПродажи(Объект);
	УчетНДСУП.ЗаполнитьСписокВыбораНалогообложенияНДСПродажи(Элементы.НалогообложениеНДС, Объект.НалогообложениеНДС, ПараметрыЗаполнения, УчетНДСКэшированныеЗначенияПараметров);

КонецПроцедуры

&НаСервере
Процедура ИнициализироватьКомпоновкуДанных(ВсегдаИспользоватьНастройкиПоУмолчанию)
	
	Если ИспользоватьАдресноеХранение Тогда
		СхемаКомпоновкиДанных = Документы.ПересчетТоваров.ПолучитьМакет("ОтборПересчетаПоЯчейкам");
	Иначе
		СхемаКомпоновкиДанных = Документы.ПересчетТоваров.ПолучитьМакет("ОтборПересчетаПоСкладу");
	КонецЕсли;
	
	URLСхемы = ПоместитьВоВременноеХранилище(СхемаКомпоновкиДанных, Новый УникальныйИдентификатор());
	
	ИсточникНастроек = Новый ИсточникДоступныхНастроекКомпоновкиДанных(URLСхемы);
	
	ОтборПересчета.Инициализировать(ИсточникНастроек);
	
	ТекОбъект = РеквизитФормыВЗначение("Объект");
	ТекНастройки = ТекОбъект.Чд_ОтборПересчета.Получить();
	Если ТекНастройки = Неопределено
		Или ВсегдаИспользоватьНастройкиПоУмолчанию Тогда
		ОтборПересчета.ЗагрузитьНастройки(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
	Иначе
		ОтборПересчета.ЗагрузитьНастройки(ТекНастройки);
	КонецЕсли;
	
	ОтборПересчета.Восстановить(СпособВосстановленияНастроекКомпоновкиДанных.ПроверятьДоступность);
	
	УстановитьЗначениеПараметраНастроек(ОтборПересчета.Настройки, "Склад" , Объект.Склад);
	УстановитьЗначениеПараметраНастроек(ОтборПересчета.Настройки, "Помещение", Справочники.СкладскиеПомещения.ПустаяСсылка());
	
КонецПроцедуры

&НаСервере
&После("СкладПриИзмененииСервер")
Процедура МЛ_СкладПриИзмененииСервер()
	ИнициализироватьКомпоновкуДанных(Истина);
КонецПроцедуры

// }
// 4D

#КонецОбласти

#Область ПодборыИОбработкаПроверкиКоличества
&НаСервере
&ИзменениеИКонтроль("ОбработкаВыбораПодборНаСервере")
Процедура МЛ_ОбработкаВыбораПодборНаСервере(ВыбранноеЗначение)

	ТаблицаТоваров = ПолучитьИзВременногоХранилища(ВыбранноеЗначение.АдресТоваровВХранилище);
	КэшированныеЗначения = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруКэшируемыеЗначения();
	#Вставка
	// ++EE:TEN 04.05.2022 MLVC-467
	РейтингПродажТЧКлиент = ЭЭ_ЗаполнитьРейтингПродажТЧКлиент();
	#КонецВставки

	Для каждого СтрокаТовара Из ТаблицаТоваров Цикл

		ТекущаяСтрока = Объект.Товары.Добавить();
		ЗаполнитьЗначенияСвойств(ТекущаяСтрока, СтрокаТовара, "НоменклатураНабора, ХарактеристикаНабора, Номенклатура, Характеристика, Упаковка, Склад, ДатаОтгрузки, ВидЦены, Цена, КоличествоУпаковок, СрокПоставки, ПроцентРучнойСкидки, ВариантОбеспечения, Серия");

		ТекущаяСтрока.ИндексНабора = ?(ЗначениеЗаполнено(ТекущаяСтрока.НоменклатураНабора), 1, 0);

		СтруктураДействий = Новый Структура;
		СтруктураДействий.Вставить("ЗаполнитьПризнакТипНоменклатуры", Новый Структура("Номенклатура", "ТипНоменклатуры"));
		СтруктураДействий.Вставить("ЗаполнитьПризнакАртикул", Новый Структура("Номенклатура", "Артикул"));
		СтруктураДействий.Вставить("ЗаполнитьСтавкуНДС", Новый Структура("НалогообложениеНДС, Дата, ПоДатеОтгрузки", Объект.НалогообложениеНДС, Объект.Дата, Истина));
		СтруктураДействий.Вставить("ЗаполнитьСтавкуНДСВозвратнойТары", Объект.ВернутьМногооборотнуюТару);
		СтруктураДействий.Вставить("ЗаполнитьПризнакБезВозвратнойТары", Объект.ВернутьМногооборотнуюТару);
		СтруктураДействий.Вставить("ЗаполнитьПризнакОтмененоБезВозвратнойТары", Объект.ВернутьМногооборотнуюТару);
		СтруктураДействий.Вставить("ЗаполнитьСодержание", ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияСодержанияУслугиВСтрокеТЧ(Объект, Ложь));
		СтруктураДействий.Вставить("ПоместитьОбработанныеСтрокиВКэшированныеЗначения");
		Если Не ЗначениеЗаполнено(СтрокаТовара.ВариантОбеспечения) Тогда

			ПараметрыДействия = ОбеспечениеКлиентСервер.ПараметрыДействияПроверитьЗаполнитьОбеспечение(ВариантыОбеспечения);
			СтруктураДействий.Вставить("ПроверитьЗаполнитьОбеспечениеВДокументеПродажи", ПараметрыДействия);

			Если ИспользоватьПострочнуюОтгрузкуВЗаказеКлиента Тогда

				СтруктураДействий.Вставить("ЗаполнитьВариантОбеспеченияПоДатеОтгрузки",
				ОбеспечениеКлиентСервер.СтруктураЗаполненияВариантаОбеспечения(Объект, СтрокаТовара.ДатаОтгрузки));

			КонецЕсли;

		КонецЕсли;

		СтруктураДействий.Вставить("ПриИзмененииТипаНоменклатурыИлиВариантаОбеспечения",
		Новый Структура("ЕстьРаботы, ЕстьОтменено", Истина, Истина));

		ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий,Объект);

		// {{ Локализация_УправлениеТорговлейДляБеларуси
		Если СтрокаТовара.ПроцентРучнойСкидки <> 0 Тогда
			СтруктураПересчетаСуммы = ОбработкаТабличнойЧастиКлиентСервер.ПараметрыПересчетаСуммыНДСВСтрокеТЧ(Объект);
			СтруктураДействий.Вставить("ПересчитатьСуммуРучнойСкидки");
			СтруктураДействий.Вставить("ПересчитатьСумму");
			СтруктураДействий.Вставить("ПересчитатьСуммуСУчетомРучнойСкидки", Новый Структура("Очищать", Ложь));
			СтруктураДействий.Вставить("ПересчитатьСуммуСУчетомАвтоматическойСкидки", Новый Структура("Очищать", Ложь));
			СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
			СтруктураДействий.Вставить("ПересчитатьСуммуСНДС", СтруктураПересчетаСуммы);
			СтруктураДействий.Вставить("ЗаполнитьДубликатыЗависимыхРеквизитов", ЗависимыеРеквизиты());
			ТекущаяСтрока.Скидка = 0;	
		КонецЕсли; 
		// Локализация_УправлениеТорговлейДляБеларуси }}

		ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);

		#Вставка
		// ++EE:TEN 04.05.2022 MLVC-467		
		// проверка наличия Партнер в регистре сведений ЭЭ_ПриоритетПродаж
		Если РейтингПродажТЧКлиент = 0 Тогда
			ТекущаяСтрока.ЭЭ_ПриоритетПродаж = 0;
		Иначе
			ТекущаяСтрока.ЭЭ_ПриоритетПродаж = ЭЭ_ЗаполнитьРейтингПродажТЧ(ТекущаяСтрока.Номенклатура);
		КонецЕсли;
		#КонецВставки
	КонецЦикла;
	ОбеспечениеСервер.РассчитатьДатуОтгрузкиВСтрокахТабличнойЧасти(КэшированныеЗначения.ОбработанныеСтроки, Объект, Объект.Товары);

	ЗаполнитьСлужебныеРеквизитыПоНоменклатуре();
	НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(Объект, ПараметрыУказанияСерий);

	ПриИзмененииДатыОтгрузкиВТабЧасти();
	СкладыСервер.ПриИзмененииСкладаВТабличнойЧасти(Объект.Товары, ТаблицаСкладов, СкладГруппа);
	ВсегоСкладов = ТаблицаСкладов.Количество();
	СкладыКлиентСервер.ОбновитьКартинкуГруппыСкладов(НадписьНесколькоСкладов, Элементы.КартинкаНесколькоСкладов, ВсегоСкладов);

	РассчитатьИтоговыеПоказателиЗаказа(ЭтаФорма);
	МногооборотнаяТараСервер.ОбновитьСостояниеЗаполненияМногооборотнойТары(Объект.СостояниеЗаполненияМногооборотнойТары);

КонецПроцедуры

// ++EE:TEN 04.05.2022 MLVC-467
&НаСервере
Функция ЭЭ_ЗаполнитьРейтингПродажТЧ(Номенклатура)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	ЭЭ_ПриоритетПродажСрезПоследних.Клиент КАК Клиент,
		|	ЭЭ_ПриоритетПродажСрезПоследних.Коллекция КАК Коллекция,
		|	ЭЭ_ПриоритетПродажСрезПоследних.Приоритет КАК Приоритет
		|ИЗ
		|	РегистрСведений.ЭЭ_ПриоритетПродаж.СрезПоследних(
		|			&Дата,
		|			Клиент = &Клиент
		|				И Коллекция = &Коллекция) КАК ЭЭ_ПриоритетПродажСрезПоследних
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ ПЕРВЫЕ 1
		|	ЭЭ_ПриоритетПродажСрезПоследних.Клиент КАК Клиент,
		|	ЭЭ_ПриоритетПродажСрезПоследних.Коллекция КАК Коллекция,
		|	ЭЭ_ПриоритетПродажСрезПоследних.Приоритет КАК Приоритет
		|ИЗ
		|	РегистрСведений.ЭЭ_ПриоритетПродаж.СрезПоследних(&Дата, Клиент = &Клиент) КАК ЭЭ_ПриоритетПродажСрезПоследних";
	
	Запрос.УстановитьПараметр("Клиент", Объект.Партнер);
	Запрос.УстановитьПараметр("Коллекция", Номенклатура.ЭЭ_КоллекцияМилавицы);
	Запрос.УстановитьПараметр("Дата", Объект.Дата);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	ВыборкаКоллекция = РезультатЗапроса[0].Выбрать();
	
	Если ВыборкаКоллекция.Следующий() Тогда
		Возврат ВыборкаКоллекция.Приоритет;
	КонецЕсли;
	
	ВыборкаКлиент = РезультатЗапроса[1].Выбрать();
	
	Если ВыборкаКлиент.Следующий() Тогда	
		Возврат ВыборкаКлиент.Приоритет;		
	КонецЕсли;

	Возврат 0;
	
КонецФункции

&НаСервере
Функция ЭЭ_ЗаполнитьРейтингПродажТЧКлиент()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	ЭЭ_ПриоритетПродажСрезПоследних.Приоритет КАК Приоритет,
		|	ЭЭ_ПриоритетПродажСрезПоследних.Клиент КАК Клиент
		|ИЗ
		|	РегистрСведений.ЭЭ_ПриоритетПродаж.СрезПоследних(
		|			&Дата,
		|			Клиент = &Клиент
		|				И Коллекция = ЗНАЧЕНИЕ(Справочник.КоллекцииНоменклатуры.ПустаяСсылка)) КАК ЭЭ_ПриоритетПродажСрезПоследних";
	
	Запрос.УстановитьПараметр("Клиент", Объект.Партнер);
	Запрос.УстановитьПараметр("Дата", Объект.Дата);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаКлиент = РезультатЗапроса.Выбрать();
	
	Если ВыборкаКлиент.Следующий() Тогда	
		Возврат 1;	// в регистре сведений ЭЭ_ПриоритетПродаж имеется Партнер	
	КонецЕсли;
	
	// в регистре сведений ЭЭ_ПриоритетПродаж отсутствует Партнер
	// рейтинг продаж по умолчанию 0
	Возврат 0;     
	
КонецФункции

// ++EE:TEN 04.05.2022 MLVC-467

#КонецОбласти
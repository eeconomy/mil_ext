﻿
#Область ОбработчикиСобытий

&ИзменениеИКонтроль("ПередЗаписью")
Процедура МЛ_ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)

	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;

	Если ПолучитьФункциональнуюОпцию("ИспользоватьШтатноеРасписание") Тогда
		ДолжностьПозиции = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ДолжностьПоШтатномуРасписанию, "Должность");
		Если Должность <> ДолжностьПозиции Тогда
			Должность = ДолжностьПозиции;
		КонецЕсли;
	Иначе
		Если ЗначениеЗаполнено(ДолжностьПоШтатномуРасписанию) Тогда
			ДолжностьПоШтатномуРасписанию = Справочники.ШтатноеРасписание.ПустаяСсылка();
		КонецЕсли;
	КонецЕсли;

	Если ЕжегодныеОтпуска.Количество() = 0 Тогда
#Удаление
		ОстаткиОтпусков.ЗаполнитьЕжегоднымиОтпускамиСотрудника(ЭтотОбъект, "ДатаПриема", Ложь);
		ОстаткиОтпусков.ПрименитьНастройкиИспользованияСеверногоОтпуска(
		ЕжегодныеОтпуска, Организация, ДатаПриема, Подразделение, Территория);
		ОстаткиОтпусков.ПрименитьНастройкиИспользованияОтпускаЗаВредность(
		ЕжегодныеОтпуска, ?(ЗначениеЗаполнено(ДолжностьПоШтатномуРасписанию), ДолжностьПоШтатномуРасписанию, Должность));
#КонецУдаления
#Вставка
// ++EE:BAN 12.03.2022 Ошибка в типовой
		ОстаткиОтпусков_Локализация.ЗаполнитьЕжегоднымиОтпускамиСотрудника(ЭтотОбъект, "ДатаПриема", Ложь);
		ОстаткиОтпусков_Локализация.ПрименитьНастройкиИспользованияСеверногоОтпуска(
			ЕжегодныеОтпуска, Организация, ДатаПриема, Подразделение, Территория);
		ОстаткиОтпусков_Локализация.ПрименитьНастройкиИспользованияОтпускаЗаВредность(
			ЕжегодныеОтпуска, ?(ЗначениеЗаполнено(ДолжностьПоШтатномуРасписанию), ДолжностьПоШтатномуРасписанию, Должность));
#КонецВставки
	КонецЕсли;

	ЗарплатаКадрыРасширенный.ПередЗаписьюМногофункциональногоДокумента(ЭтотОбъект, Отказ, РежимЗаписи, РежимПроведения);

КонецПроцедуры

#КонецОбласти 

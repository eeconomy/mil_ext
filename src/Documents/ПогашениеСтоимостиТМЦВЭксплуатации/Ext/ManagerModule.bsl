﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

&ИзменениеИКонтроль("ТребуетсяПогашениеСтоимостиТМЦВЭксплуатации")
Функция МЛ_ТребуетсяПогашениеСтоимостиТМЦВЭксплуатации(Организация, Период)

	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	ПартииТМЦ.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ ПартииТМЦ
	|ИЗ
	|	Справочник.ПартииТМЦВЭксплуатации КАК ПартииТМЦ
	|ГДЕ
	|	ПартииТМЦ.СпособПогашенияСтоимостиБУ В (
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.ПоСроку), 
#Вставка
	// 4D:Милавица, Алексей, 19.03.2019 9:23:55
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.Половина), 
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.Ноль),
#КонецВставки
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.ПоНаработке))
	|	И (ПартииТМЦ.ДатаЗавершенияЭксплуатации >= &НачалоМесяца
	|		ИЛИ ПартииТМЦ.ДатаЗавершенияЭксплуатации = ДАТАВРЕМЯ(1, 1, 1))
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ТМЦВЭксплуатации.КоличествоОборот
	|ИЗ
	|	РегистрНакопления.ТМЦВЭксплуатации.Обороты(
	|			,
	|			&НачалоМесяца,
	|			,
	|			(Организация В (&Организация)
	|				ИЛИ &ПоВсемОрганизациям)
	|			И Партия В (ВЫБРАТЬ ПартииТМЦ.Ссылка ИЗ ПартииТМЦ КАК ПартииТМЦ)
	|	) КАК ТМЦВЭксплуатации
	|ГДЕ
	|	ТМЦВЭксплуатации.КоличествоОборот > 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ВозвратИзЭксплуатации.Ссылка
	|ИЗ
	|	Документ.ПрочееОприходованиеТоваров КАК ВозвратИзЭксплуатации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПрочееОприходованиеТоваров.Товары КАК ВозвратИзЭксплуатацииТовары
	|		ПО ВозвратИзЭксплуатации.Ссылка = ВозвратИзЭксплуатацииТовары.Ссылка
	|ГДЕ
	|	(ВозвратИзЭксплуатации.Организация В(&Организация)
	|		ИЛИ &ПоВсемОрганизациям)
	|	И ВозвратИзЭксплуатации.ХозяйственнаяОперация В (ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратИзЭксплуатации))
	|	И ВозвратИзЭксплуатации.Проведен
	|	И НЕ ВозвратИзЭксплуатации.ПометкаУдаления
	|	И ВозвратИзЭксплуатации.Дата МЕЖДУ &НачалоМесяца И &КонецМесяца 
#Вставка
	// 4D:Милавица, Алексей, 19.03.2019 12:05:33 
	// {
#КонецВставки
#Удаление
	|	И НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатации.Дата, МЕСЯЦ) <> НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ВозвратИзЭксплуатации.Ссылка
	|ИЗ
	|	Документ.ПрочееОприходованиеТоваров КАК ВозвратИзЭксплуатации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПрочееОприходованиеТоваров.Товары КАК ВозвратИзЭксплуатацииТовары
	|		ПО ВозвратИзЭксплуатации.Ссылка = ВозвратИзЭксплуатацииТовары.Ссылка
	|ГДЕ
	|	(ВозвратИзЭксплуатации.Организация В(&Организация)
	|		ИЛИ &ПоВсемОрганизациям)
	|	И ВозвратИзЭксплуатации.ХозяйственнаяОперация В (ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратИзЭксплуатации))
	|	И ВозвратИзЭксплуатации.Проведен
	|	И НЕ ВозвратИзЭксплуатации.ПометкаУдаления
	|	И ВозвратИзЭксплуатации.Дата МЕЖДУ &НачалоПрошлогоМесяца И &НачалоМесяца
	|	И НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатации.Дата, МЕСЯЦ) = НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
#КонецУдаления
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПеремещениеВЭксплуатации.Ссылка
	|ИЗ
	|	Документ.ПеремещениеВЭксплуатации КАК ПеремещениеВЭксплуатации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПеремещениеВЭксплуатации.Товары КАК ПеремещениеВЭксплуатацииТовары
	|		ПО ПеремещениеВЭксплуатации.Ссылка = ПеремещениеВЭксплуатацииТовары.Ссылка
	|ГДЕ
	|	(ПеремещениеВЭксплуатации.Организация В(&Организация)
	|		ИЛИ &ПоВсемОрганизациям)
	|	И ПеремещениеВЭксплуатации.Проведен
	|	И НЕ ПеремещениеВЭксплуатации.ПометкаУдаления
	|	И ПеремещениеВЭксплуатации.Дата МЕЖДУ &НачалоМесяца И &КонецМесяца 
#Вставка
	// 4D:Милавица, Алексей, 19.03.2019 12:05:52 
	// {
#КонецВставки
#Удаление
	|	И НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатации.Дата, МЕСЯЦ) <> НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПеремещениеВЭксплуатации.Ссылка
	|ИЗ
	|	Документ.ПеремещениеВЭксплуатации КАК ПеремещениеВЭксплуатации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПеремещениеВЭксплуатации.Товары КАК ПеремещениеВЭксплуатацииТовары
	|		ПО ПеремещениеВЭксплуатации.Ссылка = ПеремещениеВЭксплуатацииТовары.Ссылка
	|ГДЕ
	|	(ПеремещениеВЭксплуатации.Организация В(&Организация)
	|		ИЛИ &ПоВсемОрганизациям)
	|	И ПеремещениеВЭксплуатации.Проведен
	|	И НЕ ПеремещениеВЭксплуатации.ПометкаУдаления
	|	И ПеремещениеВЭксплуатации.Дата МЕЖДУ &НачалоПрошлогоМесяца И &НачалоМесяца
	|	И НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатации.Дата, МЕСЯЦ) = НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
#КонецУдаления
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	СписаниеИзЭксплуатации.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.СписаниеИзЭксплуатации КАК СписаниеИзЭксплуатации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.СписаниеИзЭксплуатации.Товары КАК СписаниеИзЭксплуатацииТовары
	|		ПО СписаниеИзЭксплуатации.Ссылка = СписаниеИзЭксплуатацииТовары.Ссылка
	|ГДЕ
	|	(СписаниеИзЭксплуатации.Организация В(&Организация)
	|		ИЛИ &ПоВсемОрганизациям)
	|	И СписаниеИзЭксплуатации.Проведен
	|	И НЕ СписаниеИзЭксплуатации.ПометкаУдаления
	|	И СписаниеИзЭксплуатации.Дата МЕЖДУ &НачалоМесяца И &КонецМесяца 
#Вставка
	// 4D:Милавица, Алексей, 19.03.2019 12:06:18 
	// {
#КонецВставки
#Удаление
	|	И НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатации.Дата, МЕСЯЦ) <> НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	СписаниеИзЭксплуатации.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.СписаниеИзЭксплуатации КАК СписаниеИзЭксплуатации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.СписаниеИзЭксплуатации.Товары КАК СписаниеИзЭксплуатацииТовары
	|		ПО СписаниеИзЭксплуатации.Ссылка = СписаниеИзЭксплуатацииТовары.Ссылка
	|ГДЕ
	|	(СписаниеИзЭксплуатации.Организация В(&Организация)
	|		ИЛИ &ПоВсемОрганизациям)
	|	И СписаниеИзЭксплуатации.Проведен
	|	И НЕ СписаниеИзЭксплуатации.ПометкаУдаления
	|	И СписаниеИзЭксплуатации.Дата МЕЖДУ &НачалоПрошлогоМесяца И &НачалоМесяца
	|	И НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатации.Дата, МЕСЯЦ) = НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
#КонецУдаления
	|";

	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("НачалоПрошлогоМесяца", НачалоМесяца(НачалоМесяца(Период)-1));
	Запрос.УстановитьПараметр("НачалоМесяца", НачалоМесяца(Период));
	Запрос.УстановитьПараметр("КонецМесяца", КонецМесяца(Период));
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("ПоВсемОрганизациям", НЕ ЗначениеЗаполнено(Организация));

	УстановитьПривилегированныйРежим(Истина);
	Результат = Запрос.Выполнить();
	УстановитьПривилегированныйРежим(Ложь);

	Возврат НЕ Результат.Пустой();

КонецФункции

&ИзменениеИКонтроль("РассчитатьНомераПакетовДляПартийТМЦ")
Процедура МЛ_РассчитатьНомераПакетовДляПартийТМЦ(Месяц, Организация)

	МаксимальныйОбъемПакета = 20000;
	ТекущиеНомераПакетов = Новый Соответствие;

	ИзмененныеПакеты = Новый ТаблицаЗначений;
	ИзмененныеПакеты.Колонки.Добавить("Организация");
	ИзмененныеПакеты.Колонки.Добавить("НомерПакета");

	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ПартииТМЦ.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ ПартииТМЦ
	|ИЗ
	|	Справочник.ПартииТМЦВЭксплуатации КАК ПартииТМЦ
	|ГДЕ
	|	ПартииТМЦ.СпособПогашенияСтоимостиБУ В (
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.ПоСроку), 
#Вставка
	// 4D:Милавица, Алексей, 19.03.2019
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.Половина),
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.Ноль), 
#КонецВставки
	|						ЗНАЧЕНИЕ(Перечисление.СпособыПогашенияСтоимостиТМЦ.ПоНаработке))
	|	И (ПартииТМЦ.ДатаЗавершенияЭксплуатации > &НачДата
	|		ИЛИ ПартииТМЦ.ДатаЗавершенияЭксплуатации = ДАТАВРЕМЯ(1, 1, 1))
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВложенныйЗапрос.Организация,
	|	ВложенныйЗапрос.Партия,
	|	ВложенныйЗапрос.НомерПакета
	|ПОМЕСТИТЬ ПакетыПогашенияСтоимостиТМЦ
	|ИЗ
	|	(ВЫБРАТЬ
	|		ТМЦВЭксплуатации.Организация КАК Организация,
	|		ТМЦВЭксплуатации.Партия КАК Партия,
	|		ЕСТЬNULL(ПакетыПогашенияСтоимостиТМЦ.НомерПакета, 0) КАК НомерПакета
	|	ИЗ
	|		РегистрНакопления.ТМЦВЭксплуатации.Обороты(
	|				,
	|				&НачДата,
	|				,
	|				(Организация В (&СписокОрганизаций)
	|					ИЛИ &ПоВсемОрганизациям)
	|				И Партия В (ВЫБРАТЬ ПартииТМЦ.Ссылка ИЗ ПартииТМЦ КАК ПартииТМЦ)
	|		) КАК ТМЦВЭксплуатации
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|			ПО ТМЦВЭксплуатации.Партия = ПакетыПогашенияСтоимостиТМЦ.Партия
	|				И ТМЦВЭксплуатации.Организация = ПакетыПогашенияСтоимостиТМЦ.Организация
	|	ГДЕ
	|		ТМЦВЭксплуатации.КоличествоОборот > 0
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ВозвратИзЭксплуатации.Организация,
	|		ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации,
	|		ЕСТЬNULL(ПакетыПогашенияСтоимостиТМЦ.НомерПакета, 0)
	|	ИЗ
	|		Документ.ПрочееОприходованиеТоваров КАК ВозвратИзЭксплуатации
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПрочееОприходованиеТоваров.Товары КАК ВозвратИзЭксплуатацииТовары
	|			ПО ВозвратИзЭксплуатации.Ссылка = ВозвратИзЭксплуатацииТовары.Ссылка
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.СуммыДокументовВВалютеРегл КАК СуммыДокументовВВалютеРегл
	|			ПО (ВозвратИзЭксплуатацииТовары.ИдентификаторСтроки = СуммыДокументовВВалютеРегл.ИдентификаторСтроки)
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|			ПО (ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ПакетыПогашенияСтоимостиТМЦ.Партия)
	|				И ВозвратИзЭксплуатации.Организация = ПакетыПогашенияСтоимостиТМЦ.Организация
	|	ГДЕ
	|		(ВозвратИзЭксплуатации.Организация В (&СписокОрганизаций)
	|				ИЛИ &ПоВсемОрганизациям)
	|		И ВозвратИзЭксплуатации.ХозяйственнаяОперация В (ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратИзЭксплуатации))
	|		И ВозвратИзЭксплуатации.Проведен
	|		И НЕ ВозвратИзЭксплуатации.ПометкаУдаления
	|		И ВозвратИзЭксплуатации.Дата МЕЖДУ &НачДата И &КонДата 
	#Вставка
	// ++EE:KMV 27.04.2023 MLVC-1083
	|		И Не ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ЗНАЧЕНИЕ(Справочник.ПартииТМЦВЭксплуатации.ПустаяСсылка)
	// --EE:KMV 27.04.2023 MLVC-1083
	#КонецВставки
	#Удаление
	|		И НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатации.Дата, МЕСЯЦ) <> НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ВозвратИзЭксплуатации.Организация,
	|		ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации,
	|		ЕСТЬNULL(ПакетыПогашенияСтоимостиТМЦ.НомерПакета, 0)
	|	ИЗ
	|		Документ.ПрочееОприходованиеТоваров КАК ВозвратИзЭксплуатации
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПрочееОприходованиеТоваров.Товары КАК ВозвратИзЭксплуатацииТовары
	|			ПО ВозвратИзЭксплуатации.Ссылка = ВозвратИзЭксплуатацииТовары.Ссылка
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.СуммыДокументовВВалютеРегл КАК СуммыДокументовВВалютеРегл
	|			ПО (ВозвратИзЭксплуатацииТовары.ИдентификаторСтроки = СуммыДокументовВВалютеРегл.ИдентификаторСтроки)
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|			ПО (ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ПакетыПогашенияСтоимостиТМЦ.Партия)
	|				И ВозвратИзЭксплуатации.Организация = ПакетыПогашенияСтоимостиТМЦ.Организация
	|	ГДЕ
	|		(ВозвратИзЭксплуатации.Организация В (&СписокОрганизаций)
	|				ИЛИ &ПоВсемОрганизациям)
	|		И ВозвратИзЭксплуатации.ХозяйственнаяОперация В (ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратИзЭксплуатации))
	|		И ВозвратИзЭксплуатации.Проведен
	|		И НЕ ВозвратИзЭксплуатации.ПометкаУдаления
	|		И ВозвратИзЭксплуатации.Дата МЕЖДУ &НачДатаПрошлогоМесяца И &НачДата
	|		И НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатации.Дата, МЕСЯЦ) = НАЧАЛОПЕРИОДА(ВозвратИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	|
	#КонецУдаления
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ПеремещениеВЭксплуатации.Организация,
	|		ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации,
	|		ЕСТЬNULL(ПакетыПогашенияСтоимостиТМЦ.НомерПакета, 0)
	|	ИЗ
	|		Документ.ПеремещениеВЭксплуатации КАК ПеремещениеВЭксплуатации
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПеремещениеВЭксплуатации.Товары КАК ПеремещениеВЭксплуатацииТовары
	|			ПО ПеремещениеВЭксплуатации.Ссылка = ПеремещениеВЭксплуатацииТовары.Ссылка
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|			ПО (ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ПакетыПогашенияСтоимостиТМЦ.Партия)
	|				И ПеремещениеВЭксплуатации.Организация = ПакетыПогашенияСтоимостиТМЦ.Организация
	|	ГДЕ
	|		(ПеремещениеВЭксплуатации.Организация В (&СписокОрганизаций)
	|				ИЛИ &ПоВсемОрганизациям)
	|		И ПеремещениеВЭксплуатации.Проведен
	|		И НЕ ПеремещениеВЭксплуатации.ПометкаУдаления
	|		И ПеремещениеВЭксплуатации.Дата МЕЖДУ &НачДата И &КонДата
	#Вставка
	// ++EE:KMV 27.04.2023 MLVC-1083
	|		И Не ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ЗНАЧЕНИЕ(Справочник.ПартииТМЦВЭксплуатации.ПустаяСсылка)
	// --EE:KMV 27.04.2023 MLVC-1083
	#КонецВставки
	#Удаление	
	|		И НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатации.Дата, МЕСЯЦ) <> НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	#КонецУдаления
	|	
	#Удаление
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ПеремещениеВЭксплуатации.Организация,
	|		ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации,
	|		ЕСТЬNULL(ПакетыПогашенияСтоимостиТМЦ.НомерПакета, 0)
	|	ИЗ
	|		Документ.ПеремещениеВЭксплуатации КАК ПеремещениеВЭксплуатации
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПеремещениеВЭксплуатации.Товары КАК ПеремещениеВЭксплуатацииТовары
	|			ПО ПеремещениеВЭксплуатации.Ссылка = ПеремещениеВЭксплуатацииТовары.Ссылка
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|			ПО (ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ПакетыПогашенияСтоимостиТМЦ.Партия)
	|				И ПеремещениеВЭксплуатации.Организация = ПакетыПогашенияСтоимостиТМЦ.Организация
	|	ГДЕ
	|		(ПеремещениеВЭксплуатации.Организация В (&СписокОрганизаций)
	|				ИЛИ &ПоВсемОрганизациям)
	|		И ПеремещениеВЭксплуатации.Проведен
	|		И НЕ ПеремещениеВЭксплуатации.ПометкаУдаления
	|		И ПеремещениеВЭксплуатации.Дата МЕЖДУ &НачДатаПрошлогоМесяца И &НачДата
	|		И НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатации.Дата, МЕСЯЦ) = НАЧАЛОПЕРИОДА(ПеремещениеВЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	#КонецУдаления
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		СписаниеИзЭксплуатации.Организация,
	|		СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации,
	|		ЕСТЬNULL(ПакетыПогашенияСтоимостиТМЦ.НомерПакета, 0)
	|	ИЗ
	|		Документ.СписаниеИзЭксплуатации КАК СписаниеИзЭксплуатации
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.СписаниеИзЭксплуатации.Товары КАК СписаниеИзЭксплуатацииТовары
	|			ПО СписаниеИзЭксплуатации.Ссылка = СписаниеИзЭксплуатацииТовары.Ссылка
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|			ПО (СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ПакетыПогашенияСтоимостиТМЦ.Партия)
	|				И СписаниеИзЭксплуатации.Организация = ПакетыПогашенияСтоимостиТМЦ.Организация
	|	ГДЕ
	|		(СписаниеИзЭксплуатации.Организация В (&СписокОрганизаций)
	|				ИЛИ &ПоВсемОрганизациям)
	|		И СписаниеИзЭксплуатации.Проведен
	|		И НЕ СписаниеИзЭксплуатации.ПометкаУдаления
	|		И СписаниеИзЭксплуатации.Дата МЕЖДУ &НачДата И &КонДата
	#Вставка
	// ++EE:KMV 27.04.2023 MLVC-1083
	|		И Не СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ЗНАЧЕНИЕ(Справочник.ПартииТМЦВЭксплуатации.ПустаяСсылка)
	// --EE:KMV 27.04.2023 MLVC-1083
	#КонецВставки
	#Удаление
	|		И НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатации.Дата, МЕСЯЦ) <> НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		СписаниеИзЭксплуатации.Организация,
	|		СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации,
	|		ЕСТЬNULL(ПакетыПогашенияСтоимостиТМЦ.НомерПакета, 0)
	|	ИЗ
	|		Документ.СписаниеИзЭксплуатации КАК СписаниеИзЭксплуатации
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.СписаниеИзЭксплуатации.Товары КАК СписаниеИзЭксплуатацииТовары
	|			ПО СписаниеИзЭксплуатации.Ссылка = СписаниеИзЭксплуатацииТовары.Ссылка
	|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|			ПО (СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации = ПакетыПогашенияСтоимостиТМЦ.Партия)
	|				И СписаниеИзЭксплуатации.Организация = ПакетыПогашенияСтоимостиТМЦ.Организация
	|	ГДЕ
	|		(СписаниеИзЭксплуатации.Организация В (&СписокОрганизаций)
	|				ИЛИ &ПоВсемОрганизациям)
	|		И СписаниеИзЭксплуатации.Проведен
	|		И НЕ СписаниеИзЭксплуатации.ПометкаУдаления
	|		И СписаниеИзЭксплуатации.Дата МЕЖДУ &НачДатаПрошлогоМесяца И &НачДата
	|		И НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатации.Дата, МЕСЯЦ) = НАЧАЛОПЕРИОДА(СписаниеИзЭксплуатацииТовары.ПартияТМЦВЭксплуатации.Дата, МЕСЯЦ)
	#КонецУдаления
	|	) КАК ВложенныйЗапрос
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПакетыПогашенияСтоимостиТМЦ.Организация,
	|	ПакетыПогашенияСтоимостиТМЦ.НомерПакета,
	|	КОЛИЧЕСТВО(ПакетыПогашенияСтоимостиТМЦ.Партия) КАК ОбъемПакета
	|ИЗ
	|	ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|ГДЕ
	|	ПакетыПогашенияСтоимостиТМЦ.НомерПакета <> 0
	|
	|СГРУППИРОВАТЬ ПО
	|	ПакетыПогашенияСтоимостиТМЦ.Организация,
	|	ПакетыПогашенияСтоимостиТМЦ.НомерПакета
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПакетыПогашенияСтоимостиТМЦ.Организация,
	|	ПакетыПогашенияСтоимостиТМЦ.Партия
	|ИЗ
	|	ПакетыПогашенияСтоимостиТМЦ КАК ПакетыПогашенияСтоимостиТМЦ
	|ГДЕ
	|	ПакетыПогашенияСтоимостиТМЦ.НомерПакета = 0";

	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("НачДата", НачалоМесяца(Месяц));
	Запрос.УстановитьПараметр("КонДата", КонецМесяца(Месяц));
	Запрос.УстановитьПараметр("НачДатаПрошлогоМесяца", НачалоМесяца(НачалоМесяца(Месяц)-1));
	Запрос.УстановитьПараметр("СписокОрганизаций", Организация);
	Запрос.УстановитьПараметр("ПоВсемОрганизациям", НЕ ЗначениеЗаполнено(Организация));

	РезультатЗапроса = Запрос.ВыполнитьПакет();

	ОбъемПакетов = РезультатЗапроса[РезультатЗапроса.ВГраница()-1].Выгрузить();

	Выборка = РезультатЗапроса[РезультатЗапроса.ВГраница()].Выбрать();
	Пока Выборка.Следующий() Цикл

		НомерИОбъемПакета = ТекущиеНомераПакетов.Получить(Выборка.Организация);
		Если НомерИОбъемПакета = Неопределено 
			ИЛИ НомерИОбъемПакета.ОбъемПакета >= МаксимальныйОбъемПакета Тогда

			СледущийНомерПакета = ?(НомерИОбъемПакета = Неопределено, 0, НомерИОбъемПакета.НомерПакета);
			НомерИОбъемПакета = НомерИОбъемПакета(Выборка.Организация, СледущийНомерПакета, ОбъемПакетов, МаксимальныйОбъемПакета);

			ТекущиеНомераПакетов.Вставить(Выборка.Организация, НомерИОбъемПакета);

			ИзмененныйПакет = ИзмененныеПакеты.Добавить();
			ИзмененныйПакет.Организация = Выборка.Организация;
			ИзмененныйПакет.НомерПакета = НомерИОбъемПакета.НомерПакета;

		КонецЕсли; 

		НовыйПакетЗапись = РегистрыСведений.ПакетыПогашенияСтоимостиТМЦ.СоздатьМенеджерЗаписи();
		НовыйПакетЗапись.Организация = Выборка.Организация;
		НовыйПакетЗапись.Партия = Выборка.Партия;
		НовыйПакетЗапись.НомерПакета = НомерИОбъемПакета.НомерПакета;
		НовыйПакетЗапись.Записать();

		НомерИОбъемПакета.ОбъемПакета = НомерИОбъемПакета.ОбъемПакета + 1;

	КонецЦикла;

	Если ПланыОбмена.ГлавныйУзел() = Неопределено Тогда
		// В РИБ данный регистр обрабатывается только в главном узле.

		НомерЗадания = РегистрыСведений.ЗаданияКПогашениюСтоимостиТМЦВЭксплуатации.ПолучитьНомерЗадания();

		// Требуется сформировать задания, т.к. у партий появился номер пакета.
		// Если это не сделать, то потеряется информация о том, что для этих партий нужен пересчет.
		Для каждого ИзмененныйПакет Из ИзмененныеПакеты Цикл
			НовоеЗаданиеЗапись = РегистрыСведений.ЗаданияКПогашениюСтоимостиТМЦВЭксплуатации.СоздатьМенеджерЗаписи();
			НовоеЗаданиеЗапись.Месяц        = НачалоМесяца(Месяц);
			НовоеЗаданиеЗапись.НомерПакета  = ИзмененныйПакет.НомерПакета;
			НовоеЗаданиеЗапись.Организация  = ИзмененныйПакет.Организация;
			НовоеЗаданиеЗапись.НомерЗадания = НомерЗадания;
			НовоеЗаданиеЗапись.Записать();
		КонецЦикла;
	КонецЕсли;

КонецПроцедуры
 
#КонецОбласти 

#КонецЕсли
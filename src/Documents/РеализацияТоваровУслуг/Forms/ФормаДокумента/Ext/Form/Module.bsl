﻿// 4D:Милавица, АнастасияМ, 23.03.2021
// Документ «Реализация товаров и услуг» - настройка формы документа, № 28195
// В рамках задачи на форму добавлено программно:
// 1. Поля ввода "Количество" и "Всего"
// 2. Добавлена новая группа Чд_СуммаНДС
// 3. Для элемента ГруппаВсегоСкидка установлена Вертикальная группировка

// 4D:Милавица, АндрейБ, 24.10.2018 
// "Реализация учета материалов в разрезе паспортов кусков в 1С ERP" , № 18881
// {Контроль соответствия количества в ТЧ количеству указанному в паспорте куска (ДлинаФактическая)
&НаКлиенте
Процедура МЛ_ОбработкаВыбораПосле(ВыбранноеЗначение, ИсточникВыбора)
	
	Если НоменклатураКлиент.ЭтоУказаниеСерий(ИсточникВыбора) Тогда
		ЗаполнитьКоличествоПоПаспорту();
	КонецЕсли;
		
КонецПроцедуры

// Программное заполнение поля "КоличествоУпаковок" для номенклатуры по которой ведется учет паспортов
&НаКлиенте
Процедура МЛ_ТоварыСерияПриИзмененииПосле(Элемент)
	
	ЗаполнитьКоличествоПоПаспорту();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьКоличествоПоПаспорту()
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Если ТекущаяСтрока <> Неопределено Тогда
		
		Если ЗначениеЗаполнено(ТекущаяСтрока.Номенклатура) 
			И НЕ ОбщегоНазначенияУТВызовСервера.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.Номенклатура, "Чд_УчетПаспортов") Тогда
			
			Возврат;	
		КонецЕсли; 
		
		ПрежнееКоличество = ТекущаяСтрока.Количество;
		Если ЗначениеЗаполнено(ТекущаяСтрока.Серия) Тогда
			
			Если НЕ ПроверитьВозможностьПодбораПаспорта(Объект.Ссылка, ТекущаяСтрока.Серия, ЭтаФорма.УникальныйИдентификатор) Тогда
				ТекущаяСтрока.Серия = ПредопределенноеЗначение("Справочник.СерииНоменклатуры.ПустаяСсылка");
				ТекущаяСтрока.Количество = 0;
			Иначе
				КоличествоПоПаспорту = ПолучитьКоличествоПоПаспорту(ТекущаяСтрока.Серия, ПрежнееКоличество);
				ТекущаяСтрока.Количество = КоличествоПоПаспорту;
			КонецЕсли;
			
			
			Если ТекущаяСтрока.Количество <> ПрежнееКоличество Тогда
				Чд_ТоварыКоличествоУпаковокПриИзменении(ТекущаяСтрока);
			КонецЕсли;
		Иначе
			ТекущаяСтрока.Количество = 0;
			
			Если ТекущаяСтрока.Количество <> ПрежнееКоличество Тогда
				Чд_ТоварыКоличествоУпаковокПриИзменении(ТекущаяСтрока);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Чд_ТоварыКоличествоУпаковокПриИзменении(ТекущаяСтрока)
	
	СтруктураДействий = Новый Структура;
	ДобавитьВСтруктуруДействияПриИзмененииКоличестваУпаковок(СтруктураДействий, Объект);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	ПродажиКлиентСервер.РассчитатьИтоговыеПоказателиРеализации(ЭтаФорма);
	СкидкиНаценкиКлиент.СброситьФлагСкидкиРассчитаны(ЭтаФорма);
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьКоличествоПоПаспорту(Серия,ПрежнееКоличество)
	                                     
	КоличествоПоПаспорту = ПрежнееКоличество;
	
	Если ЗначениеЗаполнено(Серия.Чд_ПаспортКуска) Тогда
		КоличествоПоПаспорту = Справочники.Чд_ПаспортКуска.ПолучитьКоличествоОстатокНаСкладе(Серия.Чд_ПаспортКуска);
	КонецЕсли;
				
	Возврат КоличествоПоПаспорту;
	
КонецФункции

&НаСервереБезКонтекста
Функция ПроверитьВозможностьПодбораПаспорта(Ссылка, СерияСсылка, ИдентификаторНазначения)
	
	Результат = Истина;
	Паспорт = СерияСсылка.Чд_ПаспортКуска;
	Если Паспорт.Состояние = Перечисления.Чд_СостоянияПаспортовКуска.Зарезервирован 
		И Паспорт.ОснованиеДляСостояния <> Ссылка Тогда	
		Если ТипЗнч(Паспорт.ОснованиеДляСостояния) = Тип("ДокументСсылка.РасходныйОрдерНаТовары")
			И ЗначениеЗаполнено(Паспорт.ОснованиеДляСостояния) Тогда
			
			РаспоряжениеВРасходномОрдере = Документы.РасходныйОрдерНаТовары.ПолучитьРаспоряжение(Паспорт.ОснованиеДляСостояния, СерияСсылка);
			
			Если РаспоряжениеВРасходномОрдере <> Неопределено И РаспоряжениеВРасходномОрдере <> Ссылка И РаспоряжениеВРасходномОрдере.Проведен
				И (ТипЗнч(РаспоряжениеВРасходномОрдере) <> Тип("ДокументСсылка.ПеремещениеТоваров")
					ИЛИ ТипЗнч(РаспоряжениеВРасходномОрдере) <> Тип("ДокументСсылка.ЗаказКлиенту")) Тогда
				
				Результат = Ложь;
				ТекстСообщения = НСтр("ru='Паспорт %1 зарезервирован %2. Выбор не доступен.'");
				ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения,Паспорт,Паспорт.ОснованиеДляСостояния);
				Сообщение = Новый СообщениеПользователю;
				Сообщение.ИдентификаторНазначения = ИдентификаторНазначения;
				Сообщение.Текст = ТекстСообщения;
				Сообщение.Сообщить();
			КонецЕсли;
		ИначеЕсли ЗначениеЗаполнено(Паспорт.ОснованиеДляСостояния) 
			И (ТипЗнч(Паспорт.ОснованиеДляСостояния) = Тип("ДокументСсылка.ПеремещениеТоваров")
				ИЛИ ТипЗнч(РаспоряжениеВРасходномОрдере) = Тип("ДокументСсылка.ЗаказКлиенту")
				ИЛИ НЕ Паспорт.ОснованиеДляСостояния.Проведен) Тогда
			
			Результат = Истина;
		Иначе
			Результат = Ложь;
			ТекстСообщения = НСтр("ru='Паспорт %1 зарезервирован %2. Выбор не доступен.'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения,Паспорт,Паспорт.ОснованиеДляСостояния);
			Сообщение = Новый СообщениеПользователю;
			Сообщение.ИдентификаторНазначения = ИдентификаторНазначения;
			Сообщение.Текст = ТекстСообщения;
			Сообщение.Сообщить();
		КонецЕсли;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции
// }
// 4D АндрейБ,24.10.2018

// 4D:Милавица, ЕленаТ, 04.02.2021 
// "Ошибка. Печатная форма ТН-2 документа РТиУ" , № 28193
// При изменении договора изменяется Реквизит "Основание". 
&НаСервере
&Перед("ДоговорПриИзмененииСервер")
Процедура МЛ_ДоговорПриИзмененииСервер()
	Объект.Основание = ФормыДокументовСервер_Локализация.ПолучитьТекстОснованияДляПечати(Объект);  
	//++EE:IVS 14.02.2023 MLVC-1023
    Объект.ЭЭ_УсловияПоставки = Объект.Договор.ЭЭ_УсловияПоставки;
	//--EE:IVS 14.02.2023 MLVC-1023
КонецПроцедуры
// }4D
﻿
// 4D:Милавица, АндрейБ, 11.05.2019
// Задача № 21863 Доработка по ПР 'Проектирование МРМ Кладовщика для ТСД'
// {заполнение количества для номенклатуры с учетом по паспортам при выборе серии
#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура МЛ_ОбработкаВыбораПосле(ВыбранноеЗначение, ИсточникВыбора)
	
	Если НоменклатураКлиент.ЭтоУказаниеСерий(ИсточникВыбора) Тогда
		Если Элементы.Страницы.ТекущаяСтраница = Элементы.ГруппаТоварыОтбор Тогда
			ЗаполнитьКоличествоПоПаспорту("ТоварыОтбор");
		ИначеЕсли Элементы.Страницы.ТекущаяСтраница = Элементы.ГруппаТоварыРазмещение Тогда
			ЗаполнитьКоличествоПоПаспорту("ТоварыРазмещение");
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры


#КонецОбласти 

#Область ОбработчикиСобытийЭлементовТаблицыФормы

&НаКлиенте
Процедура МЛ_ТоварыРазмещениеСерияПриИзмененииПосле(Элемент)
	
	ЗаполнитьКоличествоПоПаспорту("ТоварыРазмещение");
	
КонецПроцедуры

&НаКлиенте
Процедура МЛ_ТоварыОтборСерияПриИзмененииПосле(Элемент)
	
	ЗаполнитьКоличествоПоПаспорту("ТоварыОтбор");
	
КонецПроцедуры

&НаКлиенте
Процедура МЛ_ТоварыОтборНоменклатураПриИзмененииПосле(Элемент)

	Перем ТекущаяСтрока;
	Перем СтруктураДействий;

	ТекущаяСтрока = Элементы.ТоварыОтбор.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	//4D:"Милавица", АндрейБ, 11.05.2019 
	//Задача № 21558 ДС №19.1 Доработки по складским процессам сырьевых складов Милавицы
	//{ 
	СтруктураДействий.Вставить("ЗаполнитьПризнакЧд_УчетПаспортовИспользуется", Новый Структура("Номенклатура", "Чд_УчетПаспортов"));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	// }4D
	
КонецПроцедуры

&НаКлиенте
Процедура МЛ_ТоварыРазмещениеНоменклатураПриИзмененииПосле(Элемент)
	
	Перем ТекущаяСтрока;
	Перем СтруктураДействий;

	ТекущаяСтрока = Элементы.ТоварыРазмещение.ТекущиеДанные;

	СтруктураДействий = Новый Структура;
	//4D:"Милавица", АндрейБ, 11.05.2019 
	//Задача № 21558 ДС №19.1 Доработки по складским процессам сырьевых складов Милавицы
	//{ 
	СтруктураДействий.Вставить("ЗаполнитьПризнакЧд_УчетПаспортовИспользуется", Новый Структура("Номенклатура", "Чд_УчетПаспортов"));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	// }4D
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ЗаполнитьКоличествоПоПаспорту(ИмяТЧ)
	
	ТекущаяСтрока = Элементы[ИмяТЧ].ТекущиеДанные;
	
	Если ТекущаяСтрока = Неопределено 
		ИЛИ (ЗначениеЗаполнено(ТекущаяСтрока.Номенклатура) 
		И НЕ ОбщегоНазначенияУТВызовСервера.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.Номенклатура, "Чд_УчетПаспортов")) Тогда
		
		Возврат;	
	КонецЕсли;
	
	ПрежнееКоличество = ТекущаяСтрока.Количество;
	Если ЗначениеЗаполнено(ТекущаяСтрока.Серия) Тогда
		КоличествоПоПаспорту = ПолучитьКоличествоПоПаспорту(ТекущаяСтрока.Серия, ПрежнееКоличество);
		ТекущаяСтрока.Количество = КоличествоПоПаспорту;
		Если ТекущаяСтрока.Количество <> ПрежнееКоличество Тогда
			СтруктураДействий = Новый Структура;
			СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковок");
			Если ИмяТЧ = "ТоварыОтбор" Тогда
				СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковокСуффикс", "Отобрано");
			Иначе
				СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковокСуффикс", "Размещено");
			КонецЕсли; 

			ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
		КонецЕсли;
	Иначе
		ТекущаяСтрока.Количество = 0;
		Если ТекущаяСтрока.Количество <> ПрежнееКоличество Тогда
			СтруктураДействий = Новый Структура;
			СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковок");
			Если ИмяТЧ = "ТоварыОтбор" Тогда
				СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковокСуффикс", "Отобрано");
			Иначе
				СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковокСуффикс", "Размещено");
			КонецЕсли; 

			ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьКоличествоПоПаспорту(Серия,ПрежнееКоличество)
	                                     
	КоличествоПоПаспорту = ПрежнееКоличество;
	
	Если ЗначениеЗаполнено(Серия) Тогда
		КоличествоПоПаспорту = Справочники.Чд_ПаспортКуска.ПолучитьКоличествоОстатокНаСкладе(Серия);
	КонецЕсли;
	
	Возврат КоличествоПоПаспорту;
		
КонецФункции

&НаСервере
&После("УстановитьУсловноеОформление")
Процедура МЛ_УстановитьУсловноеОформление()
	
	// при выборе номенклатуры с учетом по паспортам делать не доступным редактирование количества
	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТоварыРазмещениеКоличествоУпаковок.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение 	= Новый ПолеКомпоновкиДанных("Объект.ТоварыРазмещение.Чд_УчетПаспортов");
	ОтборЭлемента.ВидСравнения 		= ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение 	= Истина;

	Элемент.Оформление.УстановитьЗначениеПараметра("ТолькоПросмотр", Истина);
	
	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТоварыОтборКоличествоУпаковок.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение 	= Новый ПолеКомпоновкиДанных("Объект.ТоварыОтбор.Чд_УчетПаспортов");
	ОтборЭлемента.ВидСравнения 		= ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение 	= Истина;

	Элемент.Оформление.УстановитьЗначениеПараметра("ТолькоПросмотр", Истина);
	
	// выделять строки, у которых в ходе отобра возникли расхождения по количеству
	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТоварыОтбор.Имя);

	ГруппаОтбора = Элемент.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	ГруппаОтбора.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИ;
	
	ОтборЭлемента1 = ГруппаОтбора.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента1.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ТоварыОтбор.КоличествоУпаковок");
	ОтборЭлемента1.ВидСравнения = ВидСравненияКомпоновкиДанных.НеРавно;
	ОтборЭлемента1.ПравоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ТоварыОтбор.КоличествоУпаковокОтобрано");

	ОтборЭлемента2 = ГруппаОтбора.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента2.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Статус");
	ОтборЭлемента2.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента2.ПравоеЗначение = Перечисления.СтатусыОтборовРазмещенийТоваров.ВыполненоСОшибками;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветФона", ЦветаСтиля.ПолеСОшибкойФон);
	
	//
	
	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТоварыРазмещение.Имя);

	ГруппаОтбора = Элемент.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	ГруппаОтбора.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИ;
	
	ОтборЭлемента1 = ГруппаОтбора.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента1.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ТоварыРазмещение.КоличествоУпаковок");
	ОтборЭлемента1.ВидСравнения = ВидСравненияКомпоновкиДанных.НеРавно;
	ОтборЭлемента1.ПравоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ТоварыРазмещение.КоличествоУпаковокРазмещено");

	ОтборЭлемента2 = ГруппаОтбора.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента2.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Статус");
	ОтборЭлемента2.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента2.ПравоеЗначение = Перечисления.СтатусыОтборовРазмещенийТоваров.ВыполненоСОшибками;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветФона", ЦветаСтиля.ПолеСОшибкойФон);
	
КонецПроцедуры

&НаСервере
&ИзменениеИКонтроль("ПриЧтенииСозданииНаСервере")
Процедура МЛ_ПриЧтенииСозданииНаСервере()
	ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(Объект, Документы.ОтборРазмещениеТоваров);

	УстановитьВидимостьПоВидуОперации();
	НастроитьПоСтатусу();

	СтруктураУстанавливаемыхПараметров = Новый Структура;
	СтруктураУстанавливаемыхПараметров.Вставить("Склад",Объект.Склад);

	УстановитьПараметрыФункциональныхОпцийФормы(СтруктураУстанавливаемыхПараметров);

	ПараметрыЗаполненияРеквизитов = Новый Структура;
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются",
	Новый Структура("Номенклатура", "ХарактеристикиИспользуются"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакТипНоменклатуры",
	Новый Структура("Номенклатура", "ТипНоменклатуры"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакАртикул",
	Новый Структура("Номенклатура", "Артикул"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакТипИзмеряемойВеличины", Новый Структура("Номенклатура", "ТипИзмеряемойВеличины"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьВесУпаковки",Новый Структура("Номенклатура, Упаковка", "ВесУпаковки"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьОбъемУпаковки",Новый Структура("Номенклатура, Упаковка", "ОбъемУпаковки"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьЕдиницуИзмеренияВеса",Новый Структура("Номенклатура, Упаковка", "ЕдиницаИзмеренияВеса"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьЕдиницуИзмеренияОбъема",Новый Структура("Номенклатура, Упаковка", "ЕдиницаИзмеренияОбъема"));
#Вставка
	//4D:"Милавица", АндрейБ, 11.05.2019 
	//Задача № 21558 ДС №19.1 Доработки по складским процессам сырьевых складов Милавицы
	//{ 
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакЧд_УчетПаспортовИспользуется",
										Новый Структура("Номенклатура", "Чд_УчетПаспортов"));
#КонецВставки

	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.ТоварыОтбор, ПараметрыЗаполненияРеквизитов);
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.ТоварыРазмещение, ПараметрыЗаполненияРеквизитов);

	ПересчитатьВесОбъемВСтрокахТЧ();
	Элементы.ГруппаИнформация.Картинка = ОбщегоНазначенияКлиентСервер.КартинкаКомментария(Объект.Комментарий);


	Элементы.Помещение.Видимость = СкладыСервер.ИспользоватьСкладскиеПомещения(Объект.Склад,Объект.Дата);

КонецПроцедуры

&НаСервере
&ИзменениеИКонтроль("СкопироватьТовары")
Процедура МЛ_СкопироватьТовары(ИмяИсточника)

	ТаблицаТоваров = Объект[ИмяИсточника].Выгрузить();
#Удаление
	ТаблицаТоваров.Свернуть("Номенклатура,Характеристика,Назначение,Упаковка,Серия,ОбъемУпаковки,
							|ВесУпаковки,СтатусУказанияСерий,ХарактеристикиИспользуются,ТипНоменклатуры,Артикул,
							|Вес,Объем,ТипИзмеряемойВеличины,ЕдиницаИзмеренияОбъема,ЕдиницаИзмеренияВеса",
							"Количество,КоличествоУпаковок");
#КонецУдаления
#Вставка
	//4D:"Милавица", АндрейБ, 11.05.2019 14:30:16 
	//Задача № 21558 ДС №19.1 Доработки по складским процессам сырьевых складов Милавицы
	//{ 
	ТаблицаТоваров.Свернуть("Номенклатура,Характеристика,Назначение,Упаковка,Серия,ОбъемУпаковки,
							|ВесУпаковки,СтатусУказанияСерий,ХарактеристикиИспользуются,ТипНоменклатуры,Артикул,
							|Вес,Объем,ТипИзмеряемойВеличины,ЕдиницаИзмеренияОбъема,ЕдиницаИзмеренияВеса, Чд_УчетПаспортов",
							"Количество,КоличествоУпаковок");
#КонецВставки

	Если ИмяИсточника = "ТоварыРазмещение" Тогда
		Объект.ТоварыОтбор.Загрузить(ТаблицаТоваров);
		НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(Объект,ПараметрыУказанияСерий.Отбор);

	Иначе
		Объект.ТоварыРазмещение.Загрузить(ТаблицаТоваров);	
		НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(Объект,ПараметрыУказанияСерий.Размещение);

	КонецЕсли;	

КонецПроцедуры

#КонецОбласти 
// }4D
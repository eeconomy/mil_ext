﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ОбработчикиСобытий

// 4D:"Милавица", АндрейБ,  30.07.2019 
// Задача № 21961 ДС №23.1 Разработка Закрытие карты раскроя
// { создание ордера на отражение пересортицы для возврата рациональных остатков по карте раскроя
// заполняется на основании документа ПриходныйОрдерНаТовары
&После("ОбработкаЗаполнения")
Процедура МЛ_ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура")
		И ДанныеЗаполнения.Свойство("ПриходныйОрдер") Тогда
		
		Чд_ЗаполнитьНаОснованииПриходногоОрдера(ДанныеЗаполнения);
	КонецЕсли;
	
КонецПроцедуры 

// {пометка на удаление созданных программно связанных документов "ПересортицаТоваров"
&Перед("ОбработкаУдаленияПроведения")
Процедура МЛ_ОбработкаУдаленияПроведения(Отказ)
	
	 Если ЗначениеЗаполнено(Чд_ПриходныйОрдер) Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = 
			"ВЫБРАТЬ РАЗЛИЧНЫЕ
			|	ВЫРАЗИТЬ(СвязанныеДокументы.Ссылка КАК Документ.ПересортицаТоваров) КАК Ссылка,
			|	ВЫРАЗИТЬ(СвязанныеДокументы.Ссылка КАК Документ.ПересортицаТоваров).Представление КАК Представление,
			|	ВЫРАЗИТЬ(СвязанныеДокументы.Ссылка КАК Документ.ПересортицаТоваров).Проведен КАК Проведен,
			|	ВЫРАЗИТЬ(СвязанныеДокументы.Ссылка КАК Документ.ПересортицаТоваров).ПометкаУдаления КАК ПометкаУдаления
			|ИЗ
			|	КритерийОтбора.СвязанныеДокументы(&Ссылка) КАК СвязанныеДокументы";
		
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		РезультатЗапроса = Запрос.Выполнить();
		
		Если Не РезультатЗапроса.Пустой() Тогда
			Выборка = РезультатЗапроса.Выбрать();
			Пока Выборка.Следующий() Цикл
				Если НЕ Выборка.ПометкаУдаления Тогда
					ПересортицаОбъект = Выборка.Ссылка.ПолучитьОбъект();
					Попытка
						Если Выборка.Проведен Тогда
							ПересортицаОбъект.Записать(РежимЗаписиДокумента.ОтменаПроведения);
						КонецЕсли;
						ПересортицаОбъект.УстановитьПометкуУдаления(Истина);
					Исключение
						ТекстСообщения = НСтр("ru='Пометка удаления для документа ""%1"" не выполнена: '") + ИнформацияОбОшибке().Описание;
						ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Выборка.Представление);
						ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, Выборка.Ссылка, , ,Отказ);
						ЗаписьЖурналаРегистрации("Данные.Изменение", УровеньЖурналаРегистрации.Предупреждение, ,
												Чд_РаботаСТранзакциями.СсылкаДляПередачиВЖурналРегистрации(Выборка.Ссылка), ОписаниеОшибки());
						ВызватьИсключение;
					КонецПопытки;
				КонецЕсли;
			КонецЦикла;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры // }4D

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Описание:
// заполняет документ на основании приходного ордера по карте раскроя
// с типом хоз.операции = "ВозвратМатериаловИзПроизводства" или "ВозвратМатериаловИзКладовой"
// Параметры:
// 	ПриходныйОрдер  - <ДокументСсылка.ПриходныйОрдерНаТовары>
Процедура Чд_ЗаполнитьНаОснованииПриходногоОрдера(ДанныеЗаполнения)
	
	ПриходныйОрдер = ДанныеЗаполнения.ПриходныйОрдер;
	Если ПриходныйОрдер.ХозяйственнаяОперация <> Перечисления.ХозяйственныеОперации.ВозвратМатериаловИзПроизводства
		И ПриходныйОрдер.ХозяйственнаяОперация <> Перечисления.ХозяйственныеОперации.ВозвратМатериаловИзКладовой Тогда
		
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ПриходныйОрдерНаТовары.Помещение КАК Помещение,
		|	ПриходныйОрдерНаТовары.Склад КАК Склад,
		|	ПриходныйОрдерНаТовары.Ответственный КАК Ответственный,
		|	ПриходныйОрдерНаТовары.Ссылка КАК Чд_ПриходныйОрдер
		|ИЗ
		|	Документ.ПриходныйОрдерНаТовары КАК ПриходныйОрдерНаТовары
		|ГДЕ
		|	ПриходныйОрдерНаТовары.Ссылка = &ПриходныйОрдер
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ПриходныйОрдерНаТоварыТовары.Ссылка.ЗонаПриемки КАК Ячейка,
		|	ПриходныйОрдерНаТоварыТовары.Номенклатура КАК Номенклатура,
		|	ПриходныйОрдерНаТоварыТовары.Характеристика КАК Характеристика,
		|	ПриходныйОрдерНаТоварыТовары.Назначение КАК Назначение,
		|	ПриходныйОрдерНаТоварыТовары.Серия КАК Серия,
		|	ПриходныйОрдерНаТоварыТовары.СтатусУказанияСерий КАК СтатусУказанияСерий,
		|	ПриходныйОрдерНаТоварыТовары.Номенклатура КАК НоменклатураОприходование,
		|	ПриходныйОрдерНаТоварыТовары.Характеристика КАК ХарактеристикаОприходование,
		|	ПриходныйОрдерНаТоварыТовары.Назначение КАК НазначениеОприходование,
		|	ПриходныйОрдерНаТоварыТовары.СтатусУказанияСерий КАК СтатусУказанияСерийОприходование,
		|	ПриходныйОрдерНаТоварыТовары.Количество КАК Количество,
		|	ПриходныйОрдерНаТоварыТовары.Количество КАК КоличествоУпаковок,
		|	ПриходныйОрдерНаТоварыТовары.Количество КАК КоличествоОприходование
		|ИЗ
		|	Документ.ПриходныйОрдерНаТовары.Товары КАК ПриходныйОрдерНаТоварыТовары
		|ГДЕ
		|	ПриходныйОрдерНаТоварыТовары.Ссылка = &ПриходныйОрдер";
	
	Запрос.УстановитьПараметр("ПриходныйОрдер", ПриходныйОрдер);
	
	Результаты = Запрос.ВыполнитьПакет();
	
	// заполнение реквизитов документа
	РеквизитыДокумента = Результаты[0].Выбрать();
	РеквизитыДокумента.Следующий();
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, РеквизитыДокумента);
	ЭтотОбъект.Дата = ПриходныйОрдер.Дата + 1;
	
	ТекстКомментария = "Создан на основании ""%1"" по ""%2""";
	ТекстКомментария = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстКомментария,
												ПриходныйОрдер, ПриходныйОрдер.Чд_КартаРаскроя);
	Комментарий = ТекстКомментария;
	
	// заполнение ТЧ "Товары"
	Товары.Очистить();
	Товары.Загрузить(Результаты[1].Выгрузить());
	Для каждого Стр Из Товары Цикл
		Стр.СерияОприходование = ДанныеЗаполнения.СерияОприходование;
		Стр.Чд_ВозвратОстатка  = Истина;
	КонецЦикла;
	
	ПараметрыУказанияСерий = Документы.ОрдерНаОтражениеПересортицыТоваров.ПараметрыУказанияСерий(ЭтотОбъект);
	НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(ЭтотОбъект, ПараметрыУказанияСерий);
	
КонецПроцедуры  

#КонецОбласти 

#КонецЕсли
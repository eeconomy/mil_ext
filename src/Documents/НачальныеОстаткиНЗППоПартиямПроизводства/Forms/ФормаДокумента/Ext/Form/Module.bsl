﻿
&НаСервере
&ИзменениеИКонтроль("ТрудозатратыВидРаботПриИзмененииНаСервере")
Процедура МЛ_ТрудозатратыВидРаботПриИзмененииНаСервере(Идентификатор)

	ТекущиеДанные						= Объект.Трудозатраты.НайтиПоИдентификатору(Идентификатор);
	#Удаление
	ТекущиеДанные.НормативнаяРасценка	= Справочники.ВидыРаботСотрудников.ДействующаяРасценкаВидаРабот(ТекущиеДанные.ВидРабот, Объект.Дата);
	#КонецУдаления
	#Вставка 
	// ++EE:KMV 22.05.2023 SDEE-1937
	ТекущиеДанные.НормативнаяРасценка	= Справочники.ВидыРаботСотрудников.ДействующаяРасценкаВидаРабот(ТекущиеДанные.ВидРабот, Объект.Дата).Расценка;
	// ++EE:KMV 22.05.2023 SDEE-1937
	#КонецВставки
	
КонецПроцедуры

﻿// Добавила реквизит Чд_ПроизвольныйКурс АнастасияТ 

// 4D:Милавица, Михаил, 05.11.2019 
// Автоматическое создание/изменение документа Установка цен номенклатуры, №23666
// {
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура МЛ_ПриСозданииНаСервереПеред(Отказ, СтандартнаяОбработка)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) И ЗначениеЗаполнено(Объект.ДокументОснование)
		 И (ТипЗнч(Объект.ДокументОснование) = Тип("ДокументСсылка.ПриобретениеТоваровУслуг")
		 Или ТипЗнч(Объект.ДокументОснование) = Тип("ДокументСсылка.ПрочееОприходованиеТоваров")) Тогда
		 
		 Объект.Дата = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.ДокументОснование, "Дата");
	КонецЕсли;
	 	
КонецПроцедуры

&НаКлиенте
Процедура МЛ_ПриОткрытииПеред(Отказ)
	
	Если ВладелецФормы <> Неопределено
		И ТипЗнч(ВладелецФормы) = Тип("ФормаКлиентскогоПриложения") 
		И (ВладелецФормы.ИмяФормы = "Документ.ПриобретениеТоваровУслуг.Форма.ФормаДокумента"
		Или ВладелецФормы.ИмяФормы = "Документ.ПрочееОприходованиеТоваров.Форма.ФормаДокумента")
		И ВладелецФормы.Чд_СоздатьДокументУстановкаЦенНоменклатуры Тогда
		
		ФлагОткрытияФормы = Ложь;
		
		Объект.Дата = ПолучитьЗначениеДатыДокументаОснованияНаСервере(Объект.ДокументОснование);
		НомерВПределахДня = УстановкаЦенВызовСервера.РассчитатьНомерВПределахДня(Объект.Дата, Объект.Ссылка);
		Объект.Дата = УстановкаЦенКлиентСервер.РассчитатьДатуДокумента(Объект.Дата, НомерВПределахДня);
		
		АвтоВремя = ПредопределенноеЗначение("РежимАвтоВремя.НеИспользовать");
		УдалитьТоварыБезИзмененныхЦенНаСервере(); 
		Попытка
			ОбщегоНазначенияУТКлиент.Провести(ЭтотОбъект);
			Отказ = Истина;
		Исключение
			
		КонецПопытки;
		СтруктураПараметров = Новый Структура;
		СтруктураПараметров.Вставить("Ссылка", Объект.Ссылка);
		СтруктураПараметров.Вставить("Проведен", Объект.Проведен);
		Оповестить("УстановкаЦенПриПроведенииДокументаОснования", СтруктураПараметров);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура МЛ_ОбработкаВыбораПосле(ВыбранноеЗначение, ИсточникВыбора)
	
// ++EE:BAN 19.08.2022 MLVC-716	
	Если ИсточникВыбора.ИмяФормы = "Документ.УстановкаЦенНоменклатуры.Форма.ЭЭ_ФормаЗагрузкиПоМоделям"
		И ВыбранноеЗначение <> Неопределено Тогда
		
		РезультатЗагрузки = ЭЭ_ЗаполнитьДеревоЦен(ВыбранноеЗначение);
		Если Не РезультатЗагрузки.Выполнено Тогда
			ПоказатьПредупреждение(, РезультатЗагрузки.ОписаниеОшибки);
		Иначе
			ЗаголовокОповещения = НСтр("ru='Загрузка по моделям'");
			ТекстОповещения = НСтр("ru='Выполнена!'");
			ПоказатьОповещениеПользователя(ЗаголовокОповещения, , ТекстОповещения,
				БиблиотекаКартинок.ВыполненоУспешно, СтатусОповещенияПользователя.Информация);
		КонецЕсли;
	КонецЕсли;
// --EE:BAN 19.08.2022 MLVC-716
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// ++EE:BOP, 01.08.2022, MLVC-716 
// Загрузка из внешнего файла в документ "Установка цен номенклатуры"
&НаКлиенте
Процедура ЭЭ_ЗагрузитьПоМоделям(Команда) 

	ДанныеПоВидамЦен = Новый Соответствие;
	Для каждого Стр Из ВыбранныеЦены Цикл
		Если Стр.Выбрана Тогда
			ДанныеПоВидамЦен.Вставить(Стр.Идентификатор, Стр.ИмяКолонки);
		КонецЕсли;
	КонецЦикла;
	
	Если ДанныеПоВидамЦен.Количество() Тогда
		СтруктураПараметров = Новый Структура;
		СтруктураПараметров.Вставить("ДанныеПоВидамЦен", ДанныеПоВидамЦен);
		ОткрытьФорму("Документ.УстановкаЦенНоменклатуры.Форма.ЭЭ_ФормаЗагрузкиПоМоделям", СтруктураПараметров, ЭтотОбъект);
	КонецЕсли;
		
КонецПроцедуры
// --EE:BOP, 01.08.2022, MLVC-716

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура Чд_УдалитьТоварыБезИзмененныхЦенНаСервере()
	
	УстановкаЦенСервер.УдалитьТоварыБезИзмененныхЦен(ЭтотОбъект, Истина);
	
КонецПроцедуры

&НаСервере
Функция ПолучитьЗначениеДатыДокументаОснованияНаСервере(ДокументОснование)
	
	Возврат Общегоназначения.ЗначениеРеквизитаОбъекта(ДокументОснование, "Дата");
	
КонецФункции
// }
// 4D

&НаСервере
Функция ЭЭ_ЗаполнитьДеревоЦен(АдресВХранилище)
	
	РезультатЗагрузки = Новый Структура;
	РезультатЗагрузки.Вставить("Выполнено",      Истина);
	РезультатЗагрузки.Вставить("ОписаниеОшибки", "");
	
	ТаблицаДанных = ПолучитьИзВременногоХранилища(АдресВХранилище);
	Если ТипЗнч(ТаблицаДанных) = Тип("ТаблицаЗначений") И ТаблицаДанных.Количество() Тогда
		КэшДанных = УстановкаЦенСервер.ИнициализироватьСтруктуруКэшаДанных();
		ПустаяХарактеристика = Справочники.ХарактеристикиНоменклатуры.ПустаяСсылка();
		
		// Загрузка цен в ДеревоЦен
		Для Каждого СтруктураТовар Из ТаблицаДанных Цикл
			СтрокаДереваЦен = УстановкаЦенСервер.НайтиСтрокуДереваЦен(
				ЭтотОбъект,
				Новый Структура("Номенклатура, Характеристика", СтруктураТовар.Номенклатура, СтруктураТовар.Характеристика),
				КэшДанных);
			Если СтрокаДереваЦен = Неопределено Тогда
				
				СтрокаДереваЦен = УстановкаЦенСервер.НайтиСтрокуДереваЦен(
					ЭтотОбъект,
					Новый Структура("Номенклатура, Характеристика", СтруктураТовар.Номенклатура, ПустаяХарактеристика),
					КэшДанных);
				Если СтрокаДереваЦен = Неопределено Тогда
					
					СтрокаДереваЦен = ДеревоЦен.ПолучитьЭлементы().Добавить();
					СтрокаДереваЦен.Номенклатура = СтруктураТовар.Номенклатура;
					
					НоваяСтрока = КэшДанных.ТаблицаСоответствияНоменклатурыСтрокамДереваТоваров.Добавить();
					НоваяСтрока.Номенклатура = СтруктураТовар.Номенклатура;
					НоваяСтрока.СтрокаДереваЦен = СтрокаДереваЦен;
					
				КонецЕсли;
				
				Если ЗначениеЗаполнено(СтруктураТовар.Характеристика) Тогда
					
					СтрокаДереваЦен = СтрокаДереваЦен.ПолучитьЭлементы().Добавить();
					СтрокаДереваЦен.Номенклатура   = СтруктураТовар.Номенклатура;
					СтрокаДереваЦен.Характеристика = СтруктураТовар.Характеристика;
					
					НоваяСтрока = КэшДанных.ТаблицаСоответствияНоменклатурыСтрокамДереваТоваров.Добавить();
					НоваяСтрока.Номенклатура    = СтруктураТовар.Номенклатура;
					НоваяСтрока.Характеристика  = СтруктураТовар.Характеристика;
					НоваяСтрока.СтрокаДереваЦен = СтрокаДереваЦен;
					
				КонецЕсли;
				
			КонецЕсли;
			
			СтрокаДереваЦен[СтруктураТовар.ИмяКолонкиВидЦены] = СтруктураТовар.Цена;
			Если ИспользоватьУпаковкиНоменклатуры Тогда
				СтрокаДереваЦен["Упаковка" + СтруктураТовар.ИмяКолонкиВидЦены] = СтруктураТовар.Упаковка;
			КонецЕсли;
			СтрокаДереваЦен["ИзмененаВручную" + СтруктураТовар.ИмяКолонкиВидЦены] = Истина;
			
		КонецЦикла;
		
		УстановкаЦенСервер.ОбновитьСтарыеЦеныНоменклатуры(ЭтотОбъект, КэшДанных);
		
		// Автоматически рассчитывать нужно только те цены, по которым процент изменения которых
		// не изменяется и которые зависят от изменяемых.
		ВидыЦенДляРасчета = Новый Массив;
		Если РассчитыватьАвтоматически Тогда
			УстановкаЦенСервер.РассчитатьВычисляемыеЦены(
				ЭтотОбъект,
				КэшДанных.ТаблицаСоответствияНоменклатурыСтрокамДереваТоваров,
				КэшДанных,
				ВидыЦенДляРасчета,
				Ложь);
		Иначе
			УстановкаЦенСервер.УстановитьПризнакРучногоИзмененияДляВидовЦен(
				ЭтотОбъект,
				КэшДанных.ТаблицаСоответствияНоменклатурыСтрокамДереваТоваров,
				ВидыЦенДляРасчета,
				КэшДанных);
		КонецЕсли;
		
		ПереформироватьТаблицуЦен(Ложь);
	Иначе
		РезультатЗагрузки.Выполнено = Ложь;
		РезультатЗагрузки.ОписаниеОшибки = "Отсутствуют данные для загрузки";
	КонецЕсли;
	
	Возврат РезультатЗагрузки;
	
КонецФункции

#КонецОбласти

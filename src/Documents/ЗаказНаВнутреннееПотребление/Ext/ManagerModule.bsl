﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

&ИзменениеИКонтроль("ИнициализироватьДанныеДокумента")
Процедура МЛ_ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры)

	// Создание запроса инициализации движений и заполенение его параметров.
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);

	// Формирование текста запроса.
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблицаЗаказыНаВнутреннееПотребление(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаГрафикОтгрузкиТоваров(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаСвободныеОстатки(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаОбеспечениеЗаказов(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаТоварыКОтгрузке(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаРеестрДокументов(Запрос, ТекстыЗапроса, Регистры);

#Вставка
	// 4D:Милавица, АндрейБ, 06.01.2021 
	// "Реализация учета материалов в разрезе паспортов кусков в 1С ERP" , № 18881
	// изменение состояния паспорта при проведении документа, пометке на удаление, отмене проведения.
	Чд_ТекстЗапросаТаблицаИсторияСостоянийПоПаспортам(Запрос, ТекстыЗапроса, Регистры);
#КонецВставки
	// Исполнение запроса и выгрузка полученных таблиц для движений.
	ПроведениеСерверУТ.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);

КонецПроцедуры

// 4D:Милавица, АндрейБ,  06.01.2021 
// Задача №24933 Перевод номенклатуры (ведется учет по паспортам) с ед.измерения м2 в м
// { Реализовать хранение истории состояний по паспорту в РС
Функция Чд_ТекстЗапросаТаблицаИсторияСостоянийПоПаспортам(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "ТаблицаИсторияСостоянийПоПаспортам";
	
	ТекстЗапроса = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	&Период КАК Период,
		|	&Ссылка КАК ДокументРегистратор,
		|	спрПаспортКуска.Ссылка КАК ПаспортКуска,
		|	ЗНАЧЕНИЕ(Перечисление.Чд_СостоянияПаспортовКуска.Зарезервирован) КАК Состояние,
		|	&Ссылка КАК Основание
		|ИЗ
		|	Документ.ЗаказНаВнутреннееПотребление.Товары КАК Товары
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Чд_ПаспортКуска КАК спрПаспортКуска
		|		ПО Товары.Серия = спрПаспортКуска.Серия
		|ГДЕ
		|	Товары.Ссылка = &Ссылка
		|	И &Статус <> ЗНАЧЕНИЕ(Перечисление.СтатусыЗаказовМатериаловВПроизводство.КОбеспечению)
		|	И НЕ спрПаспортКуска.ПометкаУдаления
		|	И Товары.Номенклатура.Чд_УчетПаспортов";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции 
// }4D

#КонецОбласти

#КонецЕсли
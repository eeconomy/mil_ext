﻿
&После("ПередНачаломРаботыСистемы")
Процедура МЛ_ПередНачаломРаботыСистемы()
	
#Если ТонкийКлиент Тогда
	// ++EE:IVS 08.06.2023 SDEE-2139
	ПроверитьУстановкуШрифтаУтюжков();
	// --EE:IVS 08.06.2023 SDEE-2139
#КонецЕсли
	
КонецПроцедуры

// ++EE:IVS 08.06.2023 SDEE-2139
Процедура ПроверитьУстановкуШрифтаУтюжков()
	
	Попытка
		Файлы = НайтиФайлы("C:\Windows\Fonts", "ISO-Ginetex2012.ttf");
		Если НЕ Файлы.Количество() Тогда
			СистемнаяИнформация = Новый СистемнаяИнформация; 
			ВерсияОС = СистемнаяИнформация.ВерсияОС;
			Если Найти(ВерсияОС, "Microsoft Windows XP") = 0 Тогда
				ИмяФайла = "ISO-Ginetex2012.ttf"; 
				Макет = Чд_ОбщегоНазначенияСервер.ПолучитьМакетШрифтаУтюжков();
				objShell = Новый COMОбъект("Shell.Application");
				Каталог = КаталогВременныхФайлов();
				ПолноеИмяФайла = Каталог + ИмяФайла;
				Макет.Записать(ПолноеИмяФайла);
				objNameSpace = objShell.Namespace(Каталог);
				objFont      = objNameSpace.ParseName(ИмяФайла);
				objFont.InvokeVerb("Install");
				objFont = Неопределено;
				УдалитьФайлы(ПолноеИмяФайла);
			Иначе
				WshShell     = Новый COMОбъект("WScript.Shell");
				FSO          = Новый COMОбъект("Scripting.FileSystemObject");
				ПутьШрифты     = wshShell.SpecialFolders.Item("Fonts");
				ПолноеИмяФайла = ПутьШрифты + "\" + ИмяФайла;
				Макет.Записать(ПолноеИмяФайла);
				WshShell.Run("RunDll32.exe gdi32.dll,AddFontResourceA " + FSO.GetBaseName(ПолноеИмяФайла));
			КонецЕсли;	
		КонецЕсли;
	Исключение
		Чд_ОбщегоНазначенияСервер.ЗаписатьВЖурналРегистрации(КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
		
КонецПроцедуры
// --EE:IVS 08.06.2023 SDEE-2139
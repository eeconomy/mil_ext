﻿
&ИзменениеИКонтроль("ПолучитьСтрокиИзБуфераОбмена")
Функция МЛ_ПолучитьСтрокиИзБуфераОбмена(ПараметрыОтбора)
	
	ДанныеИзБуфераОбмена = ОбщегоНазначения.СтрокиИзБуфераОбмена();
	Если ДанныеИзБуфераОбмена.Источник = "Строки" Тогда
		СтрокиДляВставки = ДанныеИзБуфераОбмена.Данные;
	Иначе
		Возврат Новый ТаблицаЗначений;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ПараметрыОтбора) Тогда
		
		Запрос = Новый Запрос(
		"ВЫБРАТЬ
		|	ТЗ.ТоварнаяКатегория,
		|	ТЗ.РейтингПродаж,
		|	ТЗ.НоменклатураНабора,
		|	ТЗ.Номенклатура,
		|	ТЗ.ТипНоменклатуры,
		|	ТЗ.ХарактеристикаНабора,
		|	ТЗ.Характеристика,
		|	ТЗ.Содержание,
		|	ТЗ.Упаковка,
		|	ТЗ.КоличествоУпаковок,
		|	ТЗ.Количество,
		|	ТЗ.КатегорияЭксплуатации,
		|	ТЗ.ФизическоеЛицо,
		|	ТЗ.СтатьяРасходов,
		|	ТЗ.АналитикаРасходов,
		|	ТЗ.АналитикаАктивовПассивов,
		|	ТЗ.Цена,
		|	ТЗ.ПроцентРучнойСкидки,
		|	ТЗ.Склад,
		|	ТЗ.Партнер,
		|	ТЗ.Соглашение,
		|	ТЗ.Сумма,
		|	ТЗ.СуммаРучнойСкидки,
		|	ТЗ.СпособОпределенияСебестоимости,
		|	ТЗ.ДокументРеализации,
		|	ТЗ.Себестоимость,
		|	ТЗ.СебестоимостьБезНДС,
		|	ТЗ.СебестоимостьРегл,
		|	ТЗ.СебестоимостьПР,
		|	ТЗ.СебестоимостьВР,
		|	ТЗ.ВидЦеныСебестоимости,
		|	ТЗ.ДатаЗаполненияСебестоимостиПоВидуЦены,
		|	ТЗ.ВариантКомплектации,
		|	ТЗ.ДатаПоступления,
		|	ТЗ.ДатаОстатка,
		|	ТЗ.ДатаОтгрузки,
		|	ТЗ.ДатаСборкиРазборки
		//++ НЕ УТ
		|	,ТЗ.ВидРабот
		|	,ТЗ.СтатьяКалькуляции
		|	,ТЗ.Назначение
		|	,ТЗ.Спецификация
		|	,ТЗ.Отправитель
		|	,ТЗ.Подразделение
		|	,ТЗ.МестоХранения
		|	,ТЗ.НаправлениеВыпуска
		|	,ТЗ.Получатель
		|	,ТЗ.Исполнитель
		|	,ТЗ.Договор
		|	,ТЗ.Хранитель
		|	,ТЗ.Контрагент
		|	,ТЗ.ДатаЗапуска
		|	,ТЗ.ДатаВыпуска
		|	,ТЗ.ДатаВыпускаПродукцииПолуфабриката
		|	,ТЗ.Полуфабрикат
		|	,ТЗ.ДокументПередачи
		//-- НЕ УТ
		#Вставка
		// 4D:Милавица, Анастасия, 12.08.2019 
		// ДС №25.1 Доработки по проектному решению процессов раскроя связанные с учетом производственных операций
		// и трудозатрат (АРМ Рабочее место контролера, АРМ Выполнение производственных операций, Контрольный лист), № 22743 п.5.6
		// {
		|	,ТЗ.Цвет
		//4D:Милавица, АндрейС, 28.08.2019 
		// <ДС №25.1 Доработки по проектному решению процессов раскроя>, № 22743
		// {
		|	,ТЗ.РазмерИзделия
        |	,ТЗ.РазмерСобственнойЧашки	
		// 4D:Милавица, АнастасияТ, 11.02.2020 
		// ДС №38.1 Разработка по проектному решению «Нормирование трудозатрат процессов раскроя основных материалов, кружева (31 ,33 процессы)», № 24428 п.52
		// {
		|	,ТЗ.Показатель
		|	,ТЗ.КодОперации
		|	,ТЗ.ВидТехнологическойОперации
		|	,ТЗ.ТипОборудования
		|	,ТЗ.Значение
		|	,ТЗ.НулевоеЗначение
		|	,ТЗ.ОписаниеГруппы
		|	,ТЗ.ГруппаРазмераДеталей
		|	,ТЗ.МетодНастилания
		|	,ТЗ.Артикул
		|	,ТЗ.ГруппаАртикула
		// 4D:Милавица, ЕленаТ, 01.02.2021  
		// ДС7 "Доработка документа Карта на раскрой ткани и расход фурнитуры", № 27904 п.7
		// {
		|	,ТЗ.ШиринаТкани
		|	,ТЗ.ШиринаРаскладки
		|	,ТЗ.ДлинаРаскладки
		|	,ТЗ.НормаНаЕдиницу
		|	,ТЗ.НормаНаВыпуск
		|	,ТЗ.ЕстьАналогиМатериала
		#КонецВставки
		|ПОМЕСТИТЬ Т
		|ИЗ
		|	&ТЗ КАК ТЗ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВЫБОР 
		|		КОГДА Т.Номенклатура <> ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка) 
		|		ТОГДА Т.Номенклатура.ТоварнаяКатегория 
		|		ИНАЧЕ Т.ТоварнаяКатегория 
		|	КОНЕЦ КАК ТоварнаяКатегория,
		|	ВЫБОР 
		|		КОГДА Т.Номенклатура <> ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка) 
		|		ТОГДА Т.Номенклатура.РейтингПродаж 
		|		ИНАЧЕ Т.РейтингПродаж 
		|	КОНЕЦ КАК РейтингПродаж,
		|	Т.НоменклатураНабора,
		|	Т.Номенклатура,
		|	Т.Номенклатура.ТипНоменклатуры КАК ТипНоменклатуры,
		|	Т.ХарактеристикаНабора,
		|	Т.Характеристика,
		|	Т.Содержание,
		|	Т.Упаковка,
		|	Т.КоличествоУпаковок,
		|	Т.Количество,
		|	Т.КатегорияЭксплуатации,
		|	Т.ФизическоеЛицо,
		|	Т.СтатьяРасходов,
		|	Т.АналитикаРасходов,
		|	Т.АналитикаАктивовПассивов,
		|	Т.Цена,
		|	Т.ПроцентРучнойСкидки,
		|	Т.Склад,
		|	Т.Партнер,
		|	Т.Соглашение,
		|	Т.Сумма,
		|	Т.СуммаРучнойСкидки,
		|	Т.СпособОпределенияСебестоимости,
		|	Т.ДокументРеализации,
		|	Т.Себестоимость,
		|	Т.СебестоимостьБезНДС,
		|	Т.СебестоимостьРегл,
		|	Т.СебестоимостьПР,
		|	Т.СебестоимостьВР,
		|	Т.ВидЦеныСебестоимости,
		|	Т.ДатаЗаполненияСебестоимостиПоВидуЦены,
		|	Т.ВариантКомплектации,
		|	Т.ДатаПоступления,
		|	Т.ДатаОстатка,
		|	Т.ДатаОтгрузки,
		|	Т.ДатаСборкиРазборки
		//++ НЕ УТ
		|	,Т.ВидРабот
		|	,Т.СтатьяКалькуляции
		|	,Т.Назначение
		|	,Т.Спецификация
		|	,Т.Отправитель
		|	,Т.Подразделение
		|	,Т.МестоХранения
		|	,Т.НаправлениеВыпуска
		|	,Т.Получатель
		|	,Т.Исполнитель
		|	,Т.Договор
		|	,Т.Хранитель
		|	,Т.Контрагент
		|	,Т.ДатаЗапуска
		|	,Т.ДатаВыпуска
		|	,Т.ДатаВыпускаПродукцииПолуфабриката
		|	,Т.Полуфабрикат
		|	,Т.ДокументПередачи
		//-- НЕ УТ
		#Вставка
		// 4D:Милавица, Анастасия, 12.08.2019 
		// ДС №25.1 Доработки по проектному решению процессов раскроя связанные с учетом производственных операций
		// и трудозатрат (АРМ Рабочее место контролера, АРМ Выполнение производственных операций, Контрольный лист), № 22743 п.5.6
		// {
		|	,Т.Цвет
		//4D:Милавица, АндрейС, 28.08.2019 
		// <ДС №25.1 Доработки по проектному решению процессов раскроя>, № 22743
		// {
		|	,Т.РазмерИзделия
        |	,Т.РазмерСобственнойЧашки
		// 4D:Милавица, АнастасияТ, 11.02.2020 
		// ДС №38.1 Разработка по проектному решению «Нормирование трудозатрат процессов раскроя основных материалов, кружева (31 ,33 процессы)», № 24428 п.52
		// {
		|	,Т.Показатель
		|	,Т.КодОперации
		|	,Т.ВидТехнологическойОперации
		|	,Т.ТипОборудования
		|	,Т.Значение
		|	,Т.НулевоеЗначение
		|	,Т.ОписаниеГруппы
		|	,Т.ГруппаРазмераДеталей
		|	,Т.МетодНастилания
		|	,Т.Артикул
		|	,Т.ГруппаАртикула
		// 4D:Милавица, ЕленаТ, 01.02.2021  
		// ДС7 "Доработка документа Карта на раскрой ткани и расход фурнитуры", № 27904 п.7
		// {
		|	,Т.ШиринаТкани
		|	,Т.ШиринаРаскладки
		|	,Т.ДлинаРаскладки
		|	,Т.НормаНаЕдиницу
		|	,Т.НормаНаВыпуск
		|	,Т.ЕстьАналогиМатериала
		#КонецВставки		
		|ИЗ
		|	Т КАК Т
		|ГДЕ
		|	ИСТИНА
		|	%ТекстУсловияОтбор%");
		
		Запрос.УстановитьПараметр("ТЗ", СтрокиДляВставки);
		ТекстУсловияОтбор = "";
		
		Если ПараметрыОтбора.Свойство("ОтборПоТипуНоменклатуры") Тогда
			ТекстУсловияОтбор = "И Т.Номенклатура.ТипНоменклатуры В (&ОтборПоТипуНоменклатуры)";
			Запрос.УстановитьПараметр("ОтборПоТипуНоменклатуры", ПараметрыОтбора.ОтборПоТипуНоменклатуры);
		КонецЕсли;
		
		Запрос.Текст = СтрЗаменить(Запрос.Текст,"%ТекстУсловияОтбор%", ТекстУсловияОтбор);
		
		СтрокиДляВставки = Запрос.Выполнить().Выгрузить();
		
	КонецЕсли;
	
	Возврат СтрокиДляВставки
КонецФункции

&ИзменениеИКонтроль("ИнициализироватьТаблицуБуфераОбмена")
Функция МЛ_ИнициализироватьТаблицуБуфераОбмена()
	
	ТаблицаСтрок = Новый ТаблицаЗначений();
	ОписаниеТиповДенежногоПоля = ОбщегоНазначенияУТ.ОписаниеТипаДенежногоПоля();
	ОписаниеТиповЧисло15_3 = ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(15,3);
	ТаблицаСтрок.Колонки.Добавить("ТоварнаяКатегория", Новый ОписаниеТипов("СправочникСсылка.ТоварныеКатегории"));
	ТаблицаСтрок.Колонки.Добавить("РейтингПродаж", Новый ОписаниеТипов("СправочникСсылка.РейтингиПродажНоменклатуры"));
	ТаблицаСтрок.Колонки.Добавить("НоменклатураНабора", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаСтрок.Колонки.Добавить("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаСтрок.Колонки.Добавить("УпаковочныйЛист", Новый ОписаниеТипов("ДокументСсылка.УпаковочныйЛист"));
	ТаблицаСтрок.Колонки.Добавить("ТипНоменклатуры", Новый ОписаниеТипов("ПеречислениеСсылка.ТипыНоменклатуры"));
	ТаблицаСтрок.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	ТаблицаСтрок.Колонки.Добавить("ХарактеристикаНабора", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	ТаблицаСтрок.Колонки.Добавить("Содержание", Новый ОписаниеТипов("Строка"));
	ТаблицаСтрок.Колонки.Добавить("Упаковка", Новый ОписаниеТипов("СправочникСсылка.УпаковкиЕдиницыИзмерения"));
	ТаблицаСтрок.Колонки.Добавить("КоличествоУпаковок", ОписаниеТиповЧисло15_3);
	ТаблицаСтрок.Колонки.Добавить("Количество", ОписаниеТиповЧисло15_3);
	ТаблицаСтрок.Колонки.Добавить("Цена", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("ПроцентРучнойСкидки", ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(5,2));
	ТаблицаСтрок.Колонки.Добавить("СуммаРучнойСкидки", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("НаправлениеДеятельности", Новый ОписаниеТипов("СправочникСсылка.НаправленияДеятельности"));
	ТаблицаСтрок.Колонки.Добавить("СтатьяРасходов", Новый ОписаниеТипов("ПланВидовХарактеристикСсылка.СтатьиРасходов,
																	|ПланВидовХарактеристикСсылка.СтатьиАктивовПассивов"));
	ТаблицаСтрок.Колонки.Добавить("АналитикаРасходов", Метаданные.ПланыВидовХарактеристик.СтатьиРасходов.Тип);
	ТаблицаСтрок.Колонки.Добавить("АналитикаАктивовПассивов", Метаданные.ПланыВидовХарактеристик.СтатьиАктивовПассивов.Тип);
	ТаблицаСтрок.Колонки.Добавить("ФизическоеЛицо", Новый ОписаниеТипов("СправочникСсылка.ФизическиеЛица"));
	ТаблицаСтрок.Колонки.Добавить("СуммаРасходов", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("ДатаОтражения", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("СтатьяДоходов", Новый ОписаниеТипов("ПланВидовХарактеристикСсылка.СтатьиДоходов"));
	ТаблицаСтрок.Колонки.Добавить("СуммаДоходов", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("Сумма", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СуммаБезНДС", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СуммаРегл", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СуммаПР", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СуммаВР", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СпособОпределенияСебестоимости", Новый ОписаниеТипов("ПеречислениеСсылка.СпособыОпределенияСебестоимости"));
	ТаблицаСтрок.Колонки.Добавить("ДокументРеализации", Документы.ТипВсеСсылки());
	ТаблицаСтрок.Колонки.Добавить("Себестоимость", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СебестоимостьБезНДС", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СебестоимостьРегл", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СебестоимостьПР", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("СебестоимостьВР", ОписаниеТиповДенежногоПоля);
	ТаблицаСтрок.Колонки.Добавить("ВидЦеныСебестоимости", Новый ОписаниеТипов("СправочникСсылка.ВидыЦен"));
	ТаблицаСтрок.Колонки.Добавить("ДатаЗаполненияСебестоимостиПоВидуЦены", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("Склад", Новый ОписаниеТипов("СправочникСсылка.Склады"));
	ТаблицаСтрок.Колонки.Добавить("Партнер", Новый ОписаниеТипов("СправочникСсылка.Партнеры"));
	ТаблицаСтрок.Колонки.Добавить("Соглашение", Новый ОписаниеТипов("СправочникСсылка.СоглашенияСПоставщиками,
																	|СправочникСсылка.СоглашенияСКлиентами"));
	ТаблицаСтрок.Колонки.Добавить("ВариантКомплектации", Новый ОписаниеТипов("СправочникСсылка.ВариантыКомплектацииНоменклатуры"));
	
	ТаблицаСтрок.Колонки.Добавить("ДатаПоступления", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("ДатаОстатка", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("ДатаОтгрузки", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("ДатаСборкиРазборки", Новый ОписаниеТипов("Дата"));
	
	//++ НЕ УТ
	ТаблицаСтрок.Колонки.Добавить("ДокументПередачи", Документы.ТипВсеСсылки());
	ТаблицаСтрок.Колонки.Добавить("ВидРабот", Новый ОписаниеТипов("СправочникСсылка.ВидыРаботСотрудников"));
	ТаблицаСтрок.Колонки.Добавить("СтатьяКалькуляции", Новый ОписаниеТипов("СправочникСсылка.СтатьиКалькуляции"));
	ТаблицаСтрок.Колонки.Добавить("Назначение", Новый ОписаниеТипов("СправочникСсылка.Назначения"));
	ТаблицаСтрок.Колонки.Добавить("Спецификация", Новый ОписаниеТипов("СправочникСсылка.РесурсныеСпецификации"));
	ТаблицаСтрок.Колонки.Добавить("МестоХранения", Новый ОписаниеТипов("ПеречислениеСсылка.ТипыМестХранения"));
	ТаблицаСтрок.Колонки.Добавить("Подразделение", Новый ОписаниеТипов("СправочникСсылка.СтруктураПредприятия"));
	ТаблицаСтрок.Колонки.Добавить("Договор", Новый ОписаниеТипов("СправочникСсылка.ДоговорыКонтрагентов"));
	ТаблицаСтрок.Колонки.Добавить("Хранитель", Новый ОписаниеТипов("СправочникСсылка.Партнеры"));
	ТаблицаСтрок.Колонки.Добавить("Контрагент", Новый ОписаниеТипов("СправочникСсылка.Контрагенты"));
	ТаблицаСтрок.Колонки.Добавить("Отправитель", Новый ОписаниеТипов("СправочникСсылка.СтруктураПредприятия,
																	|СправочникСсылка.Склады"));
	ТаблицаСтрок.Колонки.Добавить("НаправлениеВыпуска", Новый ОписаниеТипов("ПеречислениеСсылка.ХозяйственныеОперации"));
	ТаблицаСтрок.Колонки.Добавить("Получатель", Новый ОписаниеТипов("ПланВидовХарактеристикСсылка.СтатьиРасходов,
																	|СправочникСсылка.СтруктураПредприятия,
																	|ПланВидовХарактеристикСсылка.СтатьиАктивовПассивов,
																	|СправочникСсылка.Склады"));
	ТаблицаСтрок.Колонки.Добавить("Исполнитель", Новый ОписаниеТипов("СправочникСсылка.Бригады,
																	|СправочникСсылка.ФизическиеЛица"));
	
	ТаблицаСтрок.Колонки.Добавить("ДатаЗапуска", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("ДатаВыпуска", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("ДатаВыпускаПродукцииПолуфабриката", Новый ОписаниеТипов("Дата"));
	ТаблицаСтрок.Колонки.Добавить("Полуфабрикат", Новый ОписаниеТипов("Булево"));
	//-- НЕ УТ
	#Вставка
	// 4D:Милавица, Анастасия, 12.08.2019 
	// ДС №25.1 Доработки по проектному решению процессов раскроя связанные с учетом производственных операций
	// и трудозатрат (АРМ Рабочее место контролера, АРМ Выполнение производственных операций, Контрольный лист), № 22743 п.5.6
	// {
	ТаблицаСтрок.Колонки.Добавить("Цвет", 				Новый ОписаниеТипов("СправочникСсылка.Чд_Цвета"));
	//4D:Милавица, АндрейС, 28.08.2019 
	// <ДС №25.1 Доработки по проектному решению процессов раскроя>, № 22743
	// {
	ТаблицаСтрок.Колонки.Добавить("РазмерИзделия", 		Новый ОписаниеТипов("СправочникСсылка.Чд_РазмерыГотовыхИзделий"));
	ТаблицаСтрок.Колонки.Добавить("РазмерСобственнойЧашки", Новый ОписаниеТипов("СправочникСсылка.Чд_РазмерыСобственныхЧашек"));	
	// 4D:Милавица, АнастасияТ, 11.02.2020 
	// ДС №38.1 Разработка по проектному решению «Нормирование трудозатрат процессов раскроя основных материалов, кружева (31 ,33 процессы)», № 24428 п.52
	// {
	ТаблицаСтрок.Колонки.Добавить("Показатель", 		Новый ОписаниеТипов("СправочникСсылка.Чд_ПоказателиДляРасчетаТрудозатрат"));
	ТаблицаСтрок.Колонки.Добавить("КодОперации", 		Новый ОписаниеТипов("СправочникСсылка.Чд_КодыОпераций"));
	ТаблицаСтрок.Колонки.Добавить("ВидТехнологическойОперации", Новый ОписаниеТипов("СправочникСсылка.ВидыТехнологическихОпераций"));
	ТаблицаСтрок.Колонки.Добавить("ТипОборудования", 	Новый ОписаниеТипов("СправочникСсылка.Чд_ТипыОборудования"));
	ТаблицаСтрок.Колонки.Добавить("Значение", ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(14,4));  
	ТаблицаСтрок.Колонки.Добавить("НулевоеЗначение", 	Новый ОписаниеТипов("Булево"));
	ТаблицаСтрок.Колонки.Добавить("ОписаниеГруппы", 	Новый ОписаниеТипов("СправочникСсылка.Чд_ГруппыАртикулов"));
	ТаблицаСтрок.Колонки.Добавить("ГруппаРазмераДеталей", Новый ОписаниеТипов("СправочникСсылка.Чд_ГруппыРазмеровДеталей"));
	ТаблицаСтрок.Колонки.Добавить("МетодНастилания", 	Новый ОписаниеТипов("СправочникСсылка.Чд_МетодыНастилания"));
	ТаблицаСтрок.Колонки.Добавить("Артикул", 			Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаСтрок.Колонки.Добавить("ГруппаАртикула", 	Новый ОписаниеТипов("СправочникСсылка.Чд_ГруппыАртикулов"));
	// 4D:Милавица, ЕленаТ, 01.02.2021  
	// ДС7 "Доработка документа Карта на раскрой ткани и расход фурнитуры", № 27904 п.7
	// {
	ТаблицаСтрок.Колонки.Добавить("ШиринаТкани", 		ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(10,3));
	ТаблицаСтрок.Колонки.Добавить("ШиринаРаскладки", 	ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(10,3));
	ТаблицаСтрок.Колонки.Добавить("ДлинаРаскладки", 	ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(10,3));
	ТаблицаСтрок.Колонки.Добавить("НормаНаЕдиницу", 	ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(10,3)); 
	ТаблицаСтрок.Колонки.Добавить("НормаНаВыпуск", 		ОбщегоНазначенияУТ.ПолучитьОписаниеТиповЧисла(10,3));
	ТаблицаСтрок.Колонки.Добавить("ЕстьАналогиМатериала", Новый ОписаниеТипов("Булево"));
	#КонецВставки
	
	КопированиеСтрокСерверЛокализация.ИнициализироватьТаблицуБуфераОбмена(ТаблицаСтрок);
	
	Возврат ТаблицаСтрок;
	
КонецФункции

﻿
&ИзменениеИКонтроль("ПараметрыШтрихкодаCode128")
Функция МЛ_ПараметрыШтрихкодаCode128(Знач Штрихкод)
	
#Вставка
	// 4D:Милавица, Анастасия, 28.09.2018
	// Печать ШК упаковок и товаров, № 20337
	// {Ошибка вызова формы для печати этикеток
	НечитаемыеСимволы = НечитаемыеСимволыШтрихкодов();
	Для каждого Символ Из НечитаемыеСимволы Цикл
		Штрихкод = СтрЗаменить(Штрихкод, Символ, "");
	КонецЦикла;
#КонецВставки
	НомерФорматаCode128 = ФорматШтрихкодаCode128(Штрихкод);
	
	НомерФорматаCode128 = ФорматШтрихкодаCode128(Штрихкод);
	Если НомерФорматаCode128 = 1 Тогда
		ПараметрыШтрихкода = ПараметрыШтрихкодаCode128Формат1(Штрихкод);
	ИначеЕсли НомерФорматаCode128 = 2 Тогда
		ПараметрыШтрихкода = ПараметрыШтрихкодаCode128Формат2(Штрихкод);
	ИначеЕсли НомерФорматаCode128 = 3 Тогда
		ПараметрыШтрихкода = ПараметрыШтрихкодаCode128Формат3(Штрихкод);
	Иначе
		ПараметрыШтрихкода = Новый Структура;
		
		ТекстОшибки  = НСтр("ru = 'Формат штрихкода Code-128 не определен: общая длина штрихкода равна %1 символам, вместо %2';
							|en = 'Формат штрихкода Code-128 не определен: общая длина штрихкода равна %1 символам, вместо %2'");
		ТекстОшибки  = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			ТекстОшибки, СтрДлина(Штрихкод), НСтр("ru = '18,22 или 26, как установлено в формате';
													|en = '18,22 или 26, как установлено в формате'"));
		ПараметрыШтрихкода.Вставить("Результат", Неопределено);
		ПараметрыШтрихкода.Вставить("ТекстОшибки", ТекстОшибки);
	КонецЕсли;
	
	Возврат ПараметрыШтрихкода;
	
КонецФункции

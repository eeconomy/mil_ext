﻿
#Область ПрограммныйИнтерфейс

&ИзменениеИКонтроль("ПолучитьДанныеДляПечатнойФормыТОРГ12")
Функция МЛ_ПолучитьДанныеДляПечатнойФормыТОРГ12(ПараметрыПечати, МассивОбъектов) Экспорт
	
	КолонкаКодов = ФормированиеПечатныхФорм.ИмяДополнительнойКолонки();
	Если Не ЗначениеЗаполнено(КолонкаКодов) Тогда
		КолонкаКодов = "Код";
	КонецЕсли;
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.Текст = "
	|ВЫБРАТЬ
	|	ДанныеДокументов.Ссылка КАК Ссылка,
	|	ДанныеДокументов.Валюта КАК Валюта
	|
	|ПОМЕСТИТЬ ТаблицаДанныхДокументов
	|ИЗ
	|	Документ.ПередачаСырьяПереработчику КАК ДанныеДокументов
	|
	|ГДЕ
	|	ДанныеДокументов.Ссылка В (&МассивОбъектов)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Ссылка
	|;";
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	
	Запрос.Выполнить();
	
	ПоместитьВременнуюТаблицуТоваров(МенеджерВременныхТаблиц);
	ПродажиСервер.ПоместитьВременнуюТаблицуКоэффициентыУпаковок(МенеджерВременныхТаблиц, "ПередачаСырьяПереработчикуТаблицаТоваров");
	ОтветственныеЛицаСервер.СформироватьВременнуюТаблицуОтветственныхЛицДокументов(МассивОбъектов, МенеджерВременныхТаблиц);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	// 4D:ERP для Беларуси
	// Печать ТТН и ТН
	// {
	|	ПередачаСырьяПереработчику.Отпустил КАК Отпустил,
	|	ПередачаСырьяПереработчику.ОтпустилДолжность КАК ОтпустилДолжность,
	|	ПередачаСырьяПереработчику.НомерБланкаСтрогойОтчетности КАК НомерБланкаСтрогойОтчетности,
	|	ПередачаСырьяПереработчику.СерияБланкаСтрогойОтчетности КАК СерияБланкаСтрогойОтчетности,
	|	ПередачаСырьяПереработчику.Руководитель.Наименование КАК Разрешил,
	|	ПередачаСырьяПереработчику.Руководитель.Должность КАК РазрешилДолжность,
	|	ПередачаСырьяПереработчику.Принял КАК Принял,
	|	ПередачаСырьяПереработчику.ПереданыДокументы КАК ПереданыДокументы,
	// }
	// 4D
	|	ПередачаСырьяПереработчику.Ссылка                          КАК Ссылка,
	|	ПередачаСырьяПереработчику.Номер                           КАК Номер,
	|	ПередачаСырьяПереработчику.Дата                            КАК Дата,
	|	НЕОПРЕДЕЛЕНО КАК Статус,
	|	ПередачаСырьяПереработчику.Партнер                         КАК Партнер,
	|	ПередачаСырьяПереработчику.Контрагент                      КАК Контрагент,
	|	ВЫБОР
	|		КОГДА ПередачаСырьяПереработчику.Организация.ОбособленноеПодразделение
	|			ТОГДА ПередачаСырьяПереработчику.Организация.ГоловнаяОрганизация
	|		ИНАЧЕ ПередачаСырьяПереработчику.Организация
	|	КОНЕЦ КАК Организация,
	|	ТаблицаОтветственныеЛица.РуководительНаименование          КАК Руководитель,
	|	ТаблицаОтветственныеЛица.РуководительДолжность             КАК ДолжностьРуководителя,
	|	ТаблицаОтветственныеЛица.ГлавныйБухгалтерНаименование      КАК ГлавныйБухгалтер,
	|	ПередачаСырьяПереработчику.Отпустил           			   КАК Кладовщик,
	|	ПередачаСырьяПереработчику.ОтпустилДолжность               КАК ДолжностьКладовщика,
	|	ПередачаСырьяПереработчику.Организация.Префикс             КАК Префикс,
	|	ПередачаСырьяПереработчику.Основание                       КАК Основание,
	|	ПередачаСырьяПереработчику.ОснованиеДата                   КАК ОснованиеДата,
	|	ПередачаСырьяПереработчику.ОснованиеНомер                  КАК ОснованиеНомер,
	|	ВЫБОР КОГДА ПередачаСырьяПереработчику.Грузополучатель = ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка) ТОГДА
	|		ПередачаСырьяПереработчику.Контрагент
	|	ИНАЧЕ
	|		ПередачаСырьяПереработчику.Грузополучатель
	|	КОНЕЦ                                                      КАК Грузополучатель,
	|	ВЫБОР КОГДА ПередачаСырьяПереработчику.Грузоотправитель = ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка) ТОГДА
	|		ПередачаСырьяПереработчику.Организация
	|	ИНАЧЕ
	|		ПередачаСырьяПереработчику.Грузоотправитель
	|	КОНЕЦ                                                      КАК Грузоотправитель,
	|	ПередачаСырьяПереработчику.БанковскийСчетОрганизации       КАК БанковскийСчетОрганизации,
	|	ПередачаСырьяПереработчику.БанковскийСчетКонтрагента       КАК БанковскийСчетКонтрагента,
	|	ПередачаСырьяПереработчику.БанковскийСчетГрузоотправителя  КАК БанковскийСчетГрузоотправителя,
	|	ПередачаСырьяПереработчику.БанковскийСчетГрузополучателя   КАК БанковскийСчетГрузополучателя,
	|	ПередачаСырьяПереработчику.АдресДоставки                   КАК АдресДоставки,
	|	НЕОПРЕДЕЛЕНО                                               КАК Подразделение,
	|	ПередачаСырьяПереработчику.Валюта                          КАК Валюта,
	|	ПередачаСырьяПереработчику.ДоверенностьНомер               КАК ДоверенностьНомер,
	|	ПередачаСырьяПереработчику.ДоверенностьДата                КАК ДоверенностьДата,
	|	ПередачаСырьяПереработчику.ДоверенностьВыдана              КАК ДоверенностьВыдана,
	|	ПередачаСырьяПереработчику.ДоверенностьЛицо                КАК ДоверенностьЛицо,
	|	&ЕдиницаИзмеренияВеса                                      КАК ЕдиницаИзмеренияВеса
	|ИЗ
	|	Документ.ПередачаСырьяПереработчику КАК ПередачаСырьяПереработчику
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ТаблицаДанныхДокументов КАК ДанныеДокументов
	|		ПО ПередачаСырьяПереработчику.Ссылка = ДанныеДокументов.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаОтветственныеЛица КАК ТаблицаОтветственныеЛица
	|		ПО ПередачаСырьяПереработчику.Ссылка = ТаблицаОтветственныеЛица.Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТоваров.Ссылка                                      КАК Ссылка,
	|	ТаблицаТоваров.Номенклатура                                КАК Номенклатура,
	// 4D:ERP для Беларуси
	// Номенклатура поставщиков
	// {
	|	ВЫБОР
	|   	КОГДА ТаблицаТоваров.НоменклатураПоставщика = ЗНАЧЕНИЕ(Справочник.НоменклатураПоставщиков.ПустаяСсылка)  
	|			ТОГДА ТаблицаТоваров.Номенклатура.НаименованиеПолное
	|		ИНАЧЕ ТаблицаТоваров.НоменклатураПоставщика.Наименование
	|	КОНЕЦ КАК НоменклатураНаименование,
	|	ВЫБОР
	|   	КОГДА ТаблицаТоваров.НоменклатураПоставщика = ЗНАЧЕНИЕ(Справочник.НоменклатураПоставщиков.ПустаяСсылка)  
	|			ТОГДА ТаблицаТоваров.Номенклатура.Наименование
	|		ИНАЧЕ ТаблицаТоваров.НоменклатураПоставщика.Наименование
	|	КОНЕЦ КАК НоменклатураНаименованиеКраткое,
	// }
	// 4D
	|	ВЫБОР КОГДА &КолонкаКодов = ""Артикул"" ТОГДА
	|		ТаблицаТоваров.Номенклатура.Артикул
	|	ИНАЧЕ
	|		ТаблицаТоваров.Номенклатура.Код
	|	КОНЕЦ                                                      КАК НоменклатураКод,
	|	ВЫБОР
	|		КОГДА &ВыводитьБазовыеЕдиницыИзмерения
	|			ТОГДА ТаблицаТоваров.Номенклатура.ЕдиницаИзмерения
	|		ИНАЧЕ &ТекстЗапросаЕдиницаИзмерения
	|	КОНЕЦ 													   КАК ЕдиницаИзмерения,
	|	ВЫБОР
	|		КОГДА &ВыводитьБазовыеЕдиницыИзмерения
	|			ТОГДА ТаблицаТоваров.Номенклатура.ЕдиницаИзмерения.Представление
	|		ИНАЧЕ &ТекстЗапросаНаименованиеЕдиницыИзмерения
	|	КОНЕЦ 													   КАК ЕдиницаИзмеренияНаименование,
	|	ВЫБОР
	|		КОГДА &ВыводитьБазовыеЕдиницыИзмерения
	|			ТОГДА ТаблицаТоваров.Номенклатура.ЕдиницаИзмерения.Код
	|		ИНАЧЕ &ТекстЗапросаКодЕдиницыИзмерения
	|	КОНЕЦ 													   КАК ЕдиницаИзмеренияКод,
	|	ТаблицаТоваров.Характеристика                              КАК Характеристика,
	|	ТаблицаТоваров.Характеристика.НаименованиеПолное           КАК ХарактеристикаНаименование,
	|	ТаблицаТоваров.Упаковка                                    КАК Упаковка,
	|	ВЫБОР КОГДА ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки, 1) = 1 ТОГДА
	|			""""
	|	ИНАЧЕ
	|		ТаблицаТоваров.Упаковка.Наименование
	|	КОНЕЦ                                                      КАК УпаковкаНаименование,
	|	ВЫБОР
	|		КОГДА &ВыводитьБазовыеЕдиницыИзмерения ТОГДА
	|			&ТекстЗапросаНаименованиеЕдиницыИзмерения
	|		ИНАЧЕ
	|			КоэффициентыУпаковок.ВидУпаковки.ЕдиницаИзмерения.Представление
	|	КОНЕЦ 													   КАК ВидУпаковки,
	|	ТаблицаТоваров.СтавкаНДС                                   КАК СтавкаНДС,
	|	НЕОПРЕДЕЛЕНО                                               КАК НомерГТД,
	|	НЕОПРЕДЕЛЕНО                                               КАК СтранаПроисхождения,
	|	ВЫБОР КОГДА НЕ &ВыводитьБазовыеЕдиницыИзмерения ТОГДА
	|		ТаблицаТоваров.КоличествоУпаковок
	|	ИНАЧЕ
	|		ТаблицаТоваров.Количество
	|	КОНЕЦ                                                      КАК Количество,
	|	ВЫБОР КОГДА &ВыводитьБазовыеЕдиницыИзмерения ТОГДА
	|		ТаблицаТоваров.КоличествоУпаковок
	|	ИНАЧЕ
	|		КоэффициентыУпаковок.Количество / КоэффициентыУпаковок.КоэффициентВложеннойУпаковки
	|	КОНЕЦ                                                      КАК КоличествоМест,
	|	ВЫБОР КОГДА НЕ &ВыводитьБазовыеЕдиницыИзмерения ТОГДА
	|		ВЫБОР КОГДА КоэффициентыУпаковок.Количество < КоэффициентыУпаковок.КоэффициентВложеннойУпаковки ТОГДА
	|			КоэффициентыУпаковок.Количество
	|		ИНАЧЕ
	|			КоэффициентыУпаковок.КоэффициентВложеннойУпаковки
	|		КОНЕЦ
	|	ИНАЧЕ
	|		ВЫБОР КОГДА ТаблицаТоваров.Упаковка = ЗНАЧЕНИЕ(Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка) ТОГДА
	|			1
	|		ИНАЧЕ
	|			&ТекстЗапросаКоэффициентУпаковки
	|		КОНЕЦ
	|	КОНЕЦ                                                      КАК КоличествоВОдномМесте,
	|	ВЫБОР КОГДА НЕ &ВыводитьБазовыеЕдиницыИзмерения ТОГДА
	|		ТаблицаТоваров.СуммаБезНДС / ТаблицаТоваров.КоличествоУпаковок
	|	ИНАЧЕ
	|		ТаблицаТоваров.СуммаБезНДС / ТаблицаТоваров.Количество
	|	КОНЕЦ                                                      КАК Цена,
	|	ТаблицаТоваров.СуммаБезНДС                                 КАК СуммаБезНДС,
	|	ТаблицаТоваров.СуммаНДС                                    КАК СуммаНДС,
	|	ТаблицаТоваров.СуммаБезНДС + ТаблицаТоваров.СуммаНДС       КАК СуммаСНДС,
	|	ТаблицаТоваров.Количество * &ТекстЗапросаВесНоменклатуры КАК МассаНетто,
	|	ВЫБОР КОГДА &ЗаполненаЕдиницаИзмеренияВеса ТОГДА
	|		ВЫБОР КОГДА ТаблицаТоваров.Упаковка.Вес ЕСТЬ NULL ТОГДА
	|			ТаблицаТоваров.Количество * &ТекстЗапросаВесУпаковки
	|		ИНАЧЕ
	|			ТаблицаТоваров.КоличествоУпаковок * &ТекстЗапросаВесУпаковки
	|		КОНЕЦ
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ                                                      КАК МассаБрутто,
	|	ТаблицаТоваров.НомерСтроки                                 КАК НомерСтроки,
	|	ЛОЖЬ                                                       КАК ЭтоВозвратнаяТара
	|ИЗ
	|	ПередачаСырьяПереработчикуТаблицаТоваров КАК ТаблицаТоваров
	|		ЛЕВОЕ СОЕДИНЕНИЕ КоэффициентыУпаковок КАК КоэффициентыУпаковок
	|		ПО ТаблицаТоваров.Ссылка = КоэффициентыУпаковок.Ссылка
	|			И ТаблицаТоваров.НомерСтроки = КоэффициентыУпаковок.НомерСтроки
	|			И (НЕ &ВыводитьБазовыеЕдиницыИзмерения)
	|ГДЕ
	|	(ТаблицаТоваров.ЭтоТовар
	|			ИЛИ &ВыводитьУслуги)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка,
	|	НомерСтроки
	|ИТОГИ ПО
	|	Ссылка";
	
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&ТекстЗапросаКоэффициентУпаковки",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки(
			"ТаблицаТоваров.Упаковка",
			"ТаблицаТоваров.Номенклатура"));
			
	Запрос.Текст = СтрЗаменить(
		Запрос.Текст, 
		"&ТекстЗапросаВесНоменклатуры",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаВесУпаковки(
			"ТаблицаТоваров.Номенклатура.ЕдиницаИзмерения",
			"ТаблицаТоваров.Номенклатура"));
		
	Запрос.Текст = СтрЗаменить(
		Запрос.Текст, 
		"&ТекстЗапросаВесУпаковки",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаВесУпаковки(
			"ТаблицаТоваров.Упаковка",
			"ТаблицаТоваров.Номенклатура"));
		
	Запрос.Текст = СтрЗаменить(
		Запрос.Текст,
		"&ТекстЗапросаЕдиницаИзмерения",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаЗначениеРеквизитаЕдиницыИзмерения(
			"Ссылка",
			"ТаблицаТоваров.Упаковка",
			"ТаблицаТоваров.Номенклатура"));
			
	Запрос.Текст = СтрЗаменить(
		Запрос.Текст,
		"&ТекстЗапросаНаименованиеЕдиницыИзмерения",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаЗначениеРеквизитаЕдиницыИзмерения(
			"Наименование",
			"ТаблицаТоваров.Упаковка",
			"ТаблицаТоваров.Номенклатура"));
	
	Запрос.Текст = СтрЗаменить(
		Запрос.Текст,
		"&ТекстЗапросаКодЕдиницыИзмерения",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаЗначениеРеквизитаЕдиницыИзмерения(
			"Код",
			"ТаблицаТоваров.Упаковка",
			"ТаблицаТоваров.Номенклатура"));
			
	Запрос.УстановитьПараметр("ВыводитьУслуги",                  ПараметрыПечати.ВыводитьУслуги);
	Запрос.УстановитьПараметр("КолонкаКодов",                    КолонкаКодов);
	Запрос.УстановитьПараметр("ЕдиницаИзмеренияВеса",            Константы.ЕдиницаИзмеренияВеса.Получить());
	Запрос.УстановитьПараметр("ЗаполненаЕдиницаИзмеренияВеса",   ЗначениеЗаполнено(Константы.ЕдиницаИзмеренияВеса.Получить()));
	Запрос.УстановитьПараметр("ВыводитьБазовыеЕдиницыИзмерения", Константы.ВыводитьБазовыеЕдиницыИзмерения.Получить());
	
	МассивРезультатов         = Запрос.ВыполнитьПакет();
	РезультатПоШапке          = МассивРезультатов[0];
	РезультатПоТабличнойЧасти = МассивРезультатов[1];
	
	СтруктураДанныхДляПечати = Новый Структура("РезультатПоШапке, РезультатПоТабличнойЧасти",
	                                               РезультатПоШапке, РезультатПоТабличнойЧасти);
	
	Возврат СтруктураДанныхДляПечати;
	
КонецФункции

#КонецОбласти
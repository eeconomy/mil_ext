﻿#Область ПрограммныйИнтерфейс

#Область ПолучениеСтруктурПараметровДляОбработкиТабличнойЧастиТовары

// 4D:Милавица, Анастасия, 11.08.2020 
// Ошибка пересчета строки ТЧ
// {	
// Возвращает структуру, содержащую поля значений, используемых для пересчета сумм НДС в строках табличной части 
// документа.
Функция ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ(Объект) Экспорт
		
	СтруктураЗаполненияЦены = Новый Структура;
	СтруктураЗаполненияЦены.Вставить("ЦенаВключаетНДС", Объект.ЦенаВключаетНДС);
	
	Возврат СтруктураЗаполненияЦены;
	
КонецФункции
// }
// 4D Анастасия

&Вместо("СтруктураКешируемойИнформацииПоШтрихкоду")
Функция МЛ_СтруктураКешируемойИнформацииПоШтрихкоду()

	ИнформацияПоШтрихкоду = ПродолжитьВызов();
	// 4D:Милавица, АндрейБ, 21.03.2019
	// Задача № 21558 ДС №19.1 Доработки по складским процессам сырьевых складов Милавицы
	// { 
	ИнформацияПоШтрихкоду.Вставить("Серия");
	ИнформацияПоШтрихкоду.Вставить("Чд_Распоряжение");
	// }4D
	
	Возврат ИнформацияПоШтрихкоду;
	
КонецФункции

&ИзменениеИКонтроль("ПересчитатьКоличествоЕдиницВСтрокеТЧ")
Процедура МЛ_ПересчитатьКоличествоЕдиницВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения)

	Перем УпаковкаНоменклатура;

	Если СтруктураДействий.Свойство("ПересчитатьКоличествоЕдиниц", УпаковкаНоменклатура) Тогда
		ПараметрыПересчета = НормализоватьПараметрыПересчетаЕдиниц(ТекущаяСтрока, УпаковкаНоменклатура);
#Удаление
		ДанныеУпаковки = ПолучитьКоэффициентУпаковки(ПараметрыПересчета.Упаковка, КэшированныеЗначения, ПараметрыПересчета.Номенклатура);
		Количество = ТекущаяСтрока.КоличествоУпаковок * ДанныеУпаковки.Коэффициент;

		Если ДанныеУпаковки.НужноОкруглятьКоличество
			И ПараметрыПересчета.НужноОкруглять Тогда
			ТекущаяСтрока.Количество = Окр(Количество, 0 ,РежимОкругления.Окр15как20);
		Иначе
			ТекущаяСтрока.Количество = Количество;
		КонецЕсли;
#КонецУдаления
#Вставка
		// 4D:Милавица, АндрейБ, 19.02.2019
		// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
		// {
		#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
			Если НЕ (ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "Серия")
				И ЗначениеЗаполнено(ТекущаяСтрока.Серия)
				И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.Серия, "Чд_ПризнакПромера")) Тогда
				
				ДанныеУпаковки = ПолучитьКоэффициентУпаковки(ПараметрыПересчета.Упаковка, КэшированныеЗначения,
																				ПараметрыПересчета.Номенклатура);
				Количество = ТекущаяСтрока.КоличествоУпаковок * ДанныеУпаковки.Коэффициент;
				Если ДанныеУпаковки.НужноОкруглятьКоличество
					И ПараметрыПересчета.НужноОкруглять Тогда
					
					ТекущаяСтрока.Количество = Окр(Количество, 0 ,РежимОкругления.Окр15как20);
				Иначе
					ТекущаяСтрока.Количество = Количество;
				КонецЕсли;
			Иначе
				Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "СерияОприходование")
					И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.СерияОприходование, "Чд_ПризнакПромера") Тогда
					
					Коэффициент = ПолучитьКоэффициентУпаковки(ПараметрыПересчета.Упаковка, КэшированныеЗначения,
															ПараметрыПересчета.Номенклатура, ТекущаяСтрока.СерияОприходование).Коэффициент;
				Иначе
					Коэффициент = ПолучитьКоэффициентУпаковки(ПараметрыПересчета.Упаковка, КэшированныеЗначения,
															ПараметрыПересчета.Номенклатура, ТекущаяСтрока.Серия).Коэффициент;
				КонецЕсли; 
				
				ТекущаяСтрока.Количество = ТекущаяСтрока.КоличествоУпаковок * Коэффициент;
			КонецЕсли;
		#Иначе
			Если НЕ (ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "Серия")
				И ЗначениеЗаполнено(ТекущаяСтрока.Серия)
				И ОбщегоНазначенияУТВызовСервера.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.Серия, "Чд_ПризнакПромера")) Тогда
				
				ДанныеУпаковки = ПолучитьКоэффициентУпаковки(ПараметрыПересчета.Упаковка, КэшированныеЗначения,
															ПараметрыПересчета.Номенклатура);
				Количество = ТекущаяСтрока.КоличествоУпаковок * ДанныеУпаковки.Коэффициент;
				Если ДанныеУпаковки.НужноОкруглятьКоличество
					И ПараметрыПересчета.НужноОкруглять Тогда
					ТекущаяСтрока.Количество = Окр(Количество, 0 ,РежимОкругления.Окр15как20);
				Иначе
					ТекущаяСтрока.Количество = Количество;
				КонецЕсли;
			КонецЕсли;
		#КонецЕсли
		// }4D 
#КонецВставки

	КонецЕсли;

КонецПроцедуры

&ИзменениеИКонтроль("ПересчитатьКоличествоЕдиницСуффиксВСтрокеТЧ")
Процедура МЛ_ПересчитатьКоличествоЕдиницСуффиксВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения)

	Перем ПараметрыПересчетаСуффикс;

	Если СтруктураДействий.Свойство("ПересчитатьКоличествоЕдиницСуффикс", ПараметрыПересчетаСуффикс) Тогда

		ПараметрыПересчета = НормализоватьПараметрыПересчетаЕдиницСуффикс(ТекущаяСтрока, ПараметрыПересчетаСуффикс);
#Удаление
		ДанныеУпаковки = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура);

		Количество = ТекущаяСтрока["КоличествоУпаковок" + ПараметрыПересчета.Суффикс] * ДанныеУпаковки.Коэффициент;

		Если ДанныеУпаковки.НужноОкруглятьКоличество
			И ПараметрыПересчета.НужноОкруглять Тогда
			ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = Окр(Количество, 0 ,РежимОкругления.Окр15как20);	
		Иначе
			ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = Количество;
		КонецЕсли;
#КонецУдаления
#Вставка
		// 4D:Милавица, АндрейБ, 19.02.2019
		// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
		// {
		#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
			Если НЕ (ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "Серия")
				И ЗначениеЗаполнено(ТекущаяСтрока.Серия)
				И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.Серия, "Чд_ПризнакПромера")) Тогда
				
				ДанныеУпаковки = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура);
				Количество = ТекущаяСтрока["КоличествоУпаковок" + ПараметрыПересчета.Суффикс] * ДанныеУпаковки.Коэффициент;
				Если ДанныеУпаковки.НужноОкруглятьКоличество
					И ПараметрыПересчета.НужноОкруглять Тогда
					
					ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = Окр(Количество, 0, РежимОкругления.Окр15как20);
				Иначе
					ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = Количество;
				КонецЕсли;
			Иначе
				Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "СерияОприходование")
					И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.СерияОприходование, "Чд_ПризнакПромера") Тогда
					
					Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения,
															ТекущаяСтрока.Номенклатура, ТекущаяСтрока.СерияОприходование).Коэффициент;
				Иначе
					Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения,
															ТекущаяСтрока.Номенклатура, ТекущаяСтрока.Серия).Коэффициент;
				КонецЕсли; 
				
				ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = ТекущаяСтрока["КоличествоУпаковок" + ПараметрыПересчета.Суффикс] * Коэффициент;
			КонецЕсли;
		#Иначе
			Если НЕ (ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "Серия")
				И ЗначениеЗаполнено(ТекущаяСтрока.Серия)
				И ОбщегоНазначенияУТВызовСервера.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.Серия, "Чд_ПризнакПромера")) Тогда
				
				ДанныеУпаковки = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура);
				Количество = ТекущаяСтрока["КоличествоУпаковок" + ПараметрыПересчета.Суффикс] * ДанныеУпаковки.Коэффициент;
				
				Если ДанныеУпаковки.НужноОкруглятьКоличество
					И ПараметрыПересчета.НужноОкруглять Тогда
					
					ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = Окр(Количество, 0, РежимОкругления.Окр15как20);
				Иначе
					ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = Количество;
				КонецЕсли;
			КонецЕсли;
		#КонецЕсли
		// }4D 
#КонецВставки

	КонецЕсли;

КонецПроцедуры

&ИзменениеИКонтроль("ПересчитатьКоличествоУпаковокВСтрокеТЧ")
Процедура МЛ_ПересчитатьКоличествоУпаковокВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения)
	
	Если СтруктураДействий.Свойство("ПересчитатьКоличествоУпаковок") Тогда
		Если ТекущаяСтрока.Количество = 0 Тогда
			ТекущаяСтрока.КоличествоУпаковок = 0;
		Иначе
#Удаление
			Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура).Коэффициент;
#КонецУдаления
#Вставка
			// 4D:Милавица, АндрейБ, 19.02.2019
			// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
			// {
			Коэффициент = 0;
			#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
				ЕстьРеквизитСерия = ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "Серия");
				Если ЕстьРеквизитСерия И ЗначениеЗаполнено(ТекущаяСтрока.Серия)
					И ТекущаяСтрока.Серия.Чд_ПризнакПромера Тогда
					
					Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "СерияОприходование")
						И ЗначениеЗаполнено(ТекущаяСтрока.СерияОприходование)
						И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.СерияОприходование, "Чд_ПризнакПромера") Тогда
						
						Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура, ТекущаяСтрока.СерияОприходование).Коэффициент;
					Иначе
						Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура, ТекущаяСтрока.Серия).Коэффициент;
					КонецЕсли; 
				Иначе
					Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура).Коэффициент;
				КонецЕсли;
			#Иначе
				Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура).Коэффициент;
			#КонецЕсли
			// }4D
#КонецВставки
			Если Коэффициент <> 0 Тогда
				ТекущаяСтрока.КоличествоУпаковок = ТекущаяСтрока.Количество / Коэффициент;
			Иначе
				ТекстИсключения = НСтр("ru = 'При попытке пересчета количества в %ЕдИзмерения% превышена допустимая разрядность.';
										|en = 'While trying to recalculate the amount in %ЕдИзмерения%, the allowable capacity exceeded.'");
				ТекстИсключения = СтрЗаменить(ТекстИсключения, "%ЕдИзмерения%", ТекущаяСтрока.Упаковка);
				
				ТекущаяСтрока.Количество = 0;
				ТекущаяСтрока.КоличествоУпаковок = 0;
				ТекущаяСтрока.Упаковка = ПредопределенноеЗначение("Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка");
				
				ВызватьИсключение ТекстИсключения;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

&ИзменениеИКонтроль("ПересчитатьКоличествоУпаковокСуффиксВСтрокеТЧ")
Процедура МЛ_ПересчитатьКоличествоУпаковокСуффиксВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения)
	
	Перем ПараметрыПересчетаСуффикс;
	
	Если СтруктураДействий.Свойство("ПересчитатьКоличествоУпаковокСуффикс",ПараметрыПересчетаСуффикс) Тогда
		ПараметрыПересчета = НормализоватьПараметрыПересчетаЕдиницСуффикс(ТекущаяСтрока, ПараметрыПересчетаСуффикс);
		Если ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = 0 Тогда
			ТекущаяСтрока["КоличествоУпаковок" + ПараметрыПересчета.Суффикс] = 0;
		Иначе
#Удаление
			Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура).Коэффициент;
#КонецУдаления
#Вставка
			// 4D:Милавица, АндрейБ, 19.02.2019
			// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
			// {
			Коэффициент = 0;
			#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
				ЕстьРеквизитСерия = ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "Серия");
				Если ЕстьРеквизитСерия И ЗначениеЗаполнено(ТекущаяСтрока.Серия)
					И ТекущаяСтрока.Серия.Чд_ПризнакПромера Тогда
					
					Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "СерияОприходование")
						И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.СерияОприходование, "Чд_ПризнакПромера") Тогда
						
						Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура, ТекущаяСтрока.СерияОприходование).Коэффициент;
					Иначе
						Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура, ТекущаяСтрока.Серия).Коэффициент;
					КонецЕсли; 
				Иначе
					Коэффициент = ПолучитьКоэффициентУпаковки(ТекущаяСтрока.Упаковка, КэшированныеЗначения, ТекущаяСтрока.Номенклатура).Коэффициент;
				КонецЕсли;
			#Иначе
				ТекстИсключения = НСтр("ru = 'Попытка получения коэффициента упаковки на клиенте.'");
				ВызватьИсключение ТекстИсключения;
			#КонецЕсли
			// }4D 
#КонецВставки
			Если Коэффициент <> 0 Тогда
				ТекущаяСтрока["КоличествоУпаковок" + ПараметрыПересчета.Суффикс] = ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] / Коэффициент;
			Иначе
				ТекстИсключения = НСтр("ru = 'При попытке пересчета количества в %ЕдИзмерения% превышена допустимая разрядность.';
										|en = 'While trying to recalculate the amount in %ЕдИзмерения%, the allowable capacity exceeded.'");
				ТекстИсключения = СтрЗаменить(ТекстИсключения, "%ЕдИзмерения%", ТекущаяСтрока.Упаковка);
				
				ТекущаяСтрока["Количество" + ПараметрыПересчета.Суффикс] = 0;
				ТекущаяСтрока["КоличествоУпаковок" + ПараметрыПересчета.Суффикс] = 0;
				ТекущаяСтрока.Упаковка = ПредопределенноеЗначение("Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка");
				
				ВызватьИсключение ТекстИсключения;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&ИзменениеИКонтроль("ПолучитьКоэффициентУпаковки")
Функция МЛ_ПолучитьКоэффициентУпаковки(ТекУпаковка, КэшированныеЗначения, ТекНоменклатура, ТекСерия)
	
	Результат = Новый Структура("Коэффициент,НужноОкруглятьКоличество");
	
	Если ЗначениеЗаполнено(ТекУпаковка) Тогда
#Удаление
		КлючКоэффициента = КлючКэшаУпаковки(ТекНоменклатура, ТекУпаковка); 
#КонецУдаления
#Вставка
		// 4D:Милавица, АндрейБ, 19.02.2019
		// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
		// {
		КлючКоэффициента = КлючКэшаУпаковки(ТекНоменклатура, ТекУпаковка, ТекСерия, Ложь);
#КонецВставки
		Кэш = КэшированныеЗначения.КоэффициентыУпаковок[КлючКоэффициента];
		Если Кэш = Неопределено Тогда
			#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
#Удаление
				ЗначенияРеквизитов = ОбработкаТабличнойЧастиСервер.ДанныеОбУпаковке(ТекНоменклатура, ТекУпаковка, КэшированныеЗначения);
#КонецУдаления
#Вставка
				// 4D:Милавица, АндрейБ, 19.02.2019
				// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
				// {
				ЗначенияРеквизитов = ОбработкаТабличнойЧастиСервер.ДанныеОбУпаковке(ТекНоменклатура,
														ТекУпаковка, КэшированныеЗначения, ТекСерия);
#КонецВставки
				Результат.Коэффициент    		   =  ЗначенияРеквизитов.Коэффициент;
				Результат.НужноОкруглятьКоличество =  ЗначенияРеквизитов.НужноОкруглятьКоличество;
			#Иначе
				ТекстИсключения = НСтр("ru = 'Попытка получения коэффициента упаковки на клиенте.';
										|en = 'Attempt to receive packaging factor on client.'");
				ВызватьИсключение ТекстИсключения;
			#КонецЕсли
		Иначе
			Результат = Кэш;
		КонецЕсли;
	Иначе
		Результат.Коэффициент = 1;
		Результат.НужноОкруглятьКоличество = Ложь;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

&ИзменениеИКонтроль("КлючКэшаУпаковки")
Функция МЛ_КлючКэшаУпаковки(Номенклатура, Упаковка, Серия, ВыполнениеНаКлиенте)

	Если ЗначениеЗаполнено(Номенклатура) Тогда
		КлючНоменклатура = Строка(Номенклатура.УникальныйИдентификатор());
	Иначе
		КлючНоменклатура = "ПустоеЗначение";
	КонецЕсли;

	Если ЗначениеЗаполнено(Упаковка) Тогда
		КлючУпаковка = Строка(Упаковка.УникальныйИдентификатор());
	Иначе
		КлючУпаковка = "ПустоеЗначение";
	КонецЕсли;
#Вставка
	// 4D:"Милавица", АндрейБ, 19.02.2019
	// Задача № 21179, Доработка объектов, связанная с изменением ед.хранения для номенклатуры
	// { 
	Если ЗначениеЗаполнено(Серия) Тогда
		Если ВыполнениеНаКлиенте <> Неопределено И НЕ ВыполнениеНаКлиенте Тогда
			Если Серия.Чд_ПризнакПромера Тогда

				КлючСерия = Строка(Серия.УникальныйИдентификатор());
				Возврат (КлючНоменклатура + КлючУпаковка + КлючСерия);
			Иначе
				Возврат КлючНоменклатура + КлючУпаковка;
			КонецЕсли;
		Иначе
			Возврат Новый УникальныйИдентификатор;
		КонецЕсли; 
	КонецЕсли;
#КонецВставки

	Возврат КлючНоменклатура + КлючУпаковка;

КонецФункции

&ИзменениеИКонтроль("ПересчитатьЦенуСкидкуПоСуммеВЗакупкахВСтрокеТЧ")
Процедура МЛ_ПересчитатьЦенуСкидкуПоСуммеВЗакупкахВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения)

	СтруктураПараметровДействия = Неопределено;

	Если СтруктураДействий.Свойство("ПересчитатьЦенуСкидкуПоСуммеВЗакупках", СтруктураПараметровДействия) Тогда

		// Если используются ручные скидки - перерасчитаем процент и сумму ручной скидки, иначе перерасчитываем цену.
		ИспользоватьРучныеСкидки = Ложь;
		Если СтруктураПараметровДействия <> Неопределено Тогда
			Если СтруктураПараметровДействия.Свойство("ИспользоватьРучныеСкидки") Тогда
				ИспользоватьРучныеСкидки = Истина;
			КонецЕсли;
		КонецЕсли;

		ИмяКоличества = "КоличествоУпаковок";
		Если СтруктураПараметровДействия <> Неопределено Тогда
			Если НЕ СтруктураПараметровДействия.Свойство("ИмяКоличества",ИмяКоличества) Тогда
				ИмяКоличества = "КоличествоУпаковок";
			КонецЕсли;
		КонецЕсли;

		#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
			Если ИспользоватьРучныеСкидки И КэшированныеЗначения.ИспользоватьРучныеСкидкиВЗакупках = Неопределено Тогда
				КэшированныеЗначения.ИспользоватьРучныеСкидкиВЗакупках = ПолучитьФункциональнуюОпцию("ИспользоватьРучныеСкидкиВЗакупках");
			КонецЕсли;
		#КонецЕсли

		// Если используются ручные скидки - перерасчитаем процент и сумму ручной скидки, иначе перерасчитываем цену.
		Если ИспользоватьРучныеСкидки И КэшированныеЗначения.ИспользоватьРучныеСкидкиВЗакупках Тогда

			Если ТекущаяСтрока.Сумма = 0 Или ТекущаяСтрока[ИмяКоличества] = 0 Тогда

				ТекущаяСтрока.Цена                = 0;
				ТекущаяСтрока.СуммаРучнойСкидки   = 0;

			Иначе

				Если ТекущаяСтрока.Цена = 0 Тогда
					Если ЗначениеЗаполнено(ТекущаяСтрока.ПроцентРучнойСкидки) Тогда
						ТекущаяСтрока.СуммаРучнойСкидки = (ТекущаяСтрока.Сумма * ТекущаяСтрока.ПроцентРучнойСкидки)  / (100 - ТекущаяСтрока.ПроцентРучнойСкидки);
					КонецЕсли;
					#Удаление
					ТекущаяСтрока.Цена = Окр((ТекущаяСтрока.Сумма + ТекущаяСтрока.СуммаРучнойСкидки) / ТекущаяСтрока[ИмяКоличества], 2);
					#КонецУдаления
					#Вставка
					// 4D:Милавица, ЕленаТ, 31.05.2021
					// Задача № 29429 , Доработка (срочно). Заказ поставщику, ПТиУ. Расчет системой поля Цена
					// "Цена - 4 знака после запятой"
					// {
					ТекущаяСтрока.Цена = Окр((ТекущаяСтрока.Сумма + ТекущаяСтрока.СуммаРучнойСкидки) / ТекущаяСтрока[ИмяКоличества], 4);
					#КонецВставки
				Иначе
					СуммаБезСкидки = ТекущаяСтрока.Цена * ТекущаяСтрока[ИмяКоличества];
					ТекущаяСтрока.СуммаРучнойСкидки   = СуммаБезСкидки - ТекущаяСтрока.Сумма;
					ТекущаяСтрока.ПроцентРучнойСкидки = Окр(100*ТекущаяСтрока.СуммаРучнойСкидки / СуммаБезСкидки, 2);
				КонецЕсли;

			КонецЕсли;

		Иначе

			Если ТекущаяСтрока.Сумма = 0 Или ТекущаяСтрока[ИмяКоличества] = 0 Тогда
				ТекущаяСтрока.Цена = 0;
			Иначе
				#Удаление
				ТекущаяСтрока.Цена = Окр(ТекущаяСтрока.Сумма / ТекущаяСтрока[ИмяКоличества], 2);
				#КонецУдаления
				#Вставка
				// 4D:Милавица, ЕленаТ, 19.02.2019
				// Задача № 29429 , Доработка (срочно). Заказ поставщику, ПТиУ. Расчет системой поля Цена
				// "Цена - 4 знака после запятой"
				// {
				ТекущаяСтрока.Цена = Окр(ТекущаяСтрока.Сумма / ТекущаяСтрока[ИмяКоличества], 4);
				#КонецВставки
			КонецЕсли;

		КонецЕсли;

	КонецЕсли;

КонецПроцедуры

&ИзменениеИКонтроль("НормализоватьПараметрыПересчетаЕдиниц")
Функция МЛ_НормализоватьПараметрыПересчетаЕдиниц(ТекущаяСтрока, УпаковкаНоменклатура)

	Если УпаковкаНоменклатура = Неопределено Тогда

		Номенклатура   = ТекущаяСтрока.Номенклатура;
		Упаковка       = ТекущаяСтрока.Упаковка;
		НужноОкруглять = Истина;

	ИначеЕсли ТипЗнч(УпаковкаНоменклатура) = Тип("Структура") Тогда

		Если УпаковкаНоменклатура.Свойство("Упаковка") Тогда
			Упаковка = УпаковкаНоменклатура.Упаковка;
		ИначеЕсли УпаковкаНоменклатура.Свойство("ЕдиницаИзмеренияВЕТИС") Тогда
			Если ЗначениеЗаполнено(УпаковкаНоменклатура.ЕдиницаИзмеренияВЕТИС) Тогда
				Упаковка = УпаковкаНоменклатура.ЕдиницаИзмеренияВЕТИС;
			Иначе
				Упаковка = ТекущаяСтрока.ЕдиницаИзмеренияВЕТИС;
			КонецЕсли;
		Иначе
			Упаковка = ТекущаяСтрока.Упаковка;
		КонецЕсли;

		Если УпаковкаНоменклатура.Свойство("Номенклатура") Тогда
			Номенклатура = УпаковкаНоменклатура.Номенклатура;
		Иначе
			Номенклатура = ТекущаяСтрока.Номенклатура;
		КонецЕсли;

		Если УпаковкаНоменклатура.Свойство("НужноОкруглять") Тогда
#Удаление
			Если ТекущаяСтрока.Свойство("НужноОкруглять") Тогда
#КонецУдаления
#Вставка
// 4D:Милавица, АндрейБ,  13.07.2021 
// Задача №28995 'Перевод конфигурации «1С:ERP Управление предприятием для Беларуси» с версии 2.4.6 на версию 2.4.11'
// { ошибка при обработке строки табличной части с сервера
			Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока, "НужноОкруглять") Тогда
#КонецВставки
				НужноОкруглять = ТекущаяСтрока.НужноОкруглять;	
			Иначе
				НужноОкруглять = УпаковкаНоменклатура.НужноОкруглять;
			КонецЕсли;		
		Иначе
			НужноОкруглять = Истина;
		КонецЕсли;

	Иначе
		Упаковка       = УпаковкаНоменклатура;
		Номенклатура   = ТекущаяСтрока.Номенклатура;
		НужноОкруглять = Истина;
	КонецЕсли;

	Возврат Новый Структура("Номенклатура,Упаковка,НужноОкруглять", Номенклатура, Упаковка, НужноОкруглять);

КонецФункции

&ИзменениеИКонтроль("ПересчитатьСуммуСУчетомАвтоматическойСкидкиВСтрокеТЧ")
Процедура МЛ_ПересчитатьСуммуСУчетомАвтоматическойСкидкиВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения)

	СтруктураПараметровДействия = Неопределено;

	Если СтруктураДействий.Свойство("ПересчитатьСуммуСУчетомАвтоматическойСкидки", СтруктураПараметровДействия) Тогда

		Очищать = Неопределено;
		СтруктураПараметровДействия.Свойство("Очищать", Очищать);

		Если Очищать Тогда

			ТекущаяСтрока.СуммаАвтоматическойСкидки = 0;
			ТекущаяСтрока.ПроцентАвтоматическойСкидки = 0;

			// {{ Локализация_УправлениеТорговлейДляБеларуси
			ТекущаяСтрока.ЦенаСоСкидкой = 0;
			// Локализация_УправлениеТорговлейДляБеларуси }}

		КонецЕсли;

		// {{ Локализация_УправлениеТорговлейДляБеларуси
		//Скидка = ТекущаяСтрока.СуммаАвтоматическойСкидки;
		#Вставка
		// Исправление ошибки типовой
		//ЮИБ Бушунов 13.02.2023+ 
		//Если ТекущаяСтрока.Свойство("ЦенаСоСкидкой") Тогда
		Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока,"ЦенаСоСкидкой") Тогда
		//ЮИБ Бушунов 13.02.2023-		
		#КонецВставки
		Если ТекущаяСтрока.ЦенаСоСкидкой <> 0 Тогда
			Скидка =
			#Удаление
			(ТекущаяСтрока.Цена - ТекущаяСтрока.ЦенаСоСкидкой) * ?(ТекущаяСтрока.Свойство("КоличествоУпаковок"), ТекущаяСтрока.КоличествоУпаковок, ТекущаяСтрока.Количество);
			#КонецУдаления
			#Вставка
			(ТекущаяСтрока.Цена - ТекущаяСтрока.ЦенаСоСкидкой) * ?(ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ТекущаяСтрока,"КоличествоУпаковок"), ТекущаяСтрока.КоличествоУпаковок, ТекущаяСтрока.Количество);
			#КонецВставки
		Иначе	
			Скидка = ТекущаяСтрока.СуммаАвтоматическойСкидки;
		КонецЕсли; 
		#Вставка	
		Иначе
			Скидка = ТекущаяСтрока.СуммаАвтоматическойСкидки;	
		КонецЕсли;
		#КонецВставки
		// Локализация_УправлениеТорговлейДляБеларуси }}

		ТекущаяСтрока.Сумма = ТекущаяСтрока.Сумма - Скидка;

	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#КонецОбласти

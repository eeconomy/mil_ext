﻿
#Область ПрограммныйИнтерфейс

&ИзменениеИКонтроль("ОчиститьЗаданияПередУдалениемДокумента")
Процедура МЛ_ОчиститьЗаданияПередУдалениемДокумента(Источник, Отказ)
	
	Если Источник.ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если ПланыОбмена.ГлавныйУзел() <> Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	Задания.ИмяРегистра               КАК ИмяРегистра,
	|	Задания.Месяц                     КАК Месяц,
	|	Задания.НомерЗадания              КАК НомерЗадания,
	|	Задания.Документ                  КАК Документ,
	|	Задания.Операция                  КАК Операция,
	|	Задания.Организация               КАК Организация,
	|	Задания.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|	Задания.ОбъектРасчетов            КАК ОбъектРасчетов,
	|	Задания.НомерПакета               КАК НомерПакета,
	|	Задания.ОбъектУчета               КАК ОбъектУчета,
	|	Задания.ОсновноеСредство          КАК ОсновноеСредство,
	|	Задания.Пропускать                КАК ПропускатьПриЗаписи
	|ИЗ
	|	(ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКРасчетуСебестоимости"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц        КАК Месяц,
	|		Задания.НомерЗадания КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО         КАК Документ,
	|		НЕОПРЕДЕЛЕНО         КАК Операция,
	|		Задания.Организация  КАК Организация,
	|		НЕОПРЕДЕЛЕНО         КАК АналитикаУчетаПоПартнерам,
	|		НЕОПРЕДЕЛЕНО         КАК ОбъектРасчетов,
	|		НЕОПРЕДЕЛЕНО         КАК НомерПакета,
	|		НЕОПРЕДЕЛЕНО         КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО         КАК ОсновноеСредство,
	|		ВЫБОР КОГДА НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ КАК            Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКРасчетуСебестоимости КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКРасчетуСебестоимости КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И Задания.Организация = Дубли.Организация
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|	ГДЕ
	|		Задания.Документ = &Ссылка
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКЗакрытиюМесяца"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц        КАК Месяц,
	|		Задания.НомерЗадания КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО         КАК Документ,
	|		Задания.Операция     КАК Операция,
	|		Задания.Организация  КАК Организация,
	|		НЕОПРЕДЕЛЕНО         КАК АналитикаУчетаПоПартнерам,
	|		НЕОПРЕДЕЛЕНО         КАК ОбъектРасчетов,
	|		НЕОПРЕДЕЛЕНО         КАК НомерПакета,
	|		НЕОПРЕДЕЛЕНО         КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО         КАК ОсновноеСредство,
	|		ВЫБОР КОГДА НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ КАК            Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКЗакрытиюМесяца КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКЗакрытиюМесяца КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.Операция = Дубли.Операция
	|			И Задания.Организация = Дубли.Организация
	|	ГДЕ
	|		Задания.Документ = &Ссылка
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКРаспределениюРасчетовСКлиентами"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц                     КАК Месяц,
	|		Задания.НомерЗадания              КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО                      КАК Документ,
	|		НЕОПРЕДЕЛЕНО                      КАК Операция,
	|		Задания.Организация              КАК Организация,
	|		Задания.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|		Задания.ОбъектРасчетов            КАК ОбъектРасчетов,
	|		НЕОПРЕДЕЛЕНО                      КАК НомерПакета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОсновноеСредство,
	|		ВЫБОР КОГДА Задания.ОбъектРасчетов = &Ссылка ИЛИ НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ                             КАК Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКРаспределениюРасчетовСКлиентами КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКРаспределениюРасчетовСКлиентами КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.АналитикаУчетаПоПартнерам = Дубли.АналитикаУчетаПоПартнерам
	|			И Задания.Организация = Дубли.Организация
	|			И Задания.ОбъектРасчетов = Дубли.ОбъектРасчетов
	|	ГДЕ
	|		Задания.Документ = &Ссылка
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКРаспределениюРасчетовСПоставщиками"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц                     КАК Месяц,
	|		Задания.НомерЗадания              КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО                      КАК Документ,
	|		НЕОПРЕДЕЛЕНО                      КАК Операция,
	|		Задания.Организация               КАК Организация,
	|		Задания.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|		Задания.ОбъектРасчетов            КАК ОбъектРасчетов,
	|		НЕОПРЕДЕЛЕНО                      КАК НомерПакета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОсновноеСредство,
	|		ВЫБОР КОГДА Задания.ОбъектРасчетов = &Ссылка ИЛИ НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ                             КАК Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКРаспределениюРасчетовСПоставщиками КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКРаспределениюРасчетовСПоставщиками КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.АналитикаУчетаПоПартнерам = Дубли.АналитикаУчетаПоПартнерам
	|			И Задания.Организация = Дубли.Организация
	|			И Задания.ОбъектРасчетов = Дубли.ОбъектРасчетов
	|	ГДЕ
	|		Задания.Документ = &Ссылка
	//++ НЕ УТ
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКРасчетуАмортизацииОС"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц                     КАК Месяц,
	|		Задания.НомерЗадания              КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО                      КАК Документ,
	|		НЕОПРЕДЕЛЕНО                      КАК Операция,
	|		Задания.Организация               КАК Организация,
	|		НЕОПРЕДЕЛЕНО                      КАК АналитикаУчетаПоПартнерам,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектРасчетов,
	|		Задания.НомерПакета               КАК НомерПакета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОсновноеСредство,
	|		ВЫБОР КОГДА НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ                             КАК Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКРасчетуАмортизацииОС КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКРасчетуАмортизацииОС КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.НомерПакета = Дубли.НомерПакета
	|			И Задания.Организация = Дубли.Организация
	|	ГДЕ
	|		Задания.Документ = &Ссылка
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКРасчетуАмортизацииНМА"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц                     КАК Месяц,
	|		Задания.НомерЗадания              КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО                      КАК Документ,
	|		НЕОПРЕДЕЛЕНО                      КАК Операция,
	|		Задания.Организация               КАК Организация,
	|		НЕОПРЕДЕЛЕНО                      КАК АналитикаУчетаПоПартнерам,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектРасчетов,
	|		Задания.НомерПакета               КАК НомерПакета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОсновноеСредство,
	|		ВЫБОР КОГДА НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ                             КАК Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКРасчетуАмортизацииНМА КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКРасчетуАмортизацииНМА КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.НомерПакета = Дубли.НомерПакета
	|			И Задания.Организация = Дубли.Организация
	|	ГДЕ
	|		Задания.Документ = &Ссылка
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКРасчетуСтоимостиВНА"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц                     КАК Месяц,
	|		Задания.НомерЗадания              КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО                      КАК Документ,
	|		НЕОПРЕДЕЛЕНО                      КАК Операция,
	|		Задания.Организация               КАК Организация,
	|		НЕОПРЕДЕЛЕНО                      КАК АналитикаУчетаПоПартнерам,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектРасчетов,
	|		НЕОПРЕДЕЛЕНО                      КАК НомерПакета,
	|		Задания.ОбъектУчета               КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОсновноеСредство,
	|		ВЫБОР КОГДА НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ                             КАК Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКРасчетуСтоимостиВНА КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКРасчетуСтоимостиВНА КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.ОбъектУчета = Дубли.ОбъектУчета
	|			И Задания.Организация = Дубли.Организация
	|	ГДЕ
	|		Задания.Документ = &Ссылка
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКФормированиюДвиженийПоВНА"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц                     КАК Месяц,
	|		Задания.НомерЗадания              КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО                      КАК Документ,
	|		НЕОПРЕДЕЛЕНО                      КАК Операция,
	|		Задания.Организация               КАК Организация,
	|		НЕОПРЕДЕЛЕНО                      КАК АналитикаУчетаПоПартнерам,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектРасчетов,
	|		НЕОПРЕДЕЛЕНО                      КАК НомерПакета,
	|		Задания.ОбъектУчета               КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОсновноеСредство,
	|		ВЫБОР КОГДА НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ                             КАК Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКФормированиюДвиженийПоВНА КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКФормированиюДвиженийПоВНА КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.ОбъектУчета = Дубли.ОбъектУчета
	|			И Задания.Организация = Дубли.Организация
	|	ГДЕ
	|		Задания.Документ = &Ссылка
#Вставка
	// 4D:Милавица, Михаил, 09.04.2019
	// Погашение стоимости ТМЦ в эксплуатации, №21565
	// {
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		""ЗаданияКПогашениюСтоимостиТМЦВЭксплуатации"" КАК ИмяРегистра,
	|		
	|		Задания.Месяц                     КАК Месяц,
	|		Задания.НомерЗадания              КАК НомерЗадания,
	|		НЕОПРЕДЕЛЕНО                      КАК Документ,
	|		НЕОПРЕДЕЛЕНО                      КАК Операция,
	|		Задания.Организация               КАК Организация,
	|		НЕОПРЕДЕЛЕНО                      КАК АналитикаУчетаПоПартнерам,
	|		НЕОПРЕДЕЛЕНО                      КАК ОбъектРасчетов,
	|		Задания.НомерПакета               КАК НомерПакета,
	|		НЕОПРЕДЕЛЕНО		              КАК ОбъектУчета,
	|		НЕОПРЕДЕЛЕНО                      КАК ОсновноеСредство,	
	|		ВЫБОР КОГДА НЕ Дубли.Документ ЕСТЬ NULL
	|			ТОГДА ИСТИНА
	|			ИНАЧЕ ЛОЖЬ
	|		КОНЕЦ                             КАК Пропускать
	|	ИЗ
	|		РегистрСведений.ЗаданияКПогашениюСтоимостиТМЦВЭксплуатации КАК Задания
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЗаданияКПогашениюСтоимостиТМЦВЭксплуатации КАК Дубли
	|		ПО Задания.Месяц = Дубли.Месяц
	|			И Задания.НомерЗадания = Дубли.НомерЗадания
	|			И НЕОПРЕДЕЛЕНО = Дубли.Документ
	|			И Задания.НомерПакета = Дубли.НомерПакета
	|			И Задания.Организация = Дубли.Организация
	|	ГДЕ
	|		Задания.Документ = &Ссылка
#КонецВставки
	//-- НЕ УТ
	|	) КАК Задания
	|ИТОГИ ПО
	|	Задания.ИмяРегистра
	|");
	
	Запрос.УстановитьПараметр("Ссылка", Источник.Ссылка);
	ВыборкаЗаданий = Запрос.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	Пока ВыборкаЗаданий.Следующий() Цикл
		ЗаданияКОчистке = РегистрыСведений[ВыборкаЗаданий.ИмяРегистра].СоздатьНаборЗаписей();
		ЗаданияКОчистке.Отбор.Документ.Установить(Источник.Ссылка);
		ОбновлениеИнформационнойБазы.ЗаписатьНаборЗаписей(ЗаданияКОчистке, Истина);
		
		ЗаданияКЗаписи = РегистрыСведений[ВыборкаЗаданий.ИмяРегистра].СоздатьНаборЗаписей();
		ВыборкаЗаписей = ВыборкаЗаданий.Выбрать();
		Пока ВыборкаЗаписей.Следующий() Цикл 
			Если Не ВыборкаЗаписей.ПропускатьПриЗаписи Тогда
				КЗаписи = ЗаданияКЗаписи.Добавить();
				ЗаполнитьЗначенияСвойств(КЗаписи, ВыборкаЗаписей);
			КонецЕсли;
		КонецЦикла;
		Попытка
			Если ЗаданияКЗаписи.Количество() <> 0 Тогда
				ОбновлениеИнформационнойБазы.ЗаписатьНаборЗаписей(ЗаданияКЗаписи, Ложь);
			КонецЕсли;
		Исключение
			Отказ = Истина;
			ЗаписьЖурналаРегистрации(
				НСтр("ru = 'Удаление помеченных объектов';
					|en = 'Deletion of marked objects'", ОбщегоНазначенияКлиентСервер.КодОсновногоЯзыка()),
				УровеньЖурналаРегистрации.Ошибка,
				Метаданные.ОбщиеМодули.ЗакрытиеМесяцаСервер,
				,
				ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
		КонецПопытки;
	КонецЦикла;
	
КонецПроцедуры

&ИзменениеИКонтроль("Использование_ФормированиеФинансовогоРезультата")
Процедура МЛ_Использование_ФормированиеФинансовогоРезультата(ПараметрыОбработчика)

	ПроверитьИспользованиеРегламентированногоУчета(ПараметрыОбработчика);

	Если РасчетЭтапаНеТребуется(ПараметрыОбработчика.ДанныеЭтапа) Тогда
		Возврат;
	КонецЕсли;

	Запрос = Новый Запрос;
	ИнициализироватьЗапрос(Запрос, ПараметрыОбработчика);

	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Таб.Организация,
	|	Таб.ОбособленноеПодразделение,
	|	Таб.ГоловнаяОрганизация
	|ПОМЕСТИТЬ ВТОрганизации
	|ИЗ (
	// Получим организации без обособленных подразделений и обособленные подразделения
	|	ВЫБРАТЬ
	|		ЕСТЬNULL(ОбособленныеПодразделения.Ссылка, ДД.Ссылка) КАК Организация,
	|		ЕСТЬNULL(ОбособленныеПодразделения.ОбособленноеПодразделение, ЛОЖЬ) КАК ОбособленноеПодразделение,
	|		ЕСТЬNULL(ОбособленныеПодразделения.ГоловнаяОрганизация, ДД.Ссылка) КАК ГоловнаяОрганизация
	|	ИЗ
	|		Справочник.Организации КАК ДД
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Организации КАК ОбособленныеПодразделения
	|		ПО ДД.Ссылка = ОбособленныеПодразделения.ГоловнаяОрганизация
	|			И ОбособленныеПодразделения.ОбособленноеПодразделение
	|	ГДЕ
	|		НЕ ДД.ОбособленноеПодразделение
	|		И (ДД.Ссылка В (&МассивОрганизаций)
	|			ИЛИ ЕСТЬNULL(ОбособленныеПодразделения.Ссылка, ДД.Ссылка) В (&МассивОрганизаций)
	|		) И ДД.Ссылка <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	// Добавим ссылки на сами головные организации
	|	ВЫБРАТЬ
	|		ДД.Ссылка КАК Организация,
	|		ЛОЖЬ КАК ОбособленноеПодразделение,
	|		ДД.Ссылка КАК ГоловнаяОрганизация
	|	ИЗ
	|		Справочник.Организации КАК ДД
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Организации КАК ОбособленныеПодразделения
	|		ПО ДД.Ссылка = ОбособленныеПодразделения.ГоловнаяОрганизация
	|	ГДЕ
	|		НЕ ДД.ОбособленноеПодразделение
	|		И ОбособленныеПодразделения.ОбособленноеПодразделение
	|		И ДД.Ссылка В (&МассивОрганизаций)
	|		И ДД.Ссылка <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|	) КАК Таб
	|
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ 
	|	Т.Организация
	|ИЗ
	|	ВТОрганизации КАК Т
	|ГДЕ
	|	НЕ Т.Организация В (&МассивОрганизаций)
	|	ИЛИ НЕ Т.ОбособленноеПодразделение";

	РезультатЗапроса = Запрос.Выполнить();

	УвеличитьКоличествоОбработанныхДанныхДляЗамера(ПараметрыОбработчика, РезультатЗапроса.Выгрузить().Количество());

	Запрос.УстановитьПараметр("ПроверятьОстатки90хСчетов", НЕ РезультатЗапроса.Пустой());

	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Таб.Организация КАК Ссылка
	|ПОМЕСТИТЬ ОрганизацииСОборотами
	|ИЗ (
	|	ВЫБРАТЬ
	|		ХозрасчетныйОбороты.Организация КАК Организация
	|	ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Обороты(&НачалоПериода,
	|		 &КонецПериода,
	|		 ,
	|		 ,
	|		 ,
	|		&ПроверятьОстатки90хСчетов И НЕ &ЭтоКонецГода
	|			И Организация В (ВЫБРАТЬ РАЗЛИЧНЫЕ Т.Организация ИЗ ВТОрганизации КАК Т ГДЕ НЕ Т.ОбособленноеПодразделение)
	|		 ) КАК ХозрасчетныйОбороты
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ
	|		ХозрасчетныйОбороты.Организация КАК Организация
	|	ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Обороты(НАЧАЛОПЕРИОДА(&НачалоПериода, ГОД),
	|		 КОНЕЦПЕРИОДА(&КонецПериода, ГОД),
	|		 ,
	|		 ,
	|		 ,
	|		&ЭтоКонецГода
	|			И Организация В (ВЫБРАТЬ РАЗЛИЧНЫЕ Т.Организация ИЗ ВТОрганизации КАК Т)
	|		 ) КАК ХозрасчетныйОбороты
	|	) КАК Таб
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Организации.Организация КАК Ссылка
	|ПОМЕСТИТЬ НеОбособленныеПодразделенияСОборотами
	|ИЗ
	|	ВТОрганизации КАК Организации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ОрганизацииСОборотами КАК ОрганизацииСОборотами
	|		ПО Организации.Организация = ОрганизацииСОборотами.Ссылка
	|ГДЕ
	|	НЕ Организации.ОбособленноеПодразделение
	|
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Организации.Ссылка,
	|	УчетныеПолитики.УчетнаяПолитика.ВариантУчетаОтложенногоНалога КАК ВариантУчетаОтложенногоНалога
	|ПОМЕСТИТЬ ОрганизацииОСНОСОборотами
	|ИЗ
	|	ОрганизацииСОборотами КАК Организации
	|		ЛЕВОЕ СОЕДИНЕНИЕ
	|			РегистрСведений.УчетнаяПолитикаОрганизаций.СрезПоследних(
	|				&КонецПериода, 
	|				Организация В (ВЫБРАТЬ РАЗЛИЧНЫЕ Т.Организация ИЗ ВТОрганизации КАК Т)) КАК УчетныеПолитики
	|	ПО
	|		Организации.Ссылка = УчетныеПолитики.Организация
	|ГДЕ
	|	УчетныеПолитики.УчетнаяПолитика.СистемаНалогообложения ЕСТЬ NULL
	|	ИЛИ УчетныеПолитики.УчетнаяПолитика.СистемаНалогообложения = ЗНАЧЕНИЕ(Перечисление.СистемыНалогообложения.Общая)
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ
	|	Проводки.Организация КАК Организация,
	|	Проводки.СуммаОстаток КАК Сумма90,
	|	0 КАК Сумма91
	|ПОМЕСТИТЬ ВТОстаткиПоСчетам90и91
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаКонецПериода,
	|			Счет В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.Продажи)),
	|			,
	|			&ПроверятьОстатки90хСчетов И Организация В (ВЫБРАТЬ Т.Организация ИЗ ВТОрганизации КАК Т)) КАК Проводки
	|ГДЕ
	|	(Проводки.СуммаОстаток <> 0
	|			ИЛИ Проводки.СуммаНУОстаток <> 0
	|			ИЛИ Проводки.СуммаПРОстаток <> 0
	|			ИЛИ Проводки.СуммаВРОстаток <> 0)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	Проводки.Организация,
	|	0,
	|	Проводки.СуммаОстаток
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаКонецПериода,
	|			Счет В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПрочиеДоходыИРасходы)),
	|			,
	|			&ПроверятьОстатки90хСчетов И Организация В (ВЫБРАТЬ Т.Организация ИЗ ВТОрганизации КАК Т)) КАК Проводки
	|ГДЕ
	|	(Проводки.СуммаОстаток <> 0
	|			ИЛИ Проводки.СуммаНУОстаток <> 0
	|			ИЛИ Проводки.СуммаПРОстаток <> 0
	|			ИЛИ Проводки.СуммаВРОстаток <> 0)
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ
	|	Организации.ГоловнаяОрганизация КАК Ссылка,
	|	СУММА(ВТОстаткиПоСчетам90и91.Сумма90) КАК Сумма90,
	|	СУММА(ВТОстаткиПоСчетам90и91.Сумма91) КАК Сумма91
	|ПОМЕСТИТЬ ОрганизацииСОстатками90еСчета
	|ИЗ
	|	ВТОстаткиПоСчетам90и91 КАК ВТОстаткиПоСчетам90и91
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТОрганизации КАК Организации
	|		ПО ВТОстаткиПоСчетам90и91.Организация = Организации.Организация
	|
	|СГРУППИРОВАТЬ ПО
	|	Организации.ГоловнаяОрганизация
	|
	|ИМЕЮЩИЕ
	|	(СУММА(ВТОстаткиПоСчетам90и91.Сумма90) <> 0
	|		ИЛИ СУММА(ВТОстаткиПоСчетам90и91.Сумма91) <> 0)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Т.Организация КАК Ссылка
	|ПОМЕСТИТЬ ОрганизацииСОстаткамиПоКурсовымРазницам
	|ИЗ (
	|	ВЫБРАТЬ
	|		Остатки.Счет,
	|		Остатки.Организация,
	|		Остатки.Валюта,
	|		Остатки.Подразделение,
	|		Остатки.НаправлениеДеятельности,
	|		ЕСТЬNULL(Остатки.Субконто1, НЕОПРЕДЕЛЕНО) КАК Субконто1,
	|		ЕСТЬNULL(Остатки.Субконто2, НЕОПРЕДЕЛЕНО) КАК Субконто2,
	|		ЕСТЬNULL(Остатки.Субконто3, НЕОПРЕДЕЛЕНО) КАК Субконто3
	|	ИЗ
	|		РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаКонецПериода,
	|			НЕ Счет В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПрибылиИУбытки))
	|			И НЕ Счет.Забалансовый,
	|			,
	|			Организация В (ВЫБРАТЬ Т.Организация ИЗ ВТОрганизации КАК Т)) КАК Остатки
	|	ГДЕ
	|		&УчетВВалютеФО И Остатки.СуммаФООстаток <> (ВЫРАЗИТЬ(&КоэффициентПересчета
	|			* ВЫБОР КОГДА &СуммаУУ ТОГДА Остатки.СуммаУУОстаток ИНАЧЕ Остатки.СуммаОстаток КОНЕЦ КАК ЧИСЛО(15, 2)))
	|	
	|	СГРУППИРОВАТЬ ПО
	|		Остатки.Организация,
	|		Остатки.Счет,
	|		Остатки.Валюта,
	|		Остатки.Подразделение,
	|		Остатки.НаправлениеДеятельности,
	|		ЕСТЬNULL(Остатки.Субконто1, НЕОПРЕДЕЛЕНО),
	|		ЕСТЬNULL(Остатки.Субконто2, НЕОПРЕДЕЛЕНО),
	|		ЕСТЬNULL(Остатки.Субконто3, НЕОПРЕДЕЛЕНО)
	|	
	|	ИМЕЮЩИЕ
	|		СУММА(Остатки.СуммаФООстаток) <> СУММА(ВЫРАЗИТЬ(&КоэффициентПересчета * ВЫБОР
	|			КОГДА &СуммаУУ ТОГДА Остатки.СуммаУУОстаток ИНАЧЕ Остатки.СуммаОстаток КОНЕЦ КАК ЧИСЛО(15, 2)))
	|	) КАК Т
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Т.Организация КАК Ссылка
	|ПОМЕСТИТЬ ОрганизацииОСНОСОборотамиУбыткиПрошлыхЛет
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			НАЧАЛОПЕРИОДА(&КонецПериода, ГОД),
	|			Счет В ИЕРАРХИИ (ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.УбыткиПрошлыхЛет)),
	|			,
	|			Организация В
	|				(ВЫБРАТЬ
	|					Т.Ссылка
	|				ИЗ
	|					ОрганизацииОСНОСОборотами КАК Т)) КАК Т
	|ГДЕ
	|	Т.СуммаНУОстаток <> 0
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Т.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ ОрганизацииОСНОСОборотамиБалансовыйМетод
	|ИЗ
	|	ОрганизацииОСНОСОборотами КАК Т
	|ГДЕ
	|	Т.ВариантУчетаОтложенногоНалога В
	|			(ЗНАЧЕНИЕ(Перечисление.ВариантыУчетаОтложенногоНалога.БалансовыйМетод),
	|			ЗНАЧЕНИЕ(Перечисление.ВариантыУчетаОтложенногоНалога.БалансовыйМетодПостоянныеРазницы))
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	| ВЫБРАТЬ
	|	T.Организация КАК Организация
	|ПОМЕСТИТЬ НеПеренесенУбыток
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			НАЧАЛОПЕРИОДА(&НачалоПериода, ГОД),
	|			Счет = ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ОтложенныеНалоговыеАктивы),
	|			,
	|			Организация В (ВЫБРАТЬ Т.Ссылка ИЗ ОрганизацииОСНОСОборотами КАК Т)
	|				И (ВЫРАЗИТЬ(Субконто1 КАК Перечисление.ВидыАктивовИОбязательств)) = ЗНАЧЕНИЕ(Перечисление.ВидыАктивовИОбязательств.УбытокТекущегоПериода)) КАК T
	|;
	|////////////////////////////////////////////////////////////////////////////////
	| ВЫБРАТЬ
	|	T.Организация КАК Организация
	|ПОМЕСТИТЬ НеПеренесенУбытокКонецГода
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаКонецПериода,
	|			Счет = ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ОтложенныеНалоговыеАктивы),
	|			,
	|			&ЭтоКонецГода И Организация В (ВЫБРАТЬ Т.Ссылка ИЗ ОрганизацииОСНОСОборотами КАК Т)
	|				И (ВЫРАЗИТЬ(Субконто1 КАК Перечисление.ВидыАктивовИОбязательств)) = ЗНАЧЕНИЕ(Перечисление.ВидыАктивовИОбязательств.УбытокТекущегоПериода)) КАК T
	|;
	|////////////////////////////////////////////////////////////////////////////////
	| ВЫБРАТЬ
	|	T.Организация КАК Организация
	|ПОМЕСТИТЬ УбыткиСрок10Лет
	|ИЗ
	|	РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаКонецПериода,
	|			Счет = ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.УбыткиПрошлыхЛет),
	|			,
	|			Организация В (ВЫБРАТЬ Т.Ссылка ИЗ ОрганизацииОСНОСОборотами КАК Т)
	|				И ВЫРАЗИТЬ(Субконто1 КАК Справочник.УбыткиПрошлыхЛет).ДатаОкончанияСписания <= НАЧАЛОПЕРИОДА(&КонецПериода,ДЕНЬ)) КАК T
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Т.Ссылка КАК Организация
	|ИЗ
	|	ОрганизацииСОстатками90еСчета КАК Т
	|УПОРЯДОЧИТЬ ПО
	|	Организация
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Т.Ссылка КАК Организация
	|ИЗ
	|	ОрганизацииСОстаткамиПоКурсовымРазницам КАК Т
	|УПОРЯДОЧИТЬ ПО
	|	Организация
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ЗаданияФормированиеФинансовогоРезультата.Организация КАК Организация
	|ИЗ
	|	РегистрСведений.ЗаданияКЗакрытиюМесяца КАК ЗаданияФормированиеФинансовогоРезультата
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ ОрганизацииОСНОСОборотами КАК Т
	|		ПО ЗаданияФормированиеФинансовогоРезультата.Организация = Т.Ссылка
	|ГДЕ
	|	ЗаданияФормированиеФинансовогоРезультата.Операция = ЗНАЧЕНИЕ(Перечисление.ОперацииЗакрытияМесяца.ФормированиеФинансовогоРезультата)
	|		И НАЧАЛОПЕРИОДА(ЗаданияФормированиеФинансовогоРезультата.Месяц, МЕСЯЦ) <= &НачалоПериода
	|УПОРЯДОЧИТЬ ПО
	|	Организация";

	УвеличитьКоличествоОбработанныхДанныхДляЗамера(ПараметрыОбработчика, 1);

	УчетВВалютеФО = ПолучитьФункциональнуюОпцию("ВестиУчетНаПланеСчетовХозрасчетныйВВалютеФинОтчетности");
	Запрос.УстановитьПараметр("УчетВВалютеФО", УчетВВалютеФО);

	УвеличитьКоличествоОбработанныхДанныхДляЗамера(ПараметрыОбработчика, 1);

	Если Константы.ИсточникСуммыДляПересчетаВВалютуФинОтчетности.Получить() = Перечисления.ИсточникиСуммыДляПересчетаВВалютуФинОтчетности.УУ Тогда
		ТекущаяВалюта = Константы.ВалютаУправленческогоУчета.Получить();
		Запрос.УстановитьПараметр("СуммаУУ", Истина);
	Иначе
		ТекущаяВалюта = Константы.ВалютаРегламентированногоУчета.Получить();
		Запрос.УстановитьПараметр("СуммаУУ", Ложь);
	КонецЕсли;

	УвеличитьКоличествоОбработанныхДанныхДляЗамера(ПараметрыОбработчика, 1);

	КоэффициентПересчетаВалютыФО = РаботаСКурсамиВалютУТ.ПолучитьКоэффициентПересчетаИзВалютыВВалюту(
	ТекущаяВалюта,
	Константы.ВалютаФинОтчетности.Получить(),
	КонецМесяца(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации));
	Запрос.УстановитьПараметр("КоэффициентПересчета", КоэффициентПересчетаВалютыФО);

	ЭтоКонецГода = (Месяц(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации) = 12);
	Запрос.УстановитьПараметр("ЭтоКонецГода", ЭтоКонецГода);

	РезультатЗапроса = Запрос.ВыполнитьПакет();
	ПредставлениеПериода = РасчетСебестоимостиПротоколРасчета.ПредставлениеПериодаРасчета(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации);

	РазмерыВременныхТаблиц = РазмерыВременныхТаблиц(Запрос, ПараметрыОбработчика);

	Если РазмерыВременныхТаблиц.ОрганизацииСОборотами = 0
		И РазмерыВременныхТаблиц.ОрганизацииСОстатками90еСчета = 0
		И РазмерыВременныхТаблиц.ОрганизацииСОстаткамиПоКурсовымРазницам = 0 Тогда

		УстановитьСостояниеНеТребуется(
		ПараметрыОбработчика,
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Нет организаций с оборотами по регистру бухгалтерского учета за период %1';
		|en = 'No companies with turnovers for the bookkeeping register for period %1'"),
		ПредставлениеПериода));
		Возврат;

	КонецЕсли;

	ВведеныРегламентныеДокументы = ПроверитьНаличиеДокументаРегламентнаяОперация(
	ПараметрыОбработчика,
	Перечисления.ТипыРегламентныхОпераций.ФормированиеФинансовогоРезультата,
	"НеОбособленныеПодразделенияСОборотами",
	Ложь);

	ВведеныРегламентныеДокументы = ПроверитьНаличиеДокументаРегламентнаяОперация(
	ПараметрыОбработчика,
	Перечисления.ТипыРегламентныхОпераций.СписаниеУбытковПрошлыхЛет,
	"ОрганизацииОСНОСОборотамиУбыткиПрошлыхЛет",
	Ложь);

#Вставка
	// 4D:Милавица, АндрейБ,  26.07.2021 
	// Задача №28995 'Перевод конфигурации «1С:ERP Управление предприятием для Беларуси» с версии 2.4.6 на версию 2.4.11'
	// { при закрытии месяца и отсутствии необходимости расчета налога на прибыль
	// "Формирование финансового результата" определяется как ошибочное
#КонецВставки
#Удаление
	ВведеныРегламентныеДокументы = ПроверитьНаличиеДокументаРегламентнаяОперация(
	ПараметрыОбработчика,
	Перечисления.ТипыРегламентныхОпераций.РасчетНалогаНаПрибыль,
	"ОрганизацииОСНОСОборотами",
	Ложь);
#КонецУдаления

	ВведеныРегламентныеДокументы = ПроверитьНаличиеДокументаРегламентнаяОперация(
	ПараметрыОбработчика,
	Перечисления.ТипыРегламентныхОпераций.РасчетОтложенногоНалога,
	"ОрганизацииОСНОСОборотамиБалансовыйМетод",
	Ложь);

	Если УчетВВалютеФО Тогда
		ВведеныРегламентныеДокументы = ПроверитьНаличиеДокументаРегламентнаяОперация(
		ПараметрыОбработчика,
		Перечисления.ТипыРегламентныхОпераций.ПереоценкаСуммыВВалютеФинОтчетности,
		"ОрганизацииСОстаткамиПоКурсовымРазницам",
		Ложь);
	КонецЕсли;

	Если ЭтоКонецГода Тогда
		ВведеныРегламентныеДокументы = ПроверитьНаличиеДокументаРегламентнаяОперация(
		ПараметрыОбработчика,
		Перечисления.ТипыРегламентныхОпераций.ЗакрытиеГода,
		"ОрганизацииСОборотами",
		Ложь);
	КонецЕсли;

	Выборка = РезультатЗапроса.Получить(11).Выбрать();

	Пока Выборка.Следующий() Цикл

		УстановитьСостояниеНеВыполнен(
		ПараметрыОбработчика,
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'В организации ""%1"" на конец периода %2 есть суммовые остатки по 90м счетам.';
		|en = 'There is amount balance of 90m accounts in the ""%1"" company as of the end of the period %2.'"),
		Выборка.Организация,
		ПредставлениеПериода));

	КонецЦикла;

	Выборка = РезультатЗапроса.Получить(12).Выбрать();

	Пока Выборка.Следующий() Цикл

		УстановитьСостояниеНеВыполнен(
		ПараметрыОбработчика,
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'В организации ""%1"" на конец периода %2 есть суммовые остатки по курсовым разницам.';
		|en = 'There is amount balance of exchange rate differences for the %1 company as of the end of the period %2.'"),
		Выборка.Организация,
		ПредставлениеПериода));

	КонецЦикла;

	Выборка = РезультатЗапроса.Получить(13).Выбрать();

	Пока Выборка.Следующий() Цикл

		УстановитьСостояниеНеВыполнен(
		ПараметрыОбработчика,
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'В организации ""%1"" за период %2 необходимо выполнить регламентную операцию формирование финансового результата.';
		|en = 'Perform the period-end operation of financial result generation for period %2 in the ""%1"" company.'"),
		Выборка.Организация,
		ПредставлениеПериода));


	КонецЦикла;

	ТаблицаНеПеренесенУбыток = Запрос.МенеджерВременныхТаблиц.Таблицы["НеПеренесенУбыток"].ПолучитьДанные().Выгрузить();

	Для Каждого СтрокаОрганизации Из ТаблицаНеПеренесенУбыток Цикл

		НачалоНалоговогоПериода = НалоговыйУчет.НачалоНалоговогоПериода(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации, СтрокаОрганизации.Организация);
		Если НачалоГода(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации) <> НачалоНалоговогоПериода Тогда
			Продолжить;
		КонецЕсли;

		УстановитьСостояниеНеВыполнен(
		ПараметрыОбработчика,
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Не выполнен перенос убытка прошлого года по НУ в организации ""%1"". Выполните операцию ""Закрытие года"" в %2 или перенесите убыток вручную.';
		|en = 'Loss of the previous year by TA is not transferred in the ""%1"" company. Execute the ""Year-end closing"" operation in %2 or transfer the loss manually.'"),
		СтрокаОрганизации.Организация, "декабре " + Формат(Год(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации)-1,"ЧГ=0")));
	КонецЦикла;

	ТаблицаНеПеренесенУбытокКонецГода = Запрос.МенеджерВременныхТаблиц.Таблицы["НеПеренесенУбытокКонецГода"].ПолучитьДанные().Выгрузить();

	Для Каждого СтрокаОрганизации Из ТаблицаНеПеренесенУбытокКонецГода Цикл

		НачалоНалоговогоПериода = НалоговыйУчет.НачалоНалоговогоПериода(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации, СтрокаОрганизации.Организация);
		Если НачалоГода(ПараметрыОбработчика.ПараметрыРасчета.ПериодРегистрации) <> НачалоНалоговогоПериода Тогда
			Продолжить;
		КонецЕсли;

		УстановитьСостояниеНеВыполнен(
		ПараметрыОбработчика,
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Не выполнен перенос убытка текущего года по НУ на будущее в организации ""%1"". Необходимо выполнить регламентную операцию ""Закрытие года"".';
		|en = 'Loss of the current year by TA is not transferred to the future in the ""%1"" company. Execute the ""Year-end closing"" period-end operation.'"),
		СтрокаОрганизации.Организация));
	КонецЦикла;

	ТаблицаУбыткиСрок10Лет = Запрос.МенеджерВременныхТаблиц.Таблицы["УбыткиСрок10Лет"].ПолучитьДанные().Выгрузить();

	Для Каждого СтрокаОрганизации Из ТаблицаУбыткиСрок10Лет Цикл

		УстановитьСостояниеВыполненСОшибками(
		ПараметрыОбработчика,
		СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'В организации ""%1"" обнаружены убытки прошлых лет по НУ, по которым истек срок списания - 10 лет. Для списания убытков воспользуйтесь ручной операцией.';
		|en = 'Losses of previous years by TA were detected in the ""%1""  company by which write-off period (10 years) has expired. To write off losses, use a manual operation.'"),
		СтрокаОрганизации.Организация)
		,
		,
		,Перечисления.ВариантыВажностиПроблемыСостоянияСистемы.Предупреждение);
	КонецЦикла;

КонецПроцедуры

#КонецОбласти
﻿
&ИзменениеИКонтроль("ЗаполнитьВидыЗапасовПоТаблицеОстатковСформироватьВТНовыеРезервы")
Процедура МЛ_ЗаполнитьВидыЗапасовПоТаблицеОстатковСформироватьВТНовыеРезервы(ДокументОбъект, МенеджерВременныхТаблиц, Отказ, ПараметрыЗаполненияВидовЗапасов)

	ТаблицаОшибок          = ТаблицаОшибокЗаполненияВидовЗапасов();
	ДополнительныеСвойства = ДокументОбъект.ДополнительныеСвойства;

	Если ПараметрыЗаполненияВидовЗапасов.ТаблицаВидыЗапасов <> Неопределено Тогда
		ТаблицаВидыЗапасов = ПараметрыЗаполненияВидовЗапасов.ТаблицаВидыЗапасов;
	Иначе
		ТаблицаВидыЗапасов = ДокументОбъект[ПараметрыЗаполненияВидовЗапасов.ИмяТЧВидыЗапасов].ВыгрузитьКолонки();
	КонецЕсли;

	КонтролироватьИзменениеНомеровГТД = ДополнительныеСвойства.Свойство("КонтролироватьИзменениеНомеровГТД");
	Если КонтролироватьИзменениеНомеровГТД Тогда
		ТаблицаНомеровГТД = ТаблицаВидыЗапасов.Скопировать(, "АналитикаУчетаНоменклатуры, НомерГТД, Количество");
	КонецЕсли;

	ВидЗапасовПоУмолчанию = Неопределено;
	ТаблицаНовыеРезервы = РегистрыНакопления.РезервыТоваровОрганизаций.СоздатьНаборЗаписей().ВыгрузитьКолонки();

	ПриНехваткеТоваровОрганизацииЗаполнятьВидамиЗапасовПоУмолчанию =
	ПриНехваткеТоваровОрганизацииЗаполнятьВидамиЗапасовПоУмолчанию(ПараметрыЗаполненияВидовЗапасов);


	СтруктураДанныхСтроки = Новый Структура("
	|Склад,
	|Количество,
	|СуммаСНДС,
	|СтавкаНДС,
	|СуммаНДС,
	|СуммаВознаграждения,
	|СуммаНДСВознаграждения,
	|АналитикаУчетаНоменклатурыОтгрузки,
	|ВидЗапасовОтгрузки,
	|ДокументПередачи,
	|ЗаказКлиента,
	|НедостаточноОстатковТоваровОрганизаций
	|");

	СтруктураВидаЗапасовПоУмолчанию = Новый Структура("
	|ГруппаФинансовогоУчета,
	|ВладелецТовара,
	|Контрагент,
	|Соглашение,
	|Договор,
	|Валюта,
	|НалогообложениеНДС,
	|НалогообложениеОрганизации,
	|ТипЗапасов,
	|ВидЦены
	|");

	Запрос = ЗапросРаспределенияСУчетомПриоритетовТаблицыТоваровНаТаблицуОстатков(
	МенеджерВременныхТаблиц, 
	ДополнительныеСвойства);
	МассивРезультатов = Запрос.ВыполнитьПакет();

	ВыборкаДанныеДокумента = МассивРезультатов[1].Выбрать();
	ВыборкаТаблицаТоваров = МассивРезультатов[2].Выбрать();
	ВыборкаОстаткиПоСтрокам = МассивРезультатов[5].Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);

	ВыборкаДанныеДокумента.Следующий();
	ЗаполнитьЗначенияСвойств(СтруктураВидаЗапасовПоУмолчанию, ВыборкаДанныеДокумента);
	ПередачаПодДеятельность = ВыборкаДанныеДокумента.НалогообложениеНДС;
	Организация = ВыборкаДанныеДокумента.Организация;
	ХозяйственнаяОперация = ВыборкаДанныеДокумента.ХозяйственнаяОперация;

	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиента
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиентаПрошлыхПериодов Тогда
		ТекстИсключения = НСтр("ru = 'Функция ЗаполнитьВидыЗапасовПоТаблицеОстатковСформироватьВТНовыеРезервы не предназначена для вызова с хозяйственной операцией %ХозяйственнаяОперация%';
		|en = 'The ЗаполнитьВидыЗапасовПоТаблицеОстатковСформироватьВТНовыеРезервы function cannot be called with business transaction %ХозяйственнаяОперация%'");	
		ТекстИсключения = СтрЗаменить(ТекстИсключения, "%ХозяйственнаяОперация%", ХозяйственнаяОперация);
		ВызватьИсключение ТекстИсключения;
	КонецЕсли;

	ВидыЗапасовПоУмолчанию = Новый ТаблицаЗначений;
	ВидыЗапасовПоУмолчанию.Колонки.Добавить("ВидЗапасов",
	Новый ОписаниеТипов("СправочникСсылка.ВидыЗапасов"));
	ВидыЗапасовПоУмолчанию.Колонки.Добавить("ВидЦены",
	Новый ОписаниеТипов("СправочникСсылка.ВидыЦенПоставщиков"));
	ВидыЗапасовПоУмолчанию.Колонки.Добавить("ГруппаФинансовогоУчета",
	Новый ОписаниеТипов("СправочникСсылка.ГруппыФинансовогоУчетаНоменклатуры"));

	ПроводитьБезКонтроляОстатковТоваровОрганизаций = ПараметрыСеанса.ПроводитьБезКонтроляОстатковТоваровОрганизаций
	ИЛИ НЕ Константы.КонтролироватьОстаткиТоваровОрганизаций.Получить();

	Пока ВыборкаОстаткиПоСтрокам.Следующий() Цикл

		КоличествоТовара			= ВыборкаОстаткиПоСтрокам.Количество;
		СуммаТовараСНДС				= ВыборкаОстаткиПоСтрокам.СуммаСНДС;
		СуммаНДСТовара				= ВыборкаОстаткиПоСтрокам.СуммаНДС;
		СуммаВознагражденияВсего	= ВыборкаОстаткиПоСтрокам.СуммаВознаграждения;
		СуммаНДСВознагражденияВсего	= ВыборкаОстаткиПоСтрокам.СуммаНДСВознаграждения;

		// Детальные записи
		Выборка = ВыборкаОстаткиПоСтрокам.Выбрать();
#Вставка
// ++EE:BAN 26.12.2021 MLVC-17 
		НомерГТД = Неопределено;
#КонецВставки
		
		Пока Выборка.Следующий() Цикл

			ЗаполнитьСтруктуруПоСтрокеТоваров(СтруктураВидаЗапасовПоУмолчанию, Выборка, ВыборкаДанныеДокумента);
			ПродажаНаЭкспортКомиссионногоТовара = Ложь;

			Если ЗначениеЗаполнено(Выборка.ВидЗапасов) Тогда
				ПодбиратьВТЧТоварыПринятыеНаОтветственноеХранение = ПодбиратьВТЧТоварыПринятыеНаОтветственноеХранение(ПараметрыЗаполненияВидовЗапасов, Выборка);

				Если Выборка.ОрганизацияОстатка = Организация
					И (Выборка.ТипЗапасов = Перечисления.ТипыЗапасов.ТоварНаХраненииСПравомПродажи
					И (ВРег(ПодбиратьВТЧТоварыПринятыеНаОтветственноеХранение) = ВРег("ПоНастройкамДоговора")
					//++ НЕ УТ
					И Выборка.ПорядокОформленияСписанияНедостачПринятыхНаХранениеТоваров 
					= Перечисления.ПорядокОформленияСписанияНедостачПринятыхНаХранениеТоваров.ОформлятьСписаниеПринятыхНаХранениеТоваров
					//-- НЕ УТ
					Или ВРег(ПодбиратьВТЧТоварыПринятыеНаОтветственноеХранение) = ВРег("Всегда"))
					Или Выборка.ТипЗапасов <> Перечисления.ТипыЗапасов.ТоварНаХраненииСПравомПродажи) Тогда
					ВидЗапасов = Выборка.ВидЗапасов;
				ИначеЕсли Выборка.НастройкаПередачи = Перечисления.СпособыПередачиТоваров.ПередачаНаКомиссию
					И ПередачаПодДеятельность <> Перечисления.ТипыНалогообложенияНДС.ПродажаНаЭкспорт 
					И ПередачаПодДеятельность <> Перечисления.ТипыНалогообложенияНДС.ЭкспортНесырьевыхТоваров
					И ПередачаПодДеятельность <> Перечисления.ТипыНалогообложенияНДС.ЭкспортСырьевыхТоваровУслуг Тогда
					// Должен быть сгенерирован комиссионный товар,
					// остатки по которому позже будут закрыты документом передачи на комиссию.
					Если Выборка.НовыйВидЗапасовЭтоСубкомиссия Тогда
						// сохраняется исходное налогообложение, под которое был принят товар на комиссию
						НалогообложениеКомиссионногоВидаЗапасов = Выборка.НовыйВидЗапасовНалогообложениеКомитента;
					Иначе

						// налогообложение определяется по организации-комитенту.
						ПараметрыУчетаПоОрганизации =
						УчетНДСУП.ПараметрыУчетаПоОрганизации(
						Выборка.НовыйВидЗапасовВладелецТовара,
						ДокументОбъект.Дата);

						НалогообложениеКомиссионногоВидаЗапасов =
						ПараметрыУчетаПоОрганизации.ОсновноеНалогообложениеНДСПродажи;

					КонецЕсли;

					СтруктураВидаЗапасовПоУмолчанию.НалогообложениеНДС = НалогообложениеКомиссионногоВидаЗапасов;

					ВидЗапасов = Справочники.ВидыЗапасов.ВидЗапасовДокумента(Организация,
					Перечисления.ХозяйственныеОперации.ПередачаНаКомиссиюВДругуюОрганизацию,
					СтруктураВидаЗапасовПоУмолчанию);
				ИначеЕсли Выборка.НастройкаПередачи = Перечисления.СпособыПередачиТоваров.Продажа Тогда
					ВидЗапасов = Справочники.ВидыЗапасов.ВидЗапасовДокумента(Организация,
					Перечисления.ХозяйственныеОперации.РеализацияТоваровВДругуюОрганизацию,
					СтруктураВидаЗапасовПоУмолчанию);
				ИначеЕсли Выборка.ОрганизацияОстатка = Организация
					И ЗначениеЗаполнено(Выборка.ВладелецТовара) Тогда

					ВидЗапасов = Справочники.ВидыЗапасов.ВидЗапасовДокумента(Организация,
					Перечисления.ХозяйственныеОперации.РеализацияТоваровВДругуюОрганизацию,
					СтруктураВидаЗапасовПоУмолчанию);
				Иначе // при настройках интеркампани - передача на комиссию, нельзя продавать на экспорт.
					ВидЗапасов = Справочники.ВидыЗапасов.ПустаяСсылка();
					ПродажаНаЭкспортКомиссионногоТовара = Истина;
				КонецЕсли;
			Иначе
				ВидЗапасов = Справочники.ВидыЗапасов.ПустаяСсылка();
			КонецЕсли;

			КоличествоОстаток	= Выборка.КоличествоОстаток;
			СуммаОстаток		= Выборка.СуммаОстаток;
			НеУказанНомерГТД	= Ложь;
#Вставка
// ++EE:BAN 26.12.2021 MLVC-17 
			КоличествоПодобрано = 0;
			Если ТипЗнч(ДокументОбъект) = Тип("ДокументОбъект.ЭтапПроизводства2_2")
				И ПараметрыЗаполненияВидовЗапасов.ИмяТЧВидыЗапасов = "ВидыЗапасовМатериалы"
				И НЕ ВыборкаОстаткиПоСтрокам.Количество = ВыборкаОстаткиПоСтрокам.КоличествоОстаток
				И Выборка.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПередачаВПроизводство Тогда
				
				Если НомерГТД = Неопределено
					ИЛИ НомерГТД <> Выборка.НомерГТД Тогда
					
					// возможно данный материал уже подобран (в случае не серийных кладовых в производстве и разных ГТД)
					СтруктураПоиска = Новый Структура("ВидЗапасов, АналитикаУчетаНоменклатуры, НомерГТД");
					ЗаполнитьЗначенияСвойств(СтруктураПоиска, Выборка);
					НайденныеСтроки = ТаблицаВидыЗапасов.НайтиСтроки(СтруктураПоиска);
					Для каждого ПодобраннаяСтрока Из НайденныеСтроки Цикл
						КоличествоПодобрано = КоличествоПодобрано + ПодобраннаяСтрока.Количество;
					КонецЦикла; 
				КонецЕсли;
				
				НомерГТД = Выборка.НомерГТД;
				
				Если КоличествоПодобрано > 0 Тогда
					Если Выборка.КоличествоОстаток = КоличествоПодобрано Тогда
						Продолжить;
					ИначеЕсли Выборка.КоличествоОстаток > КоличествоПодобрано Тогда
						КоличествоОстаток = Выборка.КоличествоОстаток - КоличествоПодобрано;
						Если СуммаОстаток <> 0 Тогда
							СуммаОстаток = Выборка.КоличествоОстаток / Выборка.СуммаОстаток * КоличествоОстаток;
						КонецЕсли;
					КонецЕсли;
				КонецЕсли; 
			КонецЕсли; 
#КонецВставки

			Если (ЗначениеЗаполнено(ВидЗапасов) 
				И Выборка.ВидЗапасов <> ВидЗапасов
				И СтруктураВидаЗапасовПоУмолчанию.ТипЗапасов = Перечисления.ТипыЗапасов.ТоварНаХраненииСПравомПродажи)
				// По ТоварНаХраненииСПравомПродажи не может быть прихода резерва
				Или Выборка.НеУказанНомерГТД
				Или ПродажаНаЭкспортКомиссионногоТовара Тогда

				КоличествоОстаток	= 0;
				СуммаОстаток		= 0;
				НеУказанНомерГТД	= Выборка.НеУказанНомерГТД;

			ИначеЕсли (КоличествоТовара > 0 И КоличествоОстаток > 0)
				Или (КоличествоТовара < 0 И КоличествоОстаток < 0) Тогда

				НоваяСтрока = ТаблицаВидыЗапасов.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСтрока, Выборка);

				Если ЗначениеЗаполнено(ВидЗапасов) Тогда

					НоваяСтрока.ВидЗапасов = ВидЗапасов;

				Иначе

					ТекстИсключения = НСтр("ru = 'Ненулевое количество при пустом виде запасов.';
					|en = 'Non-zero quantity with empty inventory kind.'");
					ВызватьИсключение ТекстИсключения;

				КонецЕсли;

				Если КоличествоТовара > 0 Тогда

					КоличествоОстаток = Макс(КоличествоОстаток, 0);
					Количество = Мин(КоличествоТовара, КоличествоОстаток);

					Если СуммаОстаток <> 0 Тогда
						СуммаОстаток = Макс(СуммаОстаток, 0);
						СуммаСНДС = ?(КоличествоОстаток = Количество, СуммаОстаток, Мин(СуммаТовараСНДС, СуммаОстаток));
					Иначе
						СуммаСНДС = ?(КоличествоТовара <> 0, Окр(Количество * СуммаТовараСНДС / КоличествоТовара, 2, 1), 0);
					КонецЕсли;

				Иначе

					КоличествоОстаток = Мин(КоличествоОстаток, 0);
					Количество = Макс(КоличествоТовара, КоличествоОстаток);

					Если СуммаОстаток <> 0 Тогда
						СуммаОстаток = Мин(СуммаОстаток, 0);
						СуммаСНДС = ?(КоличествоОстаток = Количество, СуммаОстаток, Макс(СуммаТовараСНДС, СуммаОстаток));
					Иначе
						СуммаСНДС = ?(КоличествоТовара <> 0, Окр(Количество * СуммаТовараСНДС / КоличествоТовара, 2, 1), 0);
					КонецЕсли;

				КонецЕсли;

				ЗаполнитьЗначенияСвойств(СтруктураДанныхСтроки, Выборка);

				СтруктураДанныхСтроки.Количество				= Количество;
				СтруктураДанныхСтроки.СуммаСНДС					= СуммаСНДС;
				СтруктураДанныхСтроки.СуммаНДС					= ?(СуммаТовараСНДС <> 0, Окр(СуммаСНДС * СуммаНДСТовара / СуммаТовараСНДС, 2, 1), 0);
				СтруктураДанныхСтроки.СуммаВознаграждения		= ?(КоличествоТовара <> 0, Окр(Количество * СуммаВознагражденияВсего / КоличествоТовара, 2, 1), 0);
				СтруктураДанныхСтроки.СуммаНДСВознаграждения	= ?(КоличествоТовара <> 0, Окр(Количество * СуммаНДСВознагражденияВсего / КоличествоТовара, 2, 1), 0);
				СтруктураДанныхСтроки.НедостаточноОстатковТоваровОрганизаций = Ложь;

				ЗаполнитьЗначенияСвойств(НоваяСтрока, СтруктураДанныхСтроки);

				// Для товаров, выкупаемых по интеркампани, формируются движения по Резервам.
				Если Выборка.ВидЗапасов <> ВидЗапасов Тогда

					ДвижениеРезерваПродавца = ТаблицаНовыеРезервы.Добавить();
					ЗаполнитьЗначенияСвойств(ДвижениеРезерваПродавца, НоваяСтрока);
					ДвижениеРезерваПродавца.ВидДвижения 	   = ВидДвиженияНакопления.Приход;
					ДвижениеРезерваПродавца.Период             = КонецМесяца(ВыборкаДанныеДокумента.Период);
					ДвижениеРезерваПродавца.Организация        = Организация;
					ДвижениеРезерваПродавца.КорОрганизация     = Выборка.ОрганизацияОстатка;
					ДвижениеРезерваПродавца.КорВидЗапасов      = Выборка.ВидЗапасов;

					РегистрыНакопления.РезервыТоваровОрганизаций.ДобавитьВНаборРезерваКорЗапись(ТаблицаНовыеРезервы, ДвижениеРезерваПродавца);

				КонецЕсли;

				КоличествоТовара			= КоличествоТовара				- СтруктураДанныхСтроки.Количество;
				СуммаТовараСНДС				= СуммаТовараСНДС				- СтруктураДанныхСтроки.СуммаСНДС;
				СуммаНДСТовара				= СуммаНДСТовара				- СтруктураДанныхСтроки.СуммаНДС;
				СуммаВознагражденияВсего	= СуммаВознагражденияВсего		- СтруктураДанныхСтроки.СуммаВознаграждения;
				СуммаНДСВознагражденияВсего	= СуммаНДСВознагражденияВсего	- СтруктураДанныхСтроки.СуммаНДСВознаграждения;
				СуммаОстаток				= СуммаОстаток					- СтруктураДанныхСтроки.СуммаСНДС;

			КонецЕсли;

		КонецЦикла; // Выборка по остаткам

		Если КоличествоТовара <> 0
			ИЛИ СуммаТовараСНДС <> 0 Тогда

			Если ПриНехваткеТоваровОрганизацииЗаполнятьВидамиЗапасовПоУмолчанию
				ИЛИ ПроводитьБезКонтроляОстатковТоваровОрганизаций Тогда

				Если Не ПараметрыЗаполненияВидовЗапасов.ВладелецТовараВШапке
					И ПараметрыЗаполненияВидовЗапасов.ТаблицаРеквизитовВидовЗапасовПоУмолчанию.Количество() = 0 Тогда
					ТекстИсключения = НСтр("ru = 'Установлен флаг получения владельца товаров из таблицы
					|реквизитов видов запасов по умолчанию, но таблица не передана.';
					|en = 'Check box of receiving goods owner from the inventory kind attribute table
					|is selected by default, but the table is not transferred.'");
					ВызватьИсключение ТекстИсключения;
				КонецЕсли;

				ВыборкаТаблицаТоваров.Сбросить();
				ВидыЗапасовПоУмолчанию.Очистить();

				Если ВыборкаТаблицаТоваров.НайтиСледующий(Новый Структура("НомерСтроки", ВыборкаОстаткиПоСтрокам.НомерСтроки)) Тогда

					ВидЦены = ВыборкаТаблицаТоваров.ВидЦены;
					ГруппаФинансовогоУчета = ВыборкаТаблицаТоваров.ГруппаФинансовогоУчета;

					Если ПараметрыЗаполненияВидовЗапасов.ВладелецТовараВШапке Тогда
						Отбор = Новый Структура("ВидЦены, ГруппаФинансовогоУчета", ВидЦены, ГруппаФинансовогоУчета);
						СтрокиВидовЗапасовПоУмолчанию = ВидыЗапасовПоУмолчанию.НайтиСтроки(Отбор);

						Если СтрокиВидовЗапасовПоУмолчанию.Количество() = 0 Тогда
							ЗаполнитьЗначенияСвойств(СтруктураВидаЗапасовПоУмолчанию, Отбор);

							ВидЗапасовПоУмолчанию = Справочники.ВидыЗапасов.ВидЗапасовДокумента(
							Организация,
							ХозяйственнаяОперация,
							СтруктураВидаЗапасовПоУмолчанию);

							НоваяСтрока = ВидыЗапасовПоУмолчанию.Добавить();
							ЗаполнитьЗначенияСвойств(НоваяСтрока, Отбор);
							НоваяСтрока.ВидЗапасов = ВидЗапасовПоУмолчанию;
						КонецЕсли;
					Иначе
						СтруктураВидаЗапасовПоУмолчанию.ВидЦены = ВидЦены;
						СтруктураВидаЗапасовПоУмолчанию.ГруппаФинансовогоУчета = ГруппаФинансовогоУчета;

						СтруктураВидаЗапасовПоУмолчанию.ВладелецТовара	= ВыборкаТаблицаТоваров.ВладелецТовара;
						СтруктураВидаЗапасовПоУмолчанию.Контрагент		= ВыборкаТаблицаТоваров.Контрагент;
						СтруктураВидаЗапасовПоУмолчанию.Договор			= ВыборкаТаблицаТоваров.Договор;
						СтруктураВидаЗапасовПоУмолчанию.ТипЗапасов		= ВыборкаТаблицаТоваров.ТипЗапасов;

						ВидЗапасовПоУмолчанию = Справочники.ВидыЗапасов.ВидЗапасовДокумента(
						Организация,
						ХозяйственнаяОперация,
						СтруктураВидаЗапасовПоУмолчанию);
					КонецЕсли;

					НоваяСтрока = ТаблицаВидыЗапасов.Добавить();
					ЗаполнитьЗначенияСвойств(НоваяСтрока, ВыборкаТаблицаТоваров);
					НоваяСтрока.ВидЗапасов = ВидЗапасовПоУмолчанию;

					ЗаполнитьЗначенияСвойств(СтруктураДанныхСтроки, ВыборкаТаблицаТоваров);

					СтруктураДанныхСтроки.Количество				= КоличествоТовара;
					СтруктураДанныхСтроки.СуммаСНДС					= СуммаТовараСНДС;
					СтруктураДанныхСтроки.СуммаНДС					= СуммаНДСТовара;
					СтруктураДанныхСтроки.СуммаВознаграждения		= 0;
					СтруктураДанныхСтроки.СуммаНДСВознаграждения	= 0;
					СтруктураДанныхСтроки.ЗаказКлиента				= Неопределено;
					СтруктураДанныхСтроки.НедостаточноОстатковТоваровОрганизаций = Истина;

					ЗаполнитьЗначенияСвойств(НоваяСтрока, СтруктураДанныхСтроки);

					Если Не ПараметрыЗаполненияВидовЗапасов.ВладелецТовараВШапке Тогда

						СтруктураВидаЗапасовПоУмолчаниюРезервы = Новый Структура;
						Для Каждого ТекКлюч Из СтруктураВидаЗапасовПоУмолчанию Цикл
							СтруктураВидаЗапасовПоУмолчаниюРезервы.Вставить(ТекКлюч.Ключ, ТекКлюч.Значение);
						КонецЦикла;
						СтруктураВидаЗапасовПоУмолчаниюРезервы.ТипЗапасов		= ВыборкаТаблицаТоваров.ТипЗапасовРезервы;

						// Добавляем резервы для неотрицательности остатка продукции по товарам организации
						ВидЗапасовПоУмолчаниюРезерв = Справочники.ВидыЗапасов.ВидЗапасовДокумента(
						Организация,
						ХозяйственнаяОперация,
						СтруктураВидаЗапасовПоУмолчаниюРезервы);

						ДвижениеРезерваСобственного = ТаблицаНовыеРезервы.Добавить();
						ЗаполнитьЗначенияСвойств(ДвижениеРезерваСобственного, НоваяСтрока);
						ДвижениеРезерваСобственного.ВидДвижения = ВидДвиженияНакопления.Приход;

						ДвижениеРезерваСобственного.Период             = ВыборкаДанныеДокумента.Период;
						ДвижениеРезерваСобственного.Организация        = Организация;

						ДвижениеРезерваСобственного.КорОрганизация     = Организация;
						ДвижениеРезерваСобственного.КорВидЗапасов      = ВидЗапасовПоУмолчаниюРезерв;

						// Добавляем резервы для отражения отрицательного неконтролируемого остатка
						РегистрыНакопления.РезервыТоваровОрганизаций.ДобавитьВНаборРезерваКорЗапись(ТаблицаНовыеРезервы, ДвижениеРезерваСобственного);

					КонецЕсли;

				КонецЕсли;
			КонецЕсли;
			Если Не ПриНехваткеТоваровОрганизацииЗаполнятьВидамиЗапасовПоУмолчанию Тогда
				Если ПараметрыЗаполненияВидовЗапасов.СообщатьОбОшибкахЗаполнения Тогда
					Выборка = ВыборкаОстаткиПоСтрокам.Выбрать();
					Выборка.Следующий();

					НоваяСтрокаОшибок = ТаблицаОшибок.Добавить();
					ЗаполнитьЗначенияСвойств(НоваяСтрокаОшибок, ВыборкаТаблицаТоваров);

					НоваяСтрокаОшибок.Количество		= КоличествоТовара;
					НоваяСтрокаОшибок.ЕдиницаИзмерения	= Выборка.ЕдиницаИзмерения;
					НоваяСтрокаОшибок.НеУказанНомерГТД	= НеУказанНомерГТД;
					НоваяСтрокаОшибок.Номенклатура		= Выборка.Номенклатура;
					НоваяСтрокаОшибок.Характеристика	= Выборка.Характеристика;
					НоваяСтрокаОшибок.Серия				= Выборка.Серия;
					НоваяСтрокаОшибок.Склад				= Выборка.Склад;
					НоваяСтрокаОшибок.Назначение		= Выборка.Назначение;
				КонецЕсли;
				Если Не ПроводитьБезКонтроляОстатковТоваровОрганизаций Тогда
					Отказ = Истина;
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла; // Выборка по строкам документа

	Если ТаблицаОшибок.Количество() > 0 И ПроводитьБезКонтроляОстатковТоваровОрганизаций Тогда
		ТекстСообщения = НСтр( "ru = 'Документ %Ссылка% проведен с выключенным контролем остатков товаров организаций.';
		|en = 'The %Ссылка% document is posted with enabled company stock balance control.'", Метаданные.ОсновнойЯзык.КодЯзыка);
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Ссылка%", ДокументОбъект.Ссылка);

		ЗаписьЖурналаРегистрации(ИмяСобытияВыключенКонтрольОстатков(),
		УровеньЖурналаРегистрации.Предупреждение,
		,
		ДокументОбъект.Ссылка,
		ТекстСообщения);
	КонецЕсли;

	Если ПараметрыЗаполненияВидовЗапасов.ТаблицаВидыЗапасов = Неопределено
		И Не Отказ Тогда
		ДокументОбъект[ПараметрыЗаполненияВидовЗапасов.ИмяТЧВидыЗапасов].Загрузить(ТаблицаВидыЗапасов);
	КонецЕсли;

	Если КонтролироватьИзменениеНомеровГТД Тогда

		ТаблицаНомеровГТД.Колонки.Добавить("НовоеКоличество");

		Для Каждого СтрокаТаблицы Из ТаблицаВидыЗапасов Цикл

			НоваяСтрока = ТаблицаНомеровГТД.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТаблицы);
			НоваяСтрока.Количество = 0;
			НоваяСтрока.НовоеКоличество = СтрокаТаблицы.Количество;

		КонецЦикла;

		ТаблицаНомеровГТД.Свернуть("АналитикаУчетаНоменклатуры, НомерГТД", "Количество, НовоеКоличество");
		Для Каждого СтрокаТаблицы Из ТаблицаНомеровГТД Цикл
			Если СтрокаТаблицы.Количество <> СтрокаТаблицы.НовоеКоличество Тогда
				ДополнительныеСвойства.Вставить("ИзменилисьНомераГТД", Истина);
			КонецЕсли;
		КонецЦикла;

	КонецЕсли;

	СообщитьОбОшибкахКонтроляТоваровОрганизаций(ДокументОбъект, ТаблицаОшибок, ПараметрыЗаполненияВидовЗапасов);

	Если ПараметрыЗаполненияВидовЗапасов.ТаблицаНовыеРезервы <> Неопределено Тогда
		// Условие выполняется только в этапе производства. В этом документе распределение резервов по периодам при заполнении
		// видов запасов не делается - оно происходит при формировании резервов в обработке проведения.
		ПараметрыЗаполненияВидовЗапасов.Вставить("ТаблицаНовыеРезервы", ТаблицаНовыеРезервы);
	Иначе
		ТаблицаНовыеРезервы = РаспределитьРезервыТоваровОрганизацийПоПериодам(ПараметрыЗаполненияВидовЗапасов.КоличествоПериодовПриФормированииРезервовОрганизаций,
		ТаблицаНовыеРезервы,
		МенеджерВременныхТаблиц); 
		Запрос = Новый Запрос;
		Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
		Запрос.Текст =
		"ВЫБРАТЬ
		|	НовыеРезервы.ВидДвижения,
		|	НовыеРезервы.Период,
		|	НовыеРезервы.АналитикаУчетаНоменклатуры,
		|	НовыеРезервы.Организация,
		|	НовыеРезервы.ВидЗапасов,
		|	НовыеРезервы.НомерГТД,
		|	НовыеРезервы.Количество,
		|	НовыеРезервы.КорОрганизация,
		|	НовыеРезервы.КорВидЗапасов
		|ПОМЕСТИТЬ ВТНовыеРезервы
		|ИЗ
		|	&ТаблицаНовыеРезервы КАК НовыеРезервы";

		Запрос.УстановитьПараметр("ТаблицаНовыеРезервы", ТаблицаНовыеРезервы);
		Запрос.Выполнить();
	КонецЕсли;

КонецПроцедуры
